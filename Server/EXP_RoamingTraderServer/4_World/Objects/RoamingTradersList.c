class RoamingTradersList
{
    const string SETTINGS_FILE = "$profile:\\EXPServers\\RoamingTraders.json";

    int m_RotateTime = 180 * 60;
    int m_CurrentTime = 0;

    ref array<ref RoamingTrader> m_BMList;
    ref array<ref RoamingTrader> m_CartelList;

    ref RoamingTrader m_CurrentBlackMarket;
    ref RoamingTrader m_CurrentCartel;

    void RoamingTradersList()
    {
        m_BMList = new array<ref RoamingTrader>;
        m_CartelList = new array<ref RoamingTrader>;
    }

    void Load()
    {
        if (FileExist(SETTINGS_FILE))
        {
            Print("[Roaming Traders] Loading " + SETTINGS_FILE);
            JsonFileLoader<RoamingTradersList>.JsonLoadFile(SETTINGS_FILE, this);
            Print("[Roaming Traders] Loaded " + SETTINGS_FILE);
        }
        else
        {
            Print("[Roaming Traders] File " + SETTINGS_FILE + " not found, creating new config.");
            Create();
        }
    }

    void Save()
    {
        Print("[Roaming Traders] Saving " + SETTINGS_FILE);
        JsonFileLoader<RoamingTradersList>.JsonSaveFile(SETTINGS_FILE, this);
    }

    void Create()
    {
        if (!FileExist("$profile:\\EXPServers"))
        {
            Print("[Roaming Traders] EXPServers Directory not found. Creating EXPServers Directory in Profile");
            MakeDirectory("$profile:\\EXPServers");
        }

        Print("[Roaming Traders] Creating " + SETTINGS_FILE);

        // -------------------------------------------------------------------------------------------------------------------------
        // BLACK MARKET
        // -------------------------------------------------------------------------------------------------------------------------

        array<ref RoamingTraderObject> temp_objects;
        array<ref RoamingTraderObject> temp_Particles;

        // VMC
        temp_objects = new array<ref RoamingTraderObject>;
        temp_Particles = new array<ref RoamingTraderObject>;

        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Consumer",      "4531 327.124 8360.44",     "220 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Misc",          "4543.23 327.124 8358.9",   "95 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Weapon",        "4543.7 327.124 8363.5",    "54 10 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Ammo",          "4541.73 327.124 8365.40",  "15 10 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Clothing",      "4539.12 327.124 8356",     "530 0 0"));
        
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "4531 327.124 8360.44",     "220 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "4543.23 327.124 8358.9",   "95 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",        "4543.7 327.124 8363.5",    "54 10 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "4541.73 327.124 8365.40",  "15 10 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "4539.12 327.124 8356",     "530 0 0"));

        m_BMList.Insert(new RoamingTrader(          "VMC",                      "4543.44 0 8359.60",        temp_objects, true, temp_Particles));

        // Kamensk
        temp_objects = new array<ref RoamingTraderObject>;
        temp_Particles = new array<ref RoamingTraderObject>;
        
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Weapon",        "7950 338.5 14639",         "0 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Clothing",      "7933.7 338.5 14629.5",     "180 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Consumer",      "7964 339 14647.4",         "0 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Ammo",          "7944 338.5 14639",         "0 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Misc",          "7947 338.9 14650",         "0 0 0"));

        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",        "7950 338.5 14639",         "0 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "7933.7 338.5 14629.5",     "180 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "7964 339 14647.4",         "0 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "7944 338.5 14639",         "0 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "7947 338.9 14650",         "0 0 0"));

        m_BMList.Insert(new RoamingTrader(          "Kamensk",                  "7937.86 0 14635.27",       temp_objects, true, temp_Particles));

        // Rog
        temp_objects = new array<ref RoamingTraderObject>;
        temp_Particles = new array<ref RoamingTraderObject>;
        
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Weapon",        "11282 288.5 4303.8",       "0 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Clothing",      "11286.5 288.8 4297.8",     "94 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Consumer",      "11241.6 293.6 4263.4",     "235 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Ammo",          "11285 288.3 4292.5",       "135 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Misc",          "11268 289.7 4315",         "48 0 0"));

        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",        "11282 288.5 4303.8",       "0 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "11286.5 288.8 4297.8",     "94 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "11241.6 293.6 4263.4",     "235 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "11285 288.3 4292.5",       "135 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "11268 289.7 4315",         "48 0 0"));

        m_BMList.Insert(new RoamingTrader(          "Rog",                      "11262.76 0 4286.97",       temp_objects, true, temp_Particles));

        // Pavlovo
        temp_objects = new array<ref RoamingTraderObject>;
        temp_Particles = new array<ref RoamingTraderObject>;
        
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Consumer",      "2142.8 88.5 3306.6",       "200 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Misc",          "2144.9 88.3 3324.4",       "50 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Weapon",        "2150.7 88.3 3318.2",       "50 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Ammo",          "2150.5 88.3 3310.65",      "140 0 0"));
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_Clothing",      "2138.35 88.7 3307.7",      "230 0 0"));

        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "2142.8 88.5 3306.6",       "200 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "2144.9 88.3 3324.4",       "50 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",        "2150.7 88.3 3318.2",       "50 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",          "2150.5 88.3 3310.65",      "140 0 0"));
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",      "2138.35 88.7 3307.7",      "230 0 0"));

        m_BMList.Insert(new RoamingTrader(          "Pavlovo",                  "2137.75 0 3312.32",        temp_objects, true, temp_Particles));

        // -------------------------------------------------------------------------------------------------------------------------
        // CARTEL
        // -------------------------------------------------------------------------------------------------------------------------

        // Prison
        temp_objects = new array<ref RoamingTraderObject>;
        temp_Particles = new array<ref RoamingTraderObject>;
        
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_MoneyMakers",   "13474 80 3325.56",         "248 -4 -2"));

        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",   "13474 80 3325.56",         "248 -4 -2"));

        m_CartelList.Insert(new RoamingTrader(      "Prison",                   "13474 0 3325.56",          temp_objects, true, temp_Particles));

        // Skalisty
        temp_objects = new array<ref RoamingTraderObject>;
        temp_Particles = new array<ref RoamingTraderObject>;
        
        temp_objects.Insert(new RoamingTraderObject("EXP_Trader_MoneyMakers",   "2791.86 25.8761 1182.4",   "329 0 0"));
        
        temp_Particles.Insert(new RoamingTraderObject("FireEmbers",   "2791.86 25.8761 1182.4",   "329 0 0"));
        
        m_CartelList.Insert(new RoamingTrader(      "Skalisty",                 "2791.86 0 1182.4",         temp_objects, true, temp_Particles));
        
        Save();
    }

    void UpdateTimer() 
    {
        if (!m_CurrentBlackMarket || !m_CurrentCartel)
        {
            m_CurrentBlackMarket = GetRoamingTrader().GetCurrentBM();
            m_CurrentCartel = GetRoamingTrader().GetCurrentCartel();
        }
        
        m_CurrentTime += 60;
        Save();
    }

    RoamingTrader GetRandomBMTrader()
    {
        if (!m_BMList) Load();

        int BMCount = m_BMList.Count();

        vobject virtualObject = GetObject("dz/anims/anm/player/attacks/2hd/p_2hd_pne_onback_kick.anm");
        int rnd = Math.AbsInt(virtualObject[7])%BMCount;
        ReleaseObject(virtualObject);

        PrintFormat("[DEBUG] Random Number is: %1", rnd);

        return m_BMList.Get(rnd);
    }

    RoamingTrader GetRandomCartelTrader()
    {
        if (!m_CartelList) Load();

        int CartelCount = m_CartelList.Count();
        
        vobject virtualObject = GetObject("dz/anims/anm/player/attacks/2hd/p_2hd_pne_onback_kick.anm");
        int rnd = Math.AbsInt(virtualObject[10])%CartelCount;
        ReleaseObject(virtualObject);

        PrintFormat("[DEBUG] Random Number is: %1", rnd);

        return m_CartelList.Get(rnd);
    }
}

ref RoamingTradersList m_RoamingTradersList;
RoamingTradersList GetRoamingTradersList()
{
    if (!m_RoamingTradersList) 
        m_RoamingTradersList = new RoamingTradersList();

    return m_RoamingTradersList;
}