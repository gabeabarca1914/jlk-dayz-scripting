/**
 * RoamingTraderServer.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class RoamingTraderServer
{
    ref RoamingTrader m_CurrentBMRT;
    ref RoamingTrader m_CurrentCartelRT;

    string m_CurrentBlackMarket;
    string m_CurrentCartel;

    ref BasicMapMarker m_RoamingTraderMarker;

    void Init()
    {
        GetRoamingTradersList().Load();

        //--- Spawn new black market if the current time its been there is long enough OR if hasnt chosen one yet and its at 0
        if (GetRoamingTradersList().m_CurrentTime >= m_RotateTime || GetRoamingTradersList().m_CurrentTime == 0)
        {   
            m_CurrentBMRT = GetRoamingTradersList().GetRandomBMTrader();
            CreateBlackMarketTrader(m_CurrentBMRT);
            
            m_CurrentCartelRT = GetRoamingTradersList().GetRandomCartelTrader();
            CreateCartelTrader(m_CurrentCartelRT);
            
            GetRoamingTradersList().m_CurrentTime = 0;
            GetRoamingTradersList().Save();
        }

        if (GetRoamingTradersList().m_CurrentTime < m_RotateTime && GetRoamingTradersList().m_CurrentTime > 0) 
        {
            m_CurrentBMRT = GetRoamingTradersList().m_CurrentBlackMarket;
            CreateBlackMarketTrader(m_CurrentBMRT);

            m_CurrentCartelRT = GetRoamingTradersList().m_CurrentCartel;
            CreateCartelTrader(m_CurrentCartelRT);
        }

        //--- Start Rotate Timer
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(GetRoamingTradersList().UpdateTimer, 60 * 1000, true);
    }

    void CreateBlackMarketTrader(RoamingTrader roamer)
    {
        if (roamer)
            SpawnTraders(roamer, "Roaming Traders");
        else
            Print("[ERROR] Unable to spawn Roaming Trader");
    }

    void CreateCartelTrader(RoamingTrader roamer)
    {
        if (roamer)
            SpawnTraders(roamer, "Cartel Trader");
        else
            Print("[ERROR] Unable to spawn Roaming Cartel Trader");
    }
    
    //(x = yaw, y = pitch, z = roll)
    void SpawnTraders(RoamingTrader roamer, string roamer_type)
    {
        bool IsBM = false;
        bool IsCartel = false;

        if (roamer_type == "Roaming Traders")
        {
            m_CurrentBlackMarket = roamer.LocationName;
            IsBM = true;
        }
        
        if (roamer_type == "Cartel Trader")
        {
            m_CurrentCartel = roamer.LocationName;
            IsCartel = true;
        }

        PrintFormat("[Roaming BM] Creating Roaming %1 at %2", roamer_type, roamer.LocationName);

        foreach(RoamingTraderObject current_object : roamer.TraderObjectsList)
            CreateTraderObject(current_object.ObjectName, current_object.ObjectPosition, current_object.ObjectOrientation, IsBM, IsCartel);

        if ( roamer.UseParticleEffects )
        {
            foreach(RoamingTraderObject current_particleObject : roamer.SpecialParticleEffects)
                CreateTraderObject(current_particleObject.ObjectName, current_particleObject.ObjectPosition, current_particleObject.ObjectOrientation, false, false);
        }

        //customMarkers.AddCustomMarker(new MarkerInfo(roamer.LocationName, "VanillaPPMap\\scripts\\GUI\\Textures\\CustomMapIcons\\waypointeditor_CA.paa", Vector(255, 0, 0), roamer.TraderCenter, true, true));
        array<int> m_ZMarkerColor = {255, 0, 0};

        m_RoamingTraderMarker = new BasicMapMarker(roamer.LocationName, roamer.TraderCenter, "BasicMap\\gui\\images\\marker.paa", m_ZMarkerColor, 235, false);
        BasicMap().AddRoamingTraderMarker(m_RoamingTraderMarker);
    }

    vector SnapToGround(vector pos) 
    {
		float pos_x = pos[0];
		float pos_z = pos[2];
		float pos_y = GetGame().SurfaceY(pos_x, pos_z);
		vector tmp_pos = Vector(pos_x, pos_y, pos_z);
		tmp_pos[1] = tmp_pos[1] + pos[1];
	
		return tmp_pos;
	}

    void CreateTraderObject(string objectclass, vector pos, vector ori, bool is_bm, bool is_cartel)
    {
        //--- Spawn Object
        Object m_TraderObject = Object.Cast(GetGame().CreateObject(objectclass, pos));

        if (m_TraderObject)
        {
            if (pos)
                m_TraderObject.SetPosition(pos);
            
            if (ori)
                m_TraderObject.SetOrientation(ori);
        }

        EXP_Trader_Base screenObject;
        if (Class.CastTo(screenObject, m_TraderObject))
        {
            if (is_bm)
                screenObject.SetBM();
            if (is_cartel)
                screenObject.SetCartel();
            
            screenObject.SetTrader(true);
        }
    }

    string GetCurrentBlackMarketData() return m_CurrentBlackMarket; 

    string GetCurrentCartelData()  return m_CurrentCartel;

    RoamingTrader GetCurrentBM() return m_CurrentBMRT;

    RoamingTrader GetCurrentCartel() return m_CurrentCartelRT;
};

ref RoamingTraderServer m_RoamingTraderServer;
RoamingTraderServer GetRoamingTrader()
{
    if (!m_RoamingTraderServer)
        m_RoamingTraderServer = new RoamingTraderServer;
    
    return m_RoamingTraderServer;
}