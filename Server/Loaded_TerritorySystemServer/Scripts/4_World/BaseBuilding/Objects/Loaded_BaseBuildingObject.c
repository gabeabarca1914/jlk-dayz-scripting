/**
 * Loaded_BaseBuildingObject.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class Loaded_BaseBuildingObject extends House 
{
	protected string m_ObjectTexture = string.Empty;
    
    void Loaded_BaseBuildingObject() 
	{
		RegisterNetSyncVariableBool("m_IsBaseObject");
		RegisterNetSyncVariableBool("m_IsDoorOpened");
		RegisterNetSyncVariableBool("m_HasCodelock");
		
		RegisterNetSyncVariableInt("m_OwnerIDOne");
		RegisterNetSyncVariableInt("m_OwnerIDTwo");
		RegisterNetSyncVariableInt("m_OwnerIDThree");
		RegisterNetSyncVariableInt("m_OwnerIDFour");
		RegisterNetSyncVariableInt("m_OwnerIDFive");
	}

    void SetCodeLock(bool status) 
	{
		m_HasCodelock = status;
		SetSynchDirty();
	}

	void SetCodelockType(int CLType) 
	{
		m_CodelockType = CLType;
		SetSynchDirty();
	}

	void SetBaseObjectTexture(string texture) 
	{
		m_ObjectTexture = texture;
	}

	void ShowCodeLock(bool status) 
	{
		if (status)
			ShowSelection("hide_codelock");
		else
			HideSelection("hide_codelock");
	}

    void InsertUser(int id) 
	{
		//--- build array of slots
		array<int> PermissionSlot = {m_OwnerIDOne, m_OwnerIDTwo, m_OwnerIDThree, m_OwnerIDFour, m_OwnerIDFive};
		
		//--- loop through all slots
		foreach (int slot: PermissionSlot)
		{
			//--- make sure slot is not currently defined
			if (slot == 0)
			{
				//--- assign id
				slot = id;

				//--- Synch to client/server
				SetSynchDirty();

				//--- exit method
				return;
			}
		}
	}

	void RemoveUser(int id) 
	{
		//--- build array of slots
		array<int> PermissionSlot = {m_OwnerIDOne, m_OwnerIDTwo, m_OwnerIDThree, m_OwnerIDFour, m_OwnerIDFive};
		
		//--- loop through all slots
		foreach (int slot: PermissionSlot)
		{
			//--- make sure slot is not currently defined
			if (slot == id)
			{
				//--- assign id default value
				slot = 0;

				//--- Synch to client/server
				SetSynchDirty();

				//--- exit method
				return;
			}
		}
	}

    override void OnStoreSave(ParamsWriteContext ctx)
    {
        super.OnStoreSave(ctx);
		
        ctx.Write(m_IsDoorOpened);
		ctx.Write(m_HasCodelock);
		ctx.Write(m_CodelockType);
		ctx.Write(m_OwnerIDOne);
		ctx.Write(m_OwnerIDTwo);
		ctx.Write(m_OwnerIDThree);
		ctx.Write(m_OwnerIDFour);
		ctx.Write(m_OwnerIDFive);
		ctx.Write(m_ObjectTexture);
    }
    
    override bool OnStoreLoad(ParamsReadContext ctx, int version)
    {
		bool result = super.OnStoreLoad(ctx, version);

        ctx.Read(m_IsDoorOpened);
		ctx.Read(m_HasCodelock);
		ctx.Read(m_CodelockType);
		ctx.Read(m_OwnerIDOne);
		ctx.Read(m_OwnerIDTwo);
		ctx.Read(m_OwnerIDThree);
		ctx.Read(m_OwnerIDFour);
		ctx.Read(m_OwnerIDFive);
		ctx.Read(m_ObjectTexture);

		if (m_HasCodelock)
		{
			SetCodeLock(true);
			ShowCodeLock(true);
			SetCodelockType(m_CodelockType);
		}

		if (m_ObjectTexture != string.Empty)
		{
			//--- set object texture here.. TODO
		}

		SetSynchDirty();	

		return result;
    }
};