/**
 * LoadedBaseBuildingKit.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class LoadedBaseBuildingKit extends ItemBase 
{
    override void OnPlacementComplete( Man player, vector position = "0 0 0", vector orientation = "0 0 0" )
	{
		if ( GetGame().IsServer() )
		{
			Loaded_BaseBuildingObject BaseObj = Loaded_BaseBuildingObject.Cast( GetGame().CreateObject( this.KitProduces(), position) );
			BaseObj.SetPosition( position );
			BaseObj.SetOrientation( orientation );
			BaseObj.SetAnimationPhase( "Hologram", 0 );
		}
	} 
};