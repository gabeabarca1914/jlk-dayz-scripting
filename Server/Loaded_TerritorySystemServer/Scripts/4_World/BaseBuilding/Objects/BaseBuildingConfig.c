/**
 * BaseBuildingConfig.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class BaseBuildingConfig 
{
    /*
     * Reads the territory config from disk
     * 
     * @returns BaseBuildingConfig
     */
    static BaseBuildingConfig Read()
    {
        string fileName;
        FileAttr fileAttributes;
        FindFileHandle file = FindFile(BASEBUILDING_CONFIG_FILE, fileName, fileAttributes, 0);

        if (!file)
            return WriteDefault();

        BaseBuildingConfig config = new BaseBuildingConfig;
        JsonFileLoader<BaseBuildingConfig>.JsonLoadFile(BASEBUILDING_CONFIG_FILE, config);
        return config;
    }

    /*
     * Writes a default config file
     */
    static BaseBuildingConfig WriteDefault()
    {
        if (!FileExist(BASEBUILDING_DIR))
            MakeDirectory(BASEBUILDING_DIR);

        BaseBuildingConfig config = new BaseBuildingConfig;

        // Create a new Recipe
        LoadedRecipe recipe = new LoadedRecipe("Example", "Some Description", "BigWallMate", "M4A1");
        recipe.RecipeItems.Insert(new LoadedRecipeItem("SomeClassName1", "SomeItemDisplayName1", 5));
        recipe.RecipeItems.Insert(new LoadedRecipeItem("SomeClassName2", "SomeItemDisplayName2", 1));
        recipe.RecipeTools.Insert(new LoadedRecipeTool("Hatchet", "Hatchet", 0, 1, 35));
        recipe.RecipeTools.Insert(new LoadedRecipeTool("DuctTape", "TieMeUpLikeOneOfYourFrenchBoys", 1, 0, 0));
        
        config.Recipes.Insert(recipe);
        
        JsonFileLoader<BaseBuildingConfig>.JsonSaveFile(BASEBUILDING_CONFIG_FILE, config);
        return config;
    }
};