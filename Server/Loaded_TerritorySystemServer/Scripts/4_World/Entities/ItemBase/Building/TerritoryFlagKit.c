modded class TerritoryFlagKit
{	
	override void OnPlacementComplete( Man player, vector position = "0 0 0", vector orientation = "0 0 0" )
	{
		if ( !GetGame().IsServer() )
		{
            PlayerBase player_base = PlayerBase.Cast( player );
			vector pos = player_base.GetLocalProjectionPosition();
			vector ori = player_base.GetLocalProjectionOrientation();

			UIManager uiManager = g_Game.GetUIManager();
            uiManager.CloseAll();
            uiManager.ShowScriptedMenu(GetNameTerritoryMenu(player_base, pos, ori), NULL);
		}
	}
}