modded class TerritoryFlag
{
    void TerritoryFlag() 
	{
        RegisterNetSyncVariableInt("m_Territory_ID");
        RegisterNetSyncVariableBool("m_IsDecaying");
	}
    
	void SetIsDecaying(bool value)
	{
		m_IsDecaying = value;
		SetSynchDirty();
	}

    override void OnStoreSave(ParamsWriteContext ctx)
    {
        super.OnStoreSave(ctx);

        if (m_Territory_ID == -1)
            m_Territory_ID = Math.RandomInt(0, 9999999);

        ctx.Write(m_Territory_ID);
    }

    override bool OnStoreLoad(ParamsReadContext ctx, int version)
	{
		if (!super.OnStoreLoad(ctx, version)) return false;
		
        ctx.Read(m_Territory_ID);

		if (m_Territory_ID != -1) 
			GetTerritoryManagerServer().LoadTerritory(m_Territory_ID);
		
		return true;
	}
}