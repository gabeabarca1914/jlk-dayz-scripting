/**
 * TerritoryTrigger.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class TerritoryTrigger extends CylinderTrigger
{	
	override void OnEnter(Object obj)
    {
		if (obj.IsMan() && PlayerBase.Cast(obj).GetIdentity())
		{
			Print("Player entered trigger");

			//--- Send Player Territory Related Info
			GetRPCManager().SendRPC("Loaded_Territory_Client", "UpdateTriggerTerritoryResponse", new Param1<ref Loaded_Territory>(GetTerritoryManagerServer().GetTerritoryByGUID(m_ParentTerritoryID.ToString())), true, PlayerBase.Cast(obj).GetIdentity());
		}
    }

    override void OnLeave(Object obj)
    {
        if (obj.IsMan())
    	{
			Print("Player exited trigger");
			GetRPCManager().SendRPC("Loaded_Territory_Client", "UpdateTriggerTerritoryResponse", new Param1<ref Loaded_Territory>(null), true, PlayerBase.Cast(obj).GetIdentity());
		}
    }
};