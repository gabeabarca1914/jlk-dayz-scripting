/**
 * Loaded_Territory.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class Loaded_Territory 
{
    void Loaded_Territory(int guid, string ownerUID, vector pos) 
    {
        GUID = guid;
        OwnerUID = ownerUID;
        Position = pos;
        
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.AttemptDecay, 10 * 1000, false);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.TerritoryRentDueCountDown, 60000 * GetTerritoryManagerServer().Config.TerritoryRentDueTimerMinutes, true);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.AddTerritoryRentCountDown, 60000 * GetTerritoryManagerServer().Config.TerritoryRentIncreaseTimerMinutes, true);

        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.CreateTerritoryTrigger, 5 * 1000, false);
    }

    void ~Loaded_Territory() 
    {
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(TerritoryRentDueCountDown);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(AddTerritoryRentCountDown);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(AttemptDecay);
    }

    void CreateTerritoryTrigger() 
    {
        if (!Level) return;

        m_TerritoryTrigger = TerritoryTrigger.Cast(GetGame().CreateObject("TerritoryTrigger", Position));
        //--- Move trigger under ground
        vector pos = m_TerritoryTrigger.GetPosition();
        pos[1] = 0;
        m_TerritoryTrigger.SetPosition(pos);
        //--- Set Trigger height to max
        m_TerritoryTrigger.SetCollisionCylinder(Level.Radius, 2000);
        m_TerritoryTrigger.m_ParentTerritoryID = GUID;
    }

    void SetName(string name)   Name = name;

    void AddMember(string name, string uid)
    {
        if (!IsMember(uid)) 
            Members.Insert(new LoadedTerritoryMember(name, uid));
        
        GetRPCManager().SendRPC("Loaded_Territory_Client", "AddToTerritoryResponse", new Param1<ref Loaded_Territory>(this), true, GetPlayerIdentityByUID(uid));
    }

    void RemoveMember(string uid)
    {
        if (IsMember(uid))
        {
            int index = GetMemberIndex(uid);
            Members.RemoveOrdered(index);
        }
    }

    void AddAdmin(string uid)
    {
        if ((IsMember(uid) && !IsAdmin(uid)) || AdminUIDS.Count() == 0) AdminUIDS.Insert(uid);
    }

    void RemoveAdmin(string uid)
    {
        int index = AdminUIDS.Find(uid);
        if (index != -1) AdminUIDS.RemoveOrdered(index);
    }

    void SetOwner(string ownerUID) OwnerUID = ownerUID;

    void UpdateMemberName(string name, string uid)
    {
        int index = GetMemberIndex(uid);
        
        if (index != -1)
        {
            LoadedTerritoryMember member = Members[index];
            
            if (member && member.Name != name)
            {
                member.Name = name;
                Write();
            }
        }
    }

    void TerritoryRentDueCountDown() 
    {
        //--- Add one minute to timer
        MinutesTillRentDue += GetTerritoryManagerServer().Config.TerritoryRentDueTimerMinutes;
        
        //--- Less than 24 hours, warn group.
        if (MinutesTillRentDue >= (GetTerritoryManagerServer().Config.TerritoryRentDueTimerMinutes - 1440))
            WarnMembers();

        //--- Rent is due
        if (MinutesTillRentDue >= GetTerritoryManagerServer().Config.TerritoryRentCycleMinutes)
        {
            TerritoryFlag.Cast(GetTerritoryFlagObject()).SetIsDecaying(true);
            TerritoryIsDecayingPayment = true;
            Write();

            if (DecayData.oriBaseItemCount == 0)
            {    
                DecayData.oriBaseItemCount = GetPartArray().Count();
                Write();
            }

            float DecayTimerInterval = GetTerritoryManagerServer().Config.TerritoryDecayTimeMinutes / GetTerritoryManagerServer().Config.DecayTotalStages;
            GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.AttemptDecay, Math.Floor(60000 * DecayTimerInterval), true);
        }
    }

    void AddTerritoryRentCountDown() 
    {
        //--- Add one minute to timer
        MinutesTillRentAdd += GetTerritoryManagerServer().Config.TerritoryRentIncreaseTimerMinutes;
        
        //--- Rent is due
        if (MinutesTillRentAdd >= GetTerritoryManagerServer().Config.TerritoryRentIncreaseMinutes)
        {
            AddTerritoryRent();
            MinutesTillRentAdd = 0;
        }
    }

    void AddTerritoryRent() 
    {
        array<Object> parts = GetPartArray();

        float increase = 0;

        foreach(Object BasePart: parts)
            increase += GetItemPrice(BasePart.GetType());

        increase += GetLevel().GetPrice();
        increase += (Members.Count() * GetLevel().GetPerMemberPrice());
        
        CurrentRentOwed += increase;

        Write();
        UpdateAllMembers();
    }

    void AttemptDecay() 
    {        
        if (TerritoryIsDecayingPayment || TerritoryIsDecayingFlag) 
            Decay();
    }

    void Decay() 
    {
        DecayData.nextDecay += (DecayData.oriBaseItemCount * GetTerritoryManagerServer().Config.decayRatePercent);

        array<Object> parts = GetPartArray(true);
        
        while (DecayData.nextDecay >= 1)
        {
            if (parts.Count() > 0)
            {
                DecayData.nextDecay--;
                GetGame().ObjectDelete(parts.GetRandomElement());
                Write();
            }
            else
            {
                DecayData.nextDecay = 0;
                Delete();
            }
        }
    }

    void Paid() 
    {
        Print("Paid Rent");
        LastPaid = GenerateDateTimeStamp();
        MinutesTillRentDue = 0;
        MinutesTillRentAdd = 0;
        CurrentRentOwed = 0;
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(AttemptDecay);
        array<Object> parts = GetPartArray();

        foreach (Object obj: parts)
        {
            EntityAI entity = EntityAI.Cast(obj);
            entity.IncreaseLifetime();
        }

        if (!TerritoryIsDecayingPayment && !TerritoryIsDecayingFlag)
        {
            if (!DecayData.oriBaseItemCount == 0 || !DecayData.nextDecay == 0)
            {
                DecayData.oriBaseItemCount = 0;
                DecayData.nextDecay = 0;
            }
        }

        Write();
        UpdateAllMembers();
    }

    void Upgrade(LoadedTerritoryLevel IncomingLevel) 
    {
        Level = IncomingLevel;
        Write();
        UpdateAllMembers();
    }

    void Delete() 
    {
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(TerritoryRentDueCountDown);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(AttemptDecay);
        
        //--- TODO: Notify server of base being deleted

        array<Object> parts = GetPartArray(true);

        foreach(Object BaseObject: parts)
            GetGame().ObjectDelete(BaseObject);

        parts = GetPartArray();

        foreach(Object Storage: parts)
        {
            if (Storage)
            {
                vector storagePos = Storage.GetPosition();
                storagePos[1] = GetGame().SurfaceY(storagePos[0], storagePos[2]);
                Storage.SetPosition(storagePos);
            }
        }

        RemoveAllMembers();

        foreach (LoadedTerritoryMember TerritoryMember: Members)
        {
            string MemberUID = TerritoryMember.UID;
            
            RemoveMember(MemberUID);
            RemoveAdmin(MemberUID);
            Write();            
            UpdateAllMembers();
        }

        DeleteFile(GenerateStorageFilePath(this.GUID.ToString()));
    }

    Object GetTerritoryFlagObject() 
    {
        array<Object> nearest_objects = new array<Object>;
        array<CargoBase> proxy_cargos = new array<CargoBase>;
        GetGame().GetObjectsAtPosition3D(GetPosition(), Level.GetRadius(), nearest_objects, proxy_cargos);
    
        foreach(Object Obj: nearest_objects)
            if (Obj.IsInherited(TerritoryFlag)) 
                return Obj;
        return null; 
    }

    //--- TODO
    void WarnMembers()
    {
        foreach(LoadedTerritoryMember TerrMember: Members)
        {
            if (GetPlayerIdentityByUID(TerrMember.UID))
            {
                NotificationSystem.SendNotificationToPlayerIdentityExtended(GetPlayerIdentityByUID(TerrMember.UID), 3, "SIX Base Building", "Your territory rent is due soon..", "set:ccgui_enforce image:Icon40Move");
            }
        }
    }

    void UpdateAllMembers()
    {
        foreach(LoadedTerritoryMember TerrMember: Members)
        {
            if (GetPlayerIdentityByUID(TerrMember.UID))
                GetRPCManager().SendRPC("Loaded_Territory_Client", "UpdateTerritoryDataResponse", new Param1<ref Loaded_Territory>(this), true, GetPlayerIdentityByUID(TerrMember.UID));
        }
    }

    void RemoveAllMembers() 
    {
        foreach(LoadedTerritoryMember TerrMember: Members) 
        {
            if (GetPlayerIdentityByUID(TerrMember.UID)) 
                GetRPCManager().SendRPC("Loaded_Territory_Client", "RemoveFromTerritoryResponse", new Param1<ref Loaded_Territory>(this), true, GetPlayerIdentityByUID(TerrMember.UID));
        }
    }

    void Write()
    {
        if (!FileExist(TERRITORY_DIR)) 
            MakeDirectory(TERRITORY_DIR);
        if (!FileExist(TERRITORY_STORAGE_DIR))
            MakeDirectory(TERRITORY_STORAGE_DIR);
        JsonFileLoader<Loaded_Territory>.JsonSaveFile(GenerateStorageFilePath(this.GUID.ToString()), this);
    }
};