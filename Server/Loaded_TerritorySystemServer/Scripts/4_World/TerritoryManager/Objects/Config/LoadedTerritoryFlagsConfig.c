/**
 * LoadedTerritoryFlagsConfig.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class LoadedTerritoryFlagsConfig 
{
    static LoadedTerritoryFlagsConfig Read()
    {
        string fileName;
        FileAttr fileAttributes;
        FindFileHandle file = FindFile(TERRITORY_FLAG_CONFIG_FILE, fileName, fileAttributes, 0);

        if (!file) 
            return WriteDefault();

        LoadedTerritoryFlagsConfig config = new LoadedTerritoryFlagsConfig;
        JsonFileLoader<LoadedTerritoryFlagsConfig>.JsonLoadFile(TERRITORY_FLAG_CONFIG_FILE, config);

        return config;
    }

   
    static LoadedTerritoryFlagsConfig WriteDefault()
    {
        if (!FileExist(TERRITORY_FLAG_CONFIG_DIR))
            MakeDirectory(TERRITORY_FLAG_CONFIG_DIR);

        LoadedTerritoryFlagsConfig config = new LoadedTerritoryFlagsConfig;
        config.FlagList.Insert("Flag_Chernarus");
        config.FlagList.Insert("Flag_Chedaki");
        config.FlagList.Insert("Flag_NAPA");
        config.FlagList.Insert("Flag_CDF");
        config.FlagList.Insert("Flag_Livonia");
        config.FlagList.Insert("Flag_Altis");
        config.FlagList.Insert("Flag_SSahrani");
        config.FlagList.Insert("Flag_NSahrani");
        config.FlagList.Insert("Flag_DayZ");
        config.FlagList.Insert("Flag_LivoniaArmy");
        config.FlagList.Insert("Flag_White");
        config.FlagList.Insert("Flag_Bohemia");
        config.FlagList.Insert("Flag_APA");
        config.FlagList.Insert("Flag_UEC");
        config.FlagList.Insert("Flag_Pirates");
        config.FlagList.Insert("Flag_Cannibals");
        config.FlagList.Insert("Flag_Bear");
        config.FlagList.Insert("Flag_Wolf");
        config.FlagList.Insert("Flag_BabyDeer");
        config.FlagList.Insert("Flag_Rooster");
        config.FlagList.Insert("Flag_LivoniaPolice");
        config.FlagList.Insert("Flag_CMC");
        config.FlagList.Insert("Flag_TEC");
        config.FlagList.Insert("Flag_CHEL");
        config.FlagList.Insert("Flag_Zenit");
        config.FlagList.Insert("Flag_HunterZ");
        config.FlagList.Insert("Flag_BrainZ");
        config.FlagList.Insert("Flag_Refuge");
        config.FlagList.Insert("Flag_RSTA");
        config.FlagList.Insert("Flag_Snake");

        JsonFileLoader<LoadedTerritoryFlagsConfig>.JsonSaveFile(TERRITORY_FLAG_CONFIG_FILE, config);

        return config;
    }
};
