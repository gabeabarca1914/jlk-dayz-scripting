/**
 * LoadedTerritoryConfig.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class LoadedTerritoryConfig 
{
    static LoadedTerritoryConfig Read()
    {
        string fileName;
        FileAttr fileAttributes;
        FindFileHandle file = FindFile(TERRITORY_CONFIG_FILE, fileName, fileAttributes, 0);

        if (!file) 
            return WriteDefault();

        LoadedTerritoryConfig config = new LoadedTerritoryConfig;
        JsonFileLoader<LoadedTerritoryConfig>.JsonLoadFile(TERRITORY_CONFIG_FILE, config);

        return config;
    }

    static LoadedTerritoryConfig WriteDefault()
    {
        if (!FileExist(TERRITORY_DIR))
            MakeDirectory(TERRITORY_DIR);

        LoadedTerritoryConfig config = new LoadedTerritoryConfig;

        config.NobuildZones.Insert(new LoadedNoBuildZone("Default", 0, 0, 0, 10));
        config.TerritoryLevels.Insert(new LoadedTerritoryLevel("Level 1", 5000, 2500, 400, 10, 30));
        config.BaseItemPrices.Insert(new LoadedBaseItemPrice("EXP_T1_Wall", 100));
        config.BaseItemPrices.Insert(new LoadedBaseItemPrice("EXP_T2_Wall", 300));

        JsonFileLoader<LoadedTerritoryConfig>.JsonSaveFile(TERRITORY_CONFIG_FILE, config);

        return config;
    }
};