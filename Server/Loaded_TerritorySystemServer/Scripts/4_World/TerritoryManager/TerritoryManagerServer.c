/**
 * TerritoryManagerServer.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */


class TerritoryManagerServer 
{
    ref LoadedTerritoryConfig Config;
    ref LoadedTerritoryFlagsConfig FlagConfig;
    ref array<ref Loaded_Territory> Territories = new array<ref Loaded_Territory>;

    void TerritoryManagerServer()
    {
        //--- Territory
        GetRPCManager().AddRPC("Loaded_Territory_Server", "CreateTerritoryRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "TerritoryCheckRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "PayTerritoryRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "UpgradeTerritoryRequest", this);

        //--- Territory Permissions
        GetRPCManager().AddRPC("Loaded_Territory_Server", "InvitePlayerRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "AcceptInviteRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "KickPlayerRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "DemotePlayerRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "PromotePlayerRequest", this);
        GetRPCManager().AddRPC("Loaded_Territory_Server", "LeaveTerritoryRequest", this);
        
        //--- Misc
        GetRPCManager().AddRPC("Loaded_Territory_Server", "GetAllPlayerListRequest", this);

        Config = LoadedTerritoryConfig.Read();
        FlagConfig = LoadedTerritoryFlagsConfig.Read();
    }

    void CreateTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param5<string, PlayerBase, vector, vector, string> data;
        if (!ctx.Read(data)) return;

        TerritoryFlag totem = TerritoryFlag.Cast( GetGame().CreateObjectEx( "TerritoryFlag", data.param3, ECE_PLACE_ON_SURFACE ) );
        totem.SetPosition( data.param3 );
        totem.SetOrientation( data.param4 );
        
        totem.m_Territory_ID = Math.RandomInt(0, 9999999);

        totem.GetConstruction().ForceBuildPart(data.param2, "base");
        totem.GetConstruction().ForceBuildPart(data.param2, "support");
        totem.GetConstruction().ForceBuildPart(data.param2, "pole");    
        totem.GetInventory().CreateAttachment(data.param5);

        //--- Insert into Territories array
        Loaded_Territory territory = new Loaded_Territory(totem.m_Territory_ID, sender.GetPlainId(), data.param3);
        territory.Name = data.param1;
        territory.Members.Insert(new LoadedTerritoryMember(sender.GetName(), sender.GetPlainId()));
        territory.AdminUIDS.Insert(sender.GetPlainId());
        territory.CreatedAt = GenerateDateTimeStamp();
        territory.Level = Config.TerritoryLevels[0];
        territory.DecayData = new LoadedTerritoryDecayData();

        //--- Write JSON File
        territory.Write();

        Territories.Insert(territory);

        //--- Send conformation to client
        GetRPCManager().SendRPC("Loaded_Territory_Client", "CreateTerritoryResponse", new Param1<Loaded_Territory>(territory), true, sender);
    }

    void TerritoryCheckRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        //--- Array of territories player is in.
        array<ref Loaded_Territory> Player_Territories = new array<ref Loaded_Territory>;
        
        string SenderUID = sender.GetPlainId();

        foreach(Loaded_Territory territory: Territories)
        {
            if (territory.OwnerUID == SenderUID)
            {
                Player_Territories.Insert(territory);
            }
        }

        //--- Send conformation to client
        GetRPCManager().SendRPC("Loaded_Territory_Client", "TerritoryCheckResponse", new Param4<array<ref Loaded_Territory>, string, LoadedTerritoryConfig, LoadedTerritoryFlagsConfig>(Player_Territories, sender.GetPlainId(), Config, FlagConfig), true, sender);
    }

    void GetAllPlayerListRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        array<Man> players = new array<Man>;
        GetGame().GetPlayers(players);
        array<ref LoadedTerritoryMember> allPlayers = new array<ref LoadedTerritoryMember>;
        for (int i = 0; i < players.Count(); i++)
        {
            PlayerBase player = PlayerBase.Cast(players[i]);
            if (player)
            {
                string name = player.GetIdentity().GetName();
                string uid = player.GetIdentity().GetPlainId();

                allPlayers.Insert(new LoadedTerritoryMember(name, uid));
            }
            GetRPCManager().SendRPC("Loaded_Territory_Client", "GetAllPlayerListResponse", new Param1<array<ref LoadedTerritoryMember>>(allPlayers), true, sender);
        }
    }

    void AcceptInviteRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref LoadedTerritoryInvite> data;
        if (!ctx.Read(data)) return;
        LoadedTerritoryInvite invite = data.param1;

        string invitingPlayerUID = invite.InviterUID;
        string acceptingPlayerUID = sender.GetPlainId();
        string acceptingPlayerName = sender.GetName();

        Loaded_Territory territory = GetTerritoryByGUID(invite.GUID);

        if (!territory) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Territory no longer exists", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsAdmin(invitingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "This invite is no longer valid", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (PlayerHasTerritory(acceptingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are already a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.AddMember(acceptingPlayerName, acceptingPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
    }

    void InvitePlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref LoadedTerritoryMember> data;
        if (!ctx.Read(data)) return;

        LoadedTerritoryMember player = data.param1;

        string invitedPlayerUID = player.UID;
        string invitingPlayerUID = sender.GetPlainId();
        string invitingPlayerName = sender.GetName();

        if (!PlayerHasTerritory(invitingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        Loaded_Territory territory = GetPlayerTerritoryByUID(invitingPlayerUID);

        if (!territory) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Failed to send invite, Can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsAdmin(invitingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only a territory admin can invite new members", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        LoadedTerritoryInvite invite = new LoadedTerritoryInvite(territory.GetGUID().ToString(), territory.GetName(), invitingPlayerUID, invitingPlayerName);
        GetRPCManager().SendRPC("Loaded_Territory_Client", "AddInviteResponse", new Param1<ref LoadedTerritoryInvite>(invite), true, GetPlayerIdentityByUID(invitedPlayerUID));
        NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Invite sent", "set:ccgui_enforce image:Icon40Move");
    }

    void KickPlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref LoadedTerritoryMember> data;
        if (!ctx.Read(data)) return;

        LoadedTerritoryMember player = data.param1;

        string kickingPlayerUID = sender.GetPlainId();
        string kickedPlayerUID = player.UID;

        Loaded_Territory territory = GetPlayerTerritoryByUID(kickingPlayerUID);

        if (kickingPlayerUID == kickedPlayerUID) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't kick yourself, Leave instead", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsAdmin(kickingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only a territory admin or owner can kick other members", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (territory.IsAdmin(kickedPlayerUID) && !territory.IsOwner(kickingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only the territory owner can kick another admin", "set:ccgui_enforce image:Icon40Move");
            return;
        }


        if (territory.IsOwner(kickedPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't kick the territory owner", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.RemoveMember(kickedPlayerUID);
        territory.RemoveAdmin(kickedPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
        GetRPCManager().SendRPC("Loaded_Territory_Client", "RemoveFromTerritoryResponse", new Param1<ref Loaded_Territory>(territory), true, GetPlayerIdentityByUID(kickedPlayerUID));
    }

    void DemotePlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref LoadedTerritoryMember> data;
        if (!ctx.Read(data)) return;

        LoadedTerritoryMember player = data.param1;

        string sendingPlayerUID = sender.GetPlainId();

        string targetPlayerUID = player.UID;

        if (sendingPlayerUID == targetPlayerUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't demote yourself", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(targetPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The selected player is not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        string sendingPlayerTerritoryGUID = GetPlayerTerritoryGUID(sendingPlayerUID);
        string targetPlayerTerritoryGUID= GetPlayerTerritoryGUID(targetPlayerUID);

        if (sendingPlayerTerritoryGUID != targetPlayerTerritoryGUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not in the same territory as the selected member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        Loaded_Territory territory = GetPlayerTerritoryByUID(sendingPlayerUID);

        if (!territory)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsOwner(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only the territory owner can demote other memebers", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.RemoveAdmin(targetPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
        NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You have demoted the selected member", "set:ccgui_enforce image:Icon40Move");
    }

    void PromotePlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref LoadedTerritoryMember> data;
        if (!ctx.Read(data)) return;

        LoadedTerritoryMember player = data.param1;
        string sendingPlayerUID = sender.GetPlainId();
        string targetPlayerUID = player.UID;

        if (sendingPlayerUID == targetPlayerUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't promote yourself", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(targetPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The target player is not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        string sendingPlayerTerritoryGUID = GetPlayerTerritoryGUID(sendingPlayerUID);
        string targetPlayerTerritoryGUID= GetPlayerTerritoryGUID(targetPlayerUID);

        if (sendingPlayerTerritoryGUID != targetPlayerTerritoryGUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not in the same territory as the target member", "set:ccgui_enforce image:Icon40Move");
            Print("You are not in the same territory as the target member");
            return;
        }

        Loaded_Territory territory = GetPlayerTerritoryByUID(sendingPlayerUID);

        if (!territory)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsOwner(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only the territory owner can promote other memebers", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.AddAdmin(targetPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
        NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You have promoted the selected member", "set:ccgui_enforce image:Icon40Move");
    }

    void LeaveTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        string senderUID = sender.GetPlainId();

        if (!PlayerHasTerritory(senderUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't leave a territory you are not in", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        Loaded_Territory territory = GetPlayerTerritoryByUID(senderUID);

        if (!territory)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server could not find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.RemoveMember(senderUID);
        territory.RemoveAdmin(senderUID);

        if (territory.IsOwner(senderUID))
        {
            if (territory.AdminUIDS.Count() > 0) 
                territory.SetOwner(territory.AdminUIDS[0]);
            else 
            {
                if (territory.Members.Count() > 0) 
                    territory.SetOwner(territory.Members[0].UID);
                else territory.SetOwner("");
            }
        }

        territory.Write();
        territory.UpdateAllMembers();
        GetRPCManager().SendRPC("Loaded_Territory_Client", "RemoveFromTerritoryResponse", new Param1<ref Loaded_Territory>(territory), true, sender);
    }

    void PayTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        string senderUID = sender.GetPlainId();
        PlayerBase player = GetPlayerBaseByUID(senderUID);

        if (PlayerHasTerritory(senderUID))
        {
            Loaded_Territory territory = data.param1;
            int territoryPrice = territory.CurrentRentOwed;

            bool MoneyResult = RemoveCurrency(player, territoryPrice);

            if (MoneyResult)
            {
                territory.Paid();
                GetRPCManager().SendRPC("Loaded_Territory_Client", "PayTerritoryResponse", new Param1<string>("SUCCESS"), true, sender);
            }
            else
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You dont have enough money you cheap fuck", "set:ccgui_enforce image:Icon40Move");
        }
        else
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You don't have a territory you fucking mong", "set:ccgui_enforce image:Icon40Move");
    }

    void UpgradeTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        string senderUID = sender.GetPlainId();
        PlayerBase player = GetPlayerBaseByUID(senderUID);
        if (PlayerHasTerritory(senderUID))
        {
            //--- Current Territory Level
            Loaded_Territory territory = data.param1;
            LoadedTerritoryLevel territoryLevel = territory.GetLevel();
            
            //--- Next Territory Info
            int NextTerritoryLvlIndex = Config.TerritoryLevels.Find(territory.GetLevel());
            bool aidsEnfusionWorkAround = NextTerritoryLvlIndex == 0;
            
            //--- Cannot increment on 0
            LoadedTerritoryLevel NextTerritoryLvl;
            if (aidsEnfusionWorkAround)
                NextTerritoryLvl = Config.TerritoryLevels[1];
            else
                NextTerritoryLvl = Config.TerritoryLevels[NextTerritoryLvlIndex++];

            //--- If next territory couldnt be found
            if (!NextTerritoryLvl) return;
            
            int paymentAmount = NextTerritoryLvl.GetPrice();
            bool MoneyResult = RemoveCurrency(player, paymentAmount);
            
            if (MoneyResult)
            {
                territory.Upgrade(NextTerritoryLvl);
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You have sucessfully upgraded your territory!", "set:ccgui_enforce image:Icon40Move");
            }
            else
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You dont have enough money you cheap fuck", "set:ccgui_enforce image:Icon40Move");
        }
        else
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You don't have a territory you fucking mong", "set:ccgui_enforce image:Icon40Move");
    }

    Loaded_Territory LoadTerritory(int id) 
    {
        Loaded_Territory territory = Read(id);
        Territories.Insert(territory);
        return territory;
    }

    //--- Read Method for territory json's
    static Loaded_Territory Read(int id)
    {
        string filePath = GenerateStorageFilePath(id.ToString());
        string fileName;
        FileAttr fileAttributes;
        FindFileHandle file = FindFile(filePath, fileName, fileAttributes, 0);

        if (file)
        {
            Loaded_Territory territory = new Loaded_Territory(id, "", vector.Zero);
            JsonFileLoader<Loaded_Territory>.JsonLoadFile(filePath, territory);

            return territory;
        }

        return null;
    }

    bool CanDismantle(PlayerBase player, Object targetObject) 
    {
        float snapping_9;
        float snapping_10;

        array<string> poopie = {"T1_Gate", "T2_Gate", "T3_Gate"};
        if (targetObject && player)
        {   
            if (poopie.Find(targetObject.GetType()) != -1)
            {
                snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), GetGame().GetPlayer().GetPosition());
                snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), GetGame().GetPlayer().GetPosition());
                if (snapping_9 > snapping_10) return true;
            }
            
            snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), player.GetPosition());
            snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), player.GetPosition());
            if (snapping_9 < snapping_10) return true;
        }
        return false;
    }

    bool RemoveCurrency(PlayerBase player, int amount)
	{
        TStringArray MoneyTypeArray =
        {
            "Poptab"
        };
		int playerCurrency = GetPlayerCurrency(player);
		if (playerCurrency < amount)
        {
			return false;
        }
		int amountTaken = 0;
		int amountRemaining = amount;
		for (int i = MoneyTypeArray.Count(); i >= 0; --i)
		{
			string currency = MoneyTypeArray[i];
			if (currency)
			{
				int currentCurrencyCount = GetCountOfSpecificItem(player, currency);
				int take = 0;

				while ((amountRemaining > 0) && (take < currentCurrencyCount))
				{
					take++;
					amountRemaining -= 1;
					amountTaken += 1;
				}

				if (take > 0)
				{
					bool removeQuantityResult = RemoveItemQuantity(player, currency, take);
					if (!removeQuantityResult) return false;
				}
			}
		}

		if (amountTaken > amount)
		{
			int change = amountTaken - amount;
			AddCurrency(player, change);
		}
		return true;
	}

    bool AddCurrency(PlayerBase player, int amount)
	{
        TStringArray MoneyTypeArray =
        {
            "Poptab"
        };
		if (!player) return false;
		int amountGiven = 0;
		int amountRemaining = amount;
		for (int i = MoneyTypeArray.Count(); i >= 0; --i)
		{
			string currency = MoneyTypeArray[i];
			
            if (currency)
			{
				int give = 0;
                
				while (amountRemaining >= 1)
				{
					give++;
					amountRemaining -= 1;
					amountGiven += 1;
				}
                
				if (give > 0) 
                    CreateInInventory(player, currency, give);
			}
		}
		return true;
	}

    Loaded_Territory GetPlayerTerritoryByUID(string uid)
    {
        for (int i = 0; i < Territories.Count(); i++)
        {
            Loaded_Territory territory = Territories[i];
            if (!territory) 
                continue;
            
            array<ref LoadedTerritoryMember> memberData = territory.Members;
            if (!memberData) continue;
            
            for (int x = 0; x < memberData.Count(); x++)
                if (memberData[x].UID == uid) return territory;
        }
        return null;
    }

    Loaded_Territory GetTerritoryByGUID(string guid)
    {
        if (!Territories) return null;
        
        for (int i = 0; i < Territories.Count(); i++)
        {
            if (!Territories[i]) continue;

            if (Territories[i].GetGUID().ToString() == guid)
                return Territories[i];
        }

        return null;
    }

    bool PlayerHasTerritory(string uid) return GetPlayerTerritoryByUID(uid) != null;

    string GetPlayerTerritoryGUID(string uid)
    {
        for (int i = 0; i < Territories.Count(); i++)
        {
            Loaded_Territory territory = Territories[i];
            if (!territory) continue;
            array<ref LoadedTerritoryMember> memberData = territory.Members;
            if (!memberData) 
                continue;
            for (int x = 0; x < memberData.Count(); x++)
                if (memberData[x].UID == uid) 
                    return territory.GetGUID().ToString();
        }
        return string.Empty;
    }

    //--- If distance is left to 0 it will just check to see if player is within radius
    bool IsPlayerInOwnTerritory(Man player, float distance = 0) 
    {
        string uid = player.GetIdentity().GetPlainId();
        vector playerPos = player.GetPosition();

        Loaded_Territory Territory = GetPlayerTerritoryByUID(uid);

        if (Territory)
        {
            vector TerritoryPosition = Territory.GetPosition();
            int TerritoryRadius = Territory.GetLevel().GetRadius();

            if (distance == 0 && vector.Distance(playerPos, TerritoryPosition) > TerritoryRadius) return false;
            if (distance != 0 && vector.Distance(playerPos, TerritoryPosition) > distance) return false;

            return true;
        }
    
        return false;
    }

    bool IsPlayerInOwnTerritory(PlayerBase player, float distance = 0) 
    {
        Loaded_Territory Territory = GetPlayerTerritoryByUID(player.GetIdentity().GetPlainId());
        vector playerPos = player.GetPosition();
        
        if (!playerPos) return false;

        if (Territory)
        {
            vector TerritoryPosition = Territory.GetPosition();
            int TerritoryRadius = Territory.GetLevel().GetRadius();

            if (distance == 0 && vector.Distance(playerPos, TerritoryPosition) > TerritoryRadius) return false;
            if (distance != 0 && vector.Distance(playerPos, TerritoryPosition) > distance) return false;

            return true;
        }
    
        return false;
    }
};

ref TerritoryManagerServer g_TerritoryManagerServer;
TerritoryManagerServer GetTerritoryManagerServer()
{
    if (!g_TerritoryManagerServer)
        g_TerritoryManagerServer = new TerritoryManagerServer;
    return g_TerritoryManagerServer;
}