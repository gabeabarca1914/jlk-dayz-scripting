/**
 * ActionBreakOffCodeLock.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionBreakOffCodeLock: ActionContinuousBase 
{
    override void OnFinishProgressServer( ActionData action_data )
	{	
		Loaded_BaseBuildingObject base_building = Loaded_BaseBuildingObject.Cast(action_data.m_Target.GetObject());
		
		if ( base_building )
		{
			//--- Remove Codelock
            base_building.SetCodeLock(false);
			
			//--- add damage to tool
			action_data.m_MainItem.DecreaseHealth( UADamageApplied.DESTROY, false );
		}

		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
	}

	override string GetAdminLogMessage(ActionData action_data)
	{
		return " destroyed codelock on " + action_data.m_Target.GetObject().GetDisplayName() + " with " + action_data.m_MainItem.GetDisplayName();
	}
};