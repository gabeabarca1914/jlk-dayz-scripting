/**
 * ActionAttachCodelock.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionAttachCodelock: ActionInteractBase 
{
    override void OnExecuteServer( ActionData action_data )
    {
        if ( action_data.m_MainItem && action_data.m_MainItem.GetHierarchyRootPlayer() == action_data.m_Player )
        {
            action_data.m_Player.DropItem(action_data.m_MainItem);
            action_data.m_MainItem.Delete();
        }
    }

    override void OnStartServer( ActionData action_data )
	{
        Object obj = action_data.m_Target.GetObject();
        Loaded_BaseBuildingObject baseObject = Loaded_BaseBuildingObject.Cast(obj);

        if (baseObject)
        {    
            baseObject.SetCodeLock(true);
            baseObject.ShowCodeLock(true);
            
            if (action_data.m_MainItem.GetType() == "EXP_PersonalCodelock")
                baseObject.SetCodelockType(CodelockType.Personal);
            else
                baseObject.SetCodelockType(CodelockType.Codelock);
        }
    }
};