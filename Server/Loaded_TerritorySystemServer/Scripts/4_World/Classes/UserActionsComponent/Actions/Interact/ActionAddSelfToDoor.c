/**
 * ActionAddSelfToDoor.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionAddSelfToDoor: ActionInteractBase 
{
    override void OnStartServer( ActionData action_data )
	{
        Object obj = action_data.m_Target.GetObject();
        Loaded_BaseBuildingObject baseObject = Loaded_BaseBuildingObject.Cast(obj);

        if (baseObject)
        {    
            if (baseObject.GetAllUsers().Count() >= 5)
                return;
            
            if (!action_data.m_Player.GetIdentity())
                return;

            baseObject.InsertUser(action_data.m_Player.GetIdentity().GetPlainId().ToInt());

            NotificationSystem.SendNotificationToPlayerIdentityExtended(action_data.m_Player.GetIdentity(), 3, "SIX Base Building", "You have been successfully added!", "set:ccgui_enforce image:Icon40Move");
        }
    }
};