/**
 * construction.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Sep 19 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class Construction
{   
    /*
    * Force the building of a specific part on a construction item
    * @note Does not run any checks what so ever so be carefull when using this 
    */
    void ForceBuildPart(Man player, string part) {

        // Destroy any collision triiger objects, No invisible walls etc
        if ( m_ConstructionBoxTrigger ) {
            DestroyCollisionTrigger();
        }

        // Build the part
		GetParent().OnPartBuiltServer(player, part, AT_BUILD_PART);
        
        UpdateVisuals();
    }
}