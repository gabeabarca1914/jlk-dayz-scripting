/**
 * Hologram.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */
modded class Hologram
{
    protected bool m_SnapActivated = true;

    ref array< Object> m_CargoObjects = new array< Object >;
	ref array< CargoBase > m_ProxyObjects = new array< CargoBase >;

    ref array< string > m_SnappingKits = new array< string >;
    ref array< string > m_SnappingTargets = new array< string >;

    ref array< string > m_SnappingHalfWalls  = new array< string >;
    ref array< string > m_SnappingWalls = new array< string >;
    ref array< string > m_SnappingFloors = new array< string >;

    ref array<Object> m_SnappingPointHelpers = new array<Object>;

    protected vector m_OffsetOrientation;

    //--- This is the object we snap to the root - cleetus
    Object SnapRoot;

    void Hologram( PlayerBase player, vector pos, ItemBase item )
	{
        m_Rotation = "180 0 0";

        //--- Kits that will snapp to other objects
        m_SnappingKits.Insert( "T1_DoorKit" );
        m_SnappingKits.Insert( "T1_DoubleDoorKit" );
        m_SnappingKits.Insert( "T1_FloorKit" );
        m_SnappingKits.Insert( "T1_GateKit" );
        m_SnappingKits.Insert( "T1_HalfWallKit" );
        m_SnappingKits.Insert( "T1_HatchKit" );
        m_SnappingKits.Insert( "T1_RampKit" );
        m_SnappingKits.Insert( "T1_RoofKit" );
        m_SnappingKits.Insert( "T1_StairsKit" );
        m_SnappingKits.Insert( "T1_WallKit" );
        m_SnappingKits.Insert( "T1_WindowKit" );
        m_SnappingKits.Insert( "T2_DoorKit" );
        m_SnappingKits.Insert( "T2_DoubleDoorKit" );
        m_SnappingKits.Insert( "T2_FloorKit" );
        m_SnappingKits.Insert( "T2_GateKit" );
        m_SnappingKits.Insert( "T2_HalfWallKit" );
        m_SnappingKits.Insert( "T2_HatchKit" );
        m_SnappingKits.Insert( "T2_RampKit" );
        m_SnappingKits.Insert( "T2_RoofKit" );
        m_SnappingKits.Insert( "T2_StairsKit" );
        m_SnappingKits.Insert( "T2_WallKit" );
        m_SnappingKits.Insert( "T2_WindowKit" );
        m_SnappingKits.Insert( "T3_DoorKit" );
        m_SnappingKits.Insert( "T3_DoubleDoorKit" );
        m_SnappingKits.Insert( "T3_FloorKit" );
        m_SnappingKits.Insert( "T3_GateKit" );
        m_SnappingKits.Insert( "T3_HalfWallKit" );
        m_SnappingKits.Insert( "T3_HatchKit" );
        m_SnappingKits.Insert( "T3_RampKit" );
        m_SnappingKits.Insert( "T3_RoofKit" );
        m_SnappingKits.Insert( "T3_StairsKit" );
        m_SnappingKits.Insert( "T3_WallKit" );
        m_SnappingKits.Insert( "T3_WindowKit" );

        //--- Objects that can be snapped to each other
        m_SnappingTargets.Insert( "T1_Door" );
        m_SnappingTargets.Insert( "T1_DoubleDoor" );
        m_SnappingTargets.Insert( "T1_Floor" );
        m_SnappingTargets.Insert( "T1_Gate" );
        m_SnappingTargets.Insert( "T1_HalfWall" );
        m_SnappingTargets.Insert( "T1_Hatch" );
        m_SnappingTargets.Insert( "T1_Ramp" );
        m_SnappingTargets.Insert( "T1_Roof" );
        m_SnappingTargets.Insert( "T1_Stairs" );
        m_SnappingTargets.Insert( "T1_Wall" );
        m_SnappingTargets.Insert( "T1_Window" );
        m_SnappingTargets.Insert( "T2_Door" );
        m_SnappingTargets.Insert( "T2_DoubleDoor" );
        m_SnappingTargets.Insert( "T2_Floor" );
        m_SnappingTargets.Insert( "T2_Gate" );
        m_SnappingTargets.Insert( "T2_HalfWall" );
        m_SnappingTargets.Insert( "T2_Hatch" );
        m_SnappingTargets.Insert( "T2_Ramp" );
        m_SnappingTargets.Insert( "T2_Roof" );
        m_SnappingTargets.Insert( "T2_Stairs" );
        m_SnappingTargets.Insert( "T2_Wall" );
        m_SnappingTargets.Insert( "T2_Window" );
        m_SnappingTargets.Insert( "T3_Door" );
        m_SnappingTargets.Insert( "T3_DoubleDoor" );
        m_SnappingTargets.Insert( "T3_Floor" );
        m_SnappingTargets.Insert( "T3_Gate" );
        m_SnappingTargets.Insert( "T3_HalfWall" );
        m_SnappingTargets.Insert( "T3_Hatch" );
        m_SnappingTargets.Insert( "T3_Ramp" );
        m_SnappingTargets.Insert( "T3_Roof" );
        m_SnappingTargets.Insert( "T3_Stairs" );
        m_SnappingTargets.Insert( "T3_Wall" );
        m_SnappingTargets.Insert( "T3_Window" );

        //--- Floors that can be snapped to each other
        m_SnappingFloors.Insert( "T1_Floor" );
        m_SnappingFloors.Insert( "T2_Floor" );
        m_SnappingFloors.Insert( "T3_Floor" );
        m_SnappingFloors.Insert( "T1_Roof" );
        m_SnappingFloors.Insert( "T2_Roof" );
        m_SnappingFloors.Insert( "T3_Roof" );
        m_SnappingFloors.Insert( "T1_Hatch" );
        m_SnappingFloors.Insert( "T2_Hatch" );
        m_SnappingFloors.Insert( "T3_Hatch" );

        //--- Walls that can be snapped to each other
        m_SnappingWalls.Insert( "T1_Door" );
        m_SnappingWalls.Insert( "T1_DoubleDoor" );
        m_SnappingWalls.Insert( "T2_Door" );
        m_SnappingWalls.Insert( "T2_DoubleDoor" );
        m_SnappingWalls.Insert( "T3_Door" );
        m_SnappingWalls.Insert( "T3_DoubleDoor" );
        m_SnappingWalls.Insert( "T1_HalfWall" );
        m_SnappingWalls.Insert( "T1_Wall" );
        m_SnappingWalls.Insert( "T2_HalfWall" );
        m_SnappingWalls.Insert( "T2_Wall" );
        m_SnappingWalls.Insert( "T3_HalfWall" );
        m_SnappingWalls.Insert( "T3_Wall" );
        m_SnappingWalls.Insert( "T1_Window" );
        m_SnappingWalls.Insert( "T2_Window" );
        m_SnappingWalls.Insert( "T3_Window" );
        m_SnappingWalls.Insert( "T1_Gate" );
        m_SnappingWalls.Insert( "T2_Gate" );
        m_SnappingWalls.Insert( "T3_Gate" );
    }

    void ~Hologram() 
    {
        foreach (Object ball: m_SnappingPointHelpers)
            GetGame().ObjectDelete(ball);
        
        m_SnappingPointHelpers.Clear();
    }

    override void UpdateHologram( float timeslice )
	{
        m_SnapActivated = true;

        if (m_Projection.IsInherited(HDSN_BreachingChargeBase))
		{
			if ( !m_Parent )
			{
				GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Call(m_Player.TogglePlacingLocal);
				
				return;
			}
			
			if ( m_Player.IsSwimming() || m_Player.IsClimbingLadder() || m_Player.IsRaised() )
			{
				GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Call(m_Player.TogglePlacingLocal);
				
				return;
			}
			
			EvaluateCollision();
			RefreshTrigger();
			CheckPowerSource();	
			RefreshVisual();
	
			if (!GetUpdatePosition())
			{
				return;
			} 
			
			if (HDSN_BreachingChargeBase.Cast(m_Parent).HasTarget())
			{
				if (hdsn_actionDeployCharge)
				{
					m_Projection.SetPosition(hdsn_actionDeployCharge.GetContactPos());
					
					vector chargeDirection = hdsn_actionDeployCharge.GetContactDir().Normalized();										
					float angle = HDSN_MiscFunctions.ViewDirectionToChargeZAngle(GetGame().GetCurrentCameraDirection());
					
					if(HDSN_MiscFunctions.IsRoughlySameVector(chargeDirection, Vector(0.0, 1.0, 0.0)))																				
						m_Projection.SetOrientation(Vector(0.00, 90.00, angle));
					else if(HDSN_MiscFunctions.IsRoughlySameVector(chargeDirection, Vector(0.0, -1.0, 0.0)))
						m_Projection.SetOrientation(Vector(0.00, -90.0, -angle));
					else
						m_Projection.SetOrientation(hdsn_actionDeployCharge.GetContactDir().VectorToAngles());
				}
			}
			else
				SetProjectionPosition(vector.Zero);				
		}
        else
        {
            if ( !CanSnap() || !m_SnapActivated )
            {
                if ( !m_Parent )
                {
                    GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Call(m_Player.TogglePlacingLocal);
                    return;
                }
                
                if ( m_Player.IsSwimming() || m_Player.IsClimbingLadder() || m_Player.IsRaised() || m_Player.IsClimbing() || m_Player.IsRestrained() || m_Player.IsUnconscious() )
                {
                    GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Call(m_Player.TogglePlacingLocal);
                    return;
                }
                
                EvaluateCollision();
                RefreshTrigger();
                CheckPowerSource();	
                RefreshVisual();

                if ( !GetUpdatePosition() ) return;

                //--- update hologram position	
                SetProjectionPosition(GetProjectionEntityPosition(m_Player));
                SetProjectionOrientation(AlignProjectionOnTerrain(timeslice));		
                m_Projection.OnHologramBeingPlaced(m_Player);

                foreach (Object ball: m_SnappingPointHelpers)
                    GetGame().ObjectDelete(ball);
                
                m_SnappingPointHelpers.Clear();
            }
            else 
                UpdateSnapping(timeslice);   
        }
    }

    void UpdateSnapping( float timeslice )
    {
        Object target = GetCollidingObject();

        Input input = GetGame().GetInput();

        if ( input )
        {
            ItemBase item_in_hands = ItemBase.Cast( m_Player.GetHumanInventory().GetEntityInHands() );

            if( input.LocalRelease( "UANextAction", false ) || input.LocalRelease( "UAPrevAction", false ) )
		    {
                string item_in_hands_type = item_in_hands.GetType();
                item_in_hands_type.Replace( "Kit", "" );

                if ( m_SnappingWalls.Find(item_in_hands_type) != -1 ) m_OffsetOrientation += "180 0 0";
                if ( m_SnappingFloors.Find(item_in_hands_type) != -1 ) m_OffsetOrientation += "90 0 0";
            }
        }
        
        if ( SnapRoot && m_SnappingTargets.Find(SnapRoot.GetType()) != -1 )
            if ( !HandleSnap() )
                m_SnapActivated = false;
    }

    bool CanSnap()
    {
        Input input = GetGame().GetInput();
        ItemBase item_in_hands = ItemBase.Cast( m_Player.GetHumanInventory().GetEntityInHands() );

        if ( item_in_hands && m_SnappingKits.Find( item_in_hands.GetType() ) != -1 )
        {
            if (input.LocalRelease("Loaded_BaseBuilding_SnapToObject", false))
            {
                foreach (Object ball: m_SnappingPointHelpers)
                    GetGame().ObjectDelete(ball);
                
                m_SnappingPointHelpers.Clear();

                SnapRoot = GetCollidingObject();
            }

            if (SnapRoot)
                if (GetBaseBuildingManagerClient().GetBuildingMode() == BuildingMode.SNAP) return true;
        }

        return false;
    }

    //1 = -left +right
    //2 = -up +down
    //3 = -back +forward
    void Snap( vector position, vector orientation )
    {
		if ( !m_Parent || m_Player.IsSwimming() || m_Player.IsClimbingLadder() || m_Player.IsRaised() || m_Player.IsClimbing() || m_Player.IsRestrained() || m_Player.IsUnconscious() )
		{
			GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).Call( m_Player.TogglePlacingLocal );
			return;
		}

		EvaluateCollision();
		RefreshTrigger();
		CheckPowerSource();	
		RefreshVisual();

		if ( !GetUpdatePosition() ) return;

        vector offsetPosition = (SnapRoot.WorldToModel( SnapRoot.GetPosition() ) - position);
        vector targetPosition = SnapRoot.ModelToWorld( offsetPosition );
        vector targetOrientation = (SnapRoot.GetOrientation() - orientation);
        m_Projection.SetPosition( targetPosition );	
		m_Projection.SetOrientation( targetOrientation + m_OffsetOrientation );	
    }

    bool HandleSnap()
    {
        if (!SnapRoot) return false;

        ItemBase item_in_hands = ItemBase.Cast( m_Player.GetHumanInventory().GetEntityInHands() );
        
        if ( item_in_hands )
        {
            string item_in_hands_type = item_in_hands.GetType();
            item_in_hands_type.Replace( "Kit", "" );

            if ( m_SnappingWalls.Find( item_in_hands_type ) != -1 && item_in_hands.GetType().Contains( "Kit" ) )
                if ( SnapWall() ) return true;

            if ( m_SnappingFloors.Find( item_in_hands_type ) != -1 && item_in_hands.GetType().Contains( "Kit" ) )
                if ( SnapFloor() ) return true;

            if ( item_in_hands.GetType().Contains( "Stairs" ) )
                if ( SnapStairs() ) return true;
        }
        
        return false;
    }

    bool SnapStairs()
    {
        if (!GetCollidingObject()) return false;

        if ( m_SnappingWalls.Find( GetCollidingObject().GetType() ) != -1 )
            if ( SnapStairsToWall() ) return true;

        if ( m_SnappingFloors.Find( GetCollidingObject().GetType() ) != -1 && GetCollidingObject().GetType().Contains("Hatch"))
            if ( SnapStairsToHatch() ) return true;

        if ( m_SnappingFloors.Find( GetCollidingObject().GetType() ) != -1 )
            if ( SnapStairsToFloor() ) return true;
        
        return false;
    }

    bool SnapStairsToWall()
    {
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_9  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_9"));
        vector SnappingPoint_10  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_10"));

        float Point_1 = vector.Distance( GetCursorPosition(), SnappingPoint_1 );
        float Point_9 = vector.Distance( GetGame().GetPlayer().GetPosition(), SnappingPoint_9 );
        float Point_10 = vector.Distance( GetGame().GetPlayer().GetPosition(), SnappingPoint_10 );

        SnapPointArray = {SnappingPoint_1, SnappingPoint_9, SnappingPoint_10};

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                //pos[1] = GetGame().SurfaceY(pos[0], pos[2]);
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        if (SnapCondition(Point_1, 1))
        {
            if (SnapCondition(Point_9, Point_10 ))
            {
                Print("1");
                Snap( "0 0 -1.70", "0 0 0" );
                return true;
            }

            if ( SnapCondition(Point_10, Point_9 ) )
            {
                Print("2");
                Snap( "0 0 1.70", "180 0 0" );
                return true;
            }
        }

        return false;
    }

    bool SnapStairsToFloor()
    {
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_2  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_2"));
        vector SnappingPoint_3  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_3"));
        vector SnappingPoint_4  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_4"));

        float Point_1 = vector.Distance( GetCursorPosition(), SnappingPoint_1 );
        float Point_2 = vector.Distance( GetCursorPosition(), SnappingPoint_2 );
        float Point_3 = vector.Distance( GetCursorPosition(), SnappingPoint_3 );
        float Point_4 = vector.Distance( GetCursorPosition(), SnappingPoint_4 );

        SnapPointArray = {SnappingPoint_1, SnappingPoint_2, SnappingPoint_3, SnappingPoint_4};

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                //pos[1] = GetGame().SurfaceY(pos[0], pos[2]);
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        if ( SnapCondition(Point_1, Point_2, Point_3, Point_4) )
        {
            Print("1");
            Snap( "0 2.75 -4.65", "0 0 0" );
            
            return true;
        }

        if ( SnapCondition(Point_2, Point_1, Point_3, Point_4) )
        {
            Print("2");
            Snap( "-4.5 2.75 0", "-90 0 0" );
            return true;
        }

        if ( SnapCondition(Point_3, Point_1, Point_2, Point_4) )
        {
            Print("3");
            Snap( "0 2.75 4.5", "180 0 0" );
            
            return true;
        }

        if ( SnapCondition(Point_4, Point_1, Point_2, Point_3) )
        {
            Print("4");
            Snap( "4.5 2.75 0", "90 0 0" );
            return true;
        }

        return false;
    }

    bool SnapStairsToHatch()
    {
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_2  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_2"));
        vector SnappingPoint_3  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_3"));
        vector SnappingPoint_4  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_4"));
        vector SnappingPoint_5  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_5"));
        vector SnappingPoint_6  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_6"));

        float Point_1 = vector.Distance( GetCursorPosition(), SnappingPoint_1 );
        float Point_2 = vector.Distance( GetCursorPosition(), SnappingPoint_2 );
        float Point_3 = vector.Distance( GetCursorPosition(), SnappingPoint_3 );
        float Point_4 = vector.Distance( GetCursorPosition(), SnappingPoint_4 );
        float Point_5 = vector.Distance( GetCursorPosition(), SnappingPoint_5 );
        float Point_6 = vector.Distance( GetCursorPosition(), SnappingPoint_6 );

        SnapPointArray = {SnappingPoint_1, SnappingPoint_2, SnappingPoint_3, SnappingPoint_4, SnappingPoint_5, SnappingPoint_6};

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                //pos[1] = GetGame().SurfaceY(pos[0], pos[2]);
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        if ( SnapCondition(Point_1, Point_2, Point_3, Point_4, Point_5, Point_6) )
        {
            Snap( "0 2.75 -4.65", "0 0 0" );
            return true;
        }

        if ( SnapCondition(Point_2, Point_1, Point_3, Point_4, Point_5, Point_6) )
        {
            Snap( "-4.5 2.75 0", "-90 0 0" );
            return true;
        }

        if ( SnapCondition(Point_3, Point_1, Point_2, Point_4, Point_5, Point_6) )
        {
            Snap( "0 2.75 4.5", "180 0 0" );
            return true;
        }

        if ( SnapCondition(Point_4, Point_1, Point_2, Point_3, Point_5, Point_6) )
        {
            Snap( "4.5 2.75 0", "90 0 0" );
            return true;
        }

        if ( SnapCondition(Point_5, Point_1, Point_2, Point_3, Point_4, Point_6) )
        {
            Snap( "0 2.77 0.5", "180 0 0" );
            return true;
        }

        if ( SnapCondition(Point_6, Point_1, Point_2, Point_3, Point_4, Point_5) )
        {
            Snap( "0 2.77 -0.5", "0 0 0" );
            return true;
        }

        return false;
    }

    bool SnapWall()
    {
        if ( m_SnappingWalls.Find( SnapRoot.GetType() ) != -1 )
            if ( SnapWallToWall() ) return true;

        if ( m_SnappingFloors.Find( SnapRoot.GetType() ) != -1 )
            if ( SnapWallToFloor() ) return true;
        
        return false;
    }

    bool SnapWallToWall()
    {
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_2  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_2"));
        vector SnappingPoint_3  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_3"));
        vector SnappingPoint_4  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_4"));
        vector SnappingPoint_5  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_5"));
        vector SnappingPoint_6  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_6"));
        vector SnappingPoint_7  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_7"));
        vector SnappingPoint_8  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_8"));
        vector SnappingPoint_9  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_9"));
        vector SnappingPoint_10 = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_10"));
        vector SnappingPoint_11 = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_11"));
        vector SnappingPoint_12 = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_12"));
        vector SnappingPoint_13 = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_13"));
        vector SnappingPoint_14 = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_14"));

        float Point_1 = vector.Distance( GetCursorPosition(), SnappingPoint_1 );
        float Point_2 = vector.Distance( GetCursorPosition(), SnappingPoint_2 );
        float Point_3 = vector.Distance( GetCursorPosition(), SnappingPoint_3 );
        float Point_4 = vector.Distance( GetCursorPosition(), SnappingPoint_4 );
        float Point_5 = vector.Distance( GetCursorPosition(), SnappingPoint_5 );
        float Point_6 = vector.Distance( GetCursorPosition(), SnappingPoint_6 );
        float Point_7 = vector.Distance( GetCursorPosition(), SnappingPoint_7 );
        float Point_8 = vector.Distance( GetCursorPosition(), SnappingPoint_8 );
        float Point_9 = vector.Distance( GetCursorPosition(), SnappingPoint_9 );
        float Point_10 = vector.Distance( GetCursorPosition(), SnappingPoint_10 );
        float Point_11 = vector.Distance( GetCursorPosition(), SnappingPoint_11 );
        float Point_12 = vector.Distance( GetCursorPosition(), SnappingPoint_12 );
        float Point_13 = vector.Distance( GetCursorPosition(), SnappingPoint_13 );
        float Point_14 = vector.Distance( GetCursorPosition(), SnappingPoint_14 );

        SnapPointArray = {SnappingPoint_1, SnappingPoint_2, SnappingPoint_3, SnappingPoint_4, SnappingPoint_5, SnappingPoint_6, SnappingPoint_7, SnappingPoint_8, SnappingPoint_9, SnappingPoint_10, SnappingPoint_11, SnappingPoint_12, SnappingPoint_13, SnappingPoint_14 };

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                //pos[1] = GetGame().SurfaceY(pos[0], pos[2]);
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        if (SnapCondition(Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("0 -3.048 0","0 0 0");
            return true;
        }
        
        if (SnapCondition(Point_2, Point_1, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("0 -3 0","0 0 0");
            return true;
        }

        if (SnapCondition(Point_3, Point_1, Point_2, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("-6 0 0","0 0 0");
            return true;
        }

        if (SnapCondition(Point_4, Point_1, Point_2, Point_3, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("6 0 0","0 0 0");
            return true;
        }

        //good - front right
        if (SnapCondition(Point_5, Point_1, Point_2, Point_3, Point_4, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("3 0 3","-90 0 0");
            return true;
        }

        //good back right
        if (SnapCondition(Point_6, Point_1, Point_2, Point_3, Point_4, Point_5, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("3 0 -3","-90 0 0");
            return true;
        }

        //good front left
        if (SnapCondition(Point_7, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("-3 0 3","90 0 0");
            return true;
        }

        //same side opposite
        if (SnapCondition(Point_8, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_9, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("-3 0 -3","90 0 0");
            return true;
        }
        
        if (SnapCondition(Point_9, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_10, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("0 0 -6","0 0 0");
            return true;
        }

        if (SnapCondition(Point_10, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_11, Point_12, Point_13, Point_14))
        {
            Snap("0 0 6","0 0 0");
            return true;
        }

        //--- 45%
        if (SnapCondition(Point_11, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_12, Point_13, Point_14))
        {
            Snap("5 0 2","45 0 0");
            return true;
        }

        if (SnapCondition(Point_12, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_13, Point_14))
        {
            Snap("5 0 -2","-45 0 0");
            return true;
        }

        if (SnapCondition(Point_13, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_14))
        {
            Snap("-5 0 -2","45 0 0");
            return true;
        }

        if (SnapCondition(Point_14, Point_1, Point_2, Point_3, Point_4, Point_5, Point_6, Point_7, Point_8, Point_9, Point_10, Point_11, Point_12, Point_13))
        {
            Snap("-5 0 2","-45 0 0");
            return true;
        }

        return false;
    }

    bool SnapWallToFloor()
    { 
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_2  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_2"));
        vector SnappingPoint_3  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_3"));
        vector SnappingPoint_4  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_4"));
        vector SnappingPoint_5  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_5"));  //--- above
        vector SnappingPoint_6  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_6"));  //--- below

        float Point_1 = vector.Distance(GetCursorPosition(), SnappingPoint_1);
        float Point_2 = vector.Distance(GetCursorPosition(), SnappingPoint_2);
        float Point_3 = vector.Distance(GetCursorPosition(), SnappingPoint_3);
        float Point_4 = vector.Distance(GetCursorPosition(), SnappingPoint_4);
        float Point_5 = vector.Distance(GetGame().GetPlayer().GetPosition(), SnappingPoint_5);
        float Point_6 = vector.Distance(GetGame().GetPlayer().GetPosition(), SnappingPoint_6);

        SnapPointArray = {SnappingPoint_1, SnappingPoint_2, SnappingPoint_3, SnappingPoint_4, SnappingPoint_5, SnappingPoint_6};

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        //--- above
        if (SnapCondition(Point_5, Point_6))
        {
            if ( SnapCondition(Point_1, Point_2, Point_3, Point_4) )
            {
                Snap( "0 -0.1 -3", "0 0 0" );
                return true;
            }

            if ( SnapCondition(Point_2, Point_1, Point_3, Point_4) )
            {
                Snap( "-3 -0.1 0", "-90 0 0" );
                return true;
            }

            if ( SnapCondition(Point_3, Point_1, Point_2, Point_4) )
            {
                Snap( "0 -0.1 3", "0 0 0" );
                return true;
            }

            if ( SnapCondition(Point_4, Point_1, Point_2, Point_3) )
            {
                Snap( "3 -0.1 0", "-90 0 0" );
                return true;
            }
        }
        else
        {
            if ( SnapCondition(Point_1, Point_2, Point_3, Point_4) )
            {
                Snap( "0 3 -3", "0 0 0" );
                return true;
            }

            if ( SnapCondition(Point_2, Point_1, Point_3, Point_4) )
            {
                Snap( "-3 3 0", "-90 0 0" );
                return true;
            }

            if ( SnapCondition(Point_3, Point_1, Point_2, Point_4) )
            {
                Snap( "0 3 3", "0 0 0" );
                return true;
            }

            if ( SnapCondition(Point_4, Point_1, Point_2, Point_3) )
            {
                Snap( "3 3 0", "-90 0 0" );
                return true;
            }
        }

        return false;
    }

    bool SnapFloor()
    {
        if (!GetCollidingObject()) return false;

        if ( m_SnappingWalls.Find( GetCollidingObject().GetType() ) != -1 )
            if ( SnapFloorToWall() ) return true;

        if ( m_SnappingFloors.Find( GetCollidingObject().GetType() ) != -1 )
            if ( SnapFloorToFloor() ) return true;
        
        return false;
    }

    bool SnapFloorToWall()
    {
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_2  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_2"));
        vector SnappingPoint_9  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_9"));
        vector SnappingPoint_10  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_10"));

        float Point_1 = vector.Distance(GetCursorPosition(), SnappingPoint_1);
        float Point_2 = vector.Distance(GetCursorPosition(), SnappingPoint_2);
        float Point_9 = vector.Distance(GetGame().GetPlayer().GetPosition(), SnappingPoint_9);
        float Point_10 = vector.Distance(GetGame().GetPlayer().GetPosition(), SnappingPoint_10);

        SnapPointArray = {SnappingPoint_1, SnappingPoint_2, SnappingPoint_9, SnappingPoint_10};

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        //--- Front
        if (SnapCondition(Point_9, Point_10))
        {
            if (SnapCondition(Point_1, Point_2))
            {
                Snap("0 -3 -3", "0 0 0");
                return true;
            }

            if (SnapCondition(Point_2, Point_1))
            {
                Snap("0 0 -3", "0 0 0");
                return true;
            }
        }
        else
        {
            if (SnapCondition(Point_1, Point_2))
            {
                Snap("0 -3 3", "0 0 0");
                return true;
            }

            if (SnapCondition(Point_2, Point_1))
            {
                Snap("0 0 3", "0 0 0");
                return true;
            }            
        }

        return false;
    }

    bool SnapFloorToFloor()
    {
        if (!SnapRoot) return false;

        TVectorArray SnapPointArray = new TVectorArray;

        vector SnappingPoint_1  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_1"));
        vector SnappingPoint_2  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_2"));
        vector SnappingPoint_3  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_3"));
        vector SnappingPoint_4  = SnapRoot.ModelToWorld(SnapRoot.GetMemoryPointPos("snapping_4"));

        float Point_1 = vector.Distance(GetCursorPosition(), SnappingPoint_1);
        float Point_2 = vector.Distance(GetCursorPosition(), SnappingPoint_2);
        float Point_3 = vector.Distance(GetCursorPosition(), SnappingPoint_3);
        float Point_4 = vector.Distance(GetCursorPosition(), SnappingPoint_4);

        SnapPointArray = {SnappingPoint_1, SnappingPoint_2, SnappingPoint_3, SnappingPoint_4};

        if (m_SnappingPointHelpers.Count() == 0)
        {
            foreach(vector pos: SnapPointArray)
            {
                Object bally = GetGame().CreateObject("SmallMarker", pos, true);
                bally.SetPosition(pos);
                m_SnappingPointHelpers.Insert(bally);
            }
        }

        if ( SnapCondition(Point_1, Point_2, Point_3, Point_4) )
        {
            Print("1");
            Snap( "0 0 -6.1", "0 0 0" );
            return true;
        }
        
        if ( SnapCondition(Point_2, Point_1, Point_3, Point_4) )
        {
            Print("2");
            Snap( "-6.1 0 0", "0 0 0" );
            return true;
        }

        if ( SnapCondition(Point_3, Point_1, Point_2, Point_4) )
        {
            Print("3");
            
            Snap( "0 0 6.1", "0 0 0" );
            return true;
        }

        if ( SnapCondition(Point_4, Point_1, Point_2, Point_3) )
        {
            Print("4");
            Snap( "6.1 0 0", "0 0 0" );
            return true;
        }

        return false;
    }

    bool SnapCondition( float dir, float dir_0 = 100, float dir_1 = 100, float dir_2 = 100, float dir_3 = 100, float dir_4 = 100, float dir_5 = 100, float dir_6 = 100, 
    float dir_7 = 100, float dir_8 = 100, float dir_9 = 100, float dir_10 = 100, float dir_11 = 100, float dir_12 = 100, float dir_13 = 100, float dir_14 = 100 )
    {
        if ( dir > dir_0 ) return false;
        if ( dir > dir_1 ) return false;
        if ( dir > dir_2 ) return false;
        if ( dir > dir_3 ) return false;
        if ( dir > dir_4 ) return false;
        if ( dir > dir_5 ) return false;
        if ( dir > dir_6 ) return false;
        if ( dir > dir_7 ) return false;
        if ( dir > dir_8 ) return false;
        if ( dir > dir_9 ) return false;
        if ( dir > dir_10 ) return false;
        if ( dir > dir_11 ) return false;
        if ( dir > dir_12 ) return false;
        if ( dir > dir_13 ) return false;
        if ( dir > dir_14 ) return false;
        
        return true;
    }

    void SnapBall(vector Poffset, out vector position) 
    {
        vector snapPosition = (SnapRoot.WorldToModel(SnapRoot.GetPosition()) - Poffset);
        position = SnapRoot.ModelToWorld(snapPosition);
    }

    Object GetCollidingObjectRaycast()
    {
        vector rayStart = GetGame().GetCurrentCameraPosition();
        vector rayEnd = rayStart + GetGame().GetCurrentCameraDirection() * 10000;

        set<Object> geom = new set<Object>;
        set<Object> view = new set<Object>;

        DayZPhysics.RaycastRV(rayStart, rayEnd, NULL, NULL, NULL, geom, GetGame().GetPlayer(), m_Projection, false, false, ObjIntersectGeom, 0.25);
        DayZPhysics.RaycastRV(rayStart, rayEnd, NULL, NULL, NULL, view, GetGame().GetPlayer(), m_Projection, false, false, ObjIntersectView, 0.25);

        array<Object> colliding = new array<Object>;
        for (int g = 0; g < geom.Count(); g++)
        {
            if (!geom[g].IsInherited(Loaded_BaseBuildingObject)) continue;

            colliding.Insert(geom[g]);
        }

        for (int v = 0; v < view.Count(); v++)
        {
            if (!view[v].IsInherited(Loaded_BaseBuildingObject)) continue;

            colliding.Insert(view[v]);
        }

        if (colliding.Count() > 0) return colliding[0];

        return NULL;
    }

    Object GetCollidingObjectAt()
    {
        array< Object > objects = new array< Object >;
		array< CargoBase > proxyCargos = new array< CargoBase >;
        GetGame().GetObjectsAtPosition3D( GetCursorPosition( 1 ), 5, objects, proxyCargos );

        array< Object > colliding = new array< Object >;
        for ( int i = 0; i < objects.Count(); i++ )
        {
            if ( !objects[i].IsInherited( Loaded_BaseBuildingObject ) )
            {
                continue;
            }

            if ( objects[i].GetType().Contains( "Floor" ) )
            {
                continue;
            }

            colliding.Insert( objects[i] );
        }

        if ( colliding.Count() > 0 )
        {
            return colliding[0];
        }

        return NULL;
    }

    Object GetCollidingObject() return GetCollidingObjectRaycast();

    vector GetCursorPosition( float distance = 2.5 )
    {
        PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
        
        vector contactPosition;
        vector contactDirection;

        vector start = GetGame().GetCurrentCameraPosition();
        float camera_to_player_distance = vector.Distance( GetGame().GetCurrentCameraPosition(), player.GetPosition() );
        vector end = start + ( GetGame().GetCurrentCameraDirection() * ( distance + camera_to_player_distance ));

        DayZPhysics.RaycastRV( start, end, contactPosition, contactDirection, NULL, NULL, player, player.GetHologramLocal().GetProjectionEntity(), true, false );

        return contactPosition;
    }

    void ModifyProjectionYaw(float modify)
	{
		m_Rotation[1] = m_Rotation[1] + modify;
	}

    void ModifyProjectionRoll(float modify)
	{
		m_Rotation[2] = m_Rotation[2] + modify;
	}

	override void EvaluateCollision( ItemBase action_item = null ) SetIsColliding(false);
}