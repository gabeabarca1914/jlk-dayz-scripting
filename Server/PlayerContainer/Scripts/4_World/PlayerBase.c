modded class PlayerBase
{
	override void Init()
	{
		super.Init();
		
		// Give the player their container if they already have a one, otherwise create a new one
		if (GetGame().IsServer())
		{
			RestoreOrCreatePlayerContainer();
			Print("yes");
		}
	}
	
	// Might need if not working in init
	/*
	bool hasContainerBeenRestored;
	override void OnScheduledTick( float deltaTime )
	{
		super.OnScheduledTick( deltaTime );
		
		// Only do this once per player initialization
		if ( !hasContainerBeenRestored )
		{
			// Give the player their container if they already have a one, otherwise create a new one
			RestoreOrCreatePlayerContainer();
			hasContainerBeenRestored = true;
		}
	}*/
	
	override bool CanDropEntity (notnull EntityAI item)
	{ 
		Alpha_PlayerContainter plrContainer;
		if (CastTo(item, plrContainer))
			return false;
		
		return super.CanDropEntity(item); 
	}
	
	override void EEKilled( Object killer )
	{
		if (GetGame().IsServer())
		{
			Alpha_PlayerContainter plrContainer;
			
			// If the container is not in the attachment slot, it should be in hands, so use it instead
			if ( !CastTo( plrContainer, FindAttachmentBySlotName( "PlayerContainer" ) ) )
				CastTo( plrContainer, GetItemInHands() )
			
			StorePlayerContainer( plrContainer );
		}
		
		super.EEKilled( killer );
	}
	
	void StorePlayerContainer( Alpha_PlayerContainter containerToStore )
	{
		this.PredictiveDropEntity(EntityAI.Cast(containerToStore));
		containerToStore.SetPosition( Vector( 0, 0, 0 ) );
		containerToStore.SetPlayerOwner( GetIdentity().GetId() );
	}
	
	void RestoreOrCreatePlayerContainer()
	{
		Print("RestoreOrCreatePlayerContainer 1");
		// For each container that is stored at the { 0, 0, 0 } position
		foreach ( Alpha_PlayerContainter container: GetAllPlrContainers() )
		{
			// If the container belongs to this player
			if ( container.SetPlayerOwner() == GetIdentity().GetId() )
			{
				Print("RestoreOrCreatePlayerContainer 2");
				// Give the player their container
				GetInventory().TakeEntityAsAttachment( InventoryMode.SERVER, EntityAI.Cast( container ) )
			
				// Save resources by stopping as soon as it's found ( return code if the player had a container )
				return;
			}
		}
		
		Print("RestoreOrCreatePlayerContainer 3");
		// If this point is reached, the player did not have a container stored, so create them a new one
		GetInventory().CreateAttachment( "Alpha_PlayerContainter" );
	}
	
	array<Alpha_PlayerContainter> GetAllPlrContainers()
	{
		array<Object> objects = new array<Object>();
		array<CargoBase> proxyCargos = new array<CargoBase>();
		vector position = Vector( 0, 0, 0 );
		float radius = 1;
		
		GetGame().GetObjectsAtPosition3D( position, radius, objects, proxyCargos );
		
		array<Alpha_PlayerContainter> containers = new array<Alpha_PlayerContainter>();
		
		foreach ( Object obj: objects )
			if ( Alpha_PlayerContainter.Cast( obj ) )
				containers.Insert( Alpha_PlayerContainter.Cast( obj ) );
		
		return containers;
	}
}
