/**
* config.cpp
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

class CfgPatches
{
	class PlayerContainer
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Scripts"
		};
	};
};

class CfgMods
{
	class PlayerContainer
	{
		name = "PlayerContainer";
		dir = "PlayerContainer";
		author = "6IX";
		type = "mod";
		hideName = 1;
		hidePicture = 1;
		dependencies[] = { "World", "Mission" };
		class defs
		{
			class worldScriptModule
			{
				value = "";
				files[] = { "PlayerContainer/Scripts/4_World" };
			};
			
			class missionScriptModule
			{
				value = "";
				files[] = { "PlayerContainer/Scripts/5_Mission" };
			};
		};
	};
};

class cfgVehicles
{
	class Man;
	class Inventory_Base;
	class SmallProtectorCase;
	
	class Alpha_PlayerContainter: SmallProtectorCase
	{
		scope=2;
		displayName="Alpha Container";
		descriptionShort="This is your personal container, use it to store valueables for your bambi reincarnate.";
		//model="\dz\gear\containers\Protector_Case.p3d";
		//rotationFlags=17;
		//weight=980;
		//itemSize[]={3,4};
		//itemsCargoSize[]={3,4};
		//canBeDigged=0;
		//isMeleeWeapon=0;
		//allowOwnedCargoManipulation=1;
		//randomQuantity=2;
		inventorySlot="PlayerContainer";
	};

	class SurvivorBase: Man
	{
		attachments[]=
		{
			"Head",
			"Shoulder",
			"Melee",
			"Headgear",
			"Mask",
			"Eyewear",
			"Hands",
			"LeftHand",
			"Gloves",
			"Armband",
			"Vest",
			"Body",
			"Back",
			"Hips",
			"Legs",
			"Feet",
			"PlayerContainer"
		};
		class InventoryEquipment
		{
			playerSlots[]=
			{
				"Slot_Shoulder",
				"Slot_Melee",
				"Slot_Vest",
				"Slot_Body",
				"Slot_Hips",
				"Slot_Legs",
				"Slot_Back",
				"Slot_Headgear",
				"Slot_Mask",
				"Slot_Eyewear",
				"Slot_Gloves",
				"Slot_Feet",
				"Slot_Armband",
				"Slot_PlayerContainer"
			};
		};
	};
};

class CfgSlots
{
	class Slot_PlayerContainer
	{
		name="PlayerContainer";				// This is the slot name
		displayName="Your Container";		// What the player will see when hovering over the attachment
	};
};