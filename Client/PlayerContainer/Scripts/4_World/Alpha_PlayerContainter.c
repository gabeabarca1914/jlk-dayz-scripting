class Alpha_PlayerContainter: ItemBase
{
	string PlayerOwner;
	
	void Alpha_PlayerContainter() {}
	
	void ~Alpha_PlayerContainter()	{}
	
	string GetPlayerOwner()
	{
		return PlayerOwner;
	}
	
	void SetPlayerOwner( string PlayerOwnerToSet )
	{
		PlayerOwner = PlayerOwnerToSet; 
	}
	
	// Make the player unable to put this item into any cargo
	override bool CanPutInCargo( EntityAI parent )
	{
		// Don't worry, CanPutAsAttachment( EntityAI parent ) still works
		return false;
	}
}