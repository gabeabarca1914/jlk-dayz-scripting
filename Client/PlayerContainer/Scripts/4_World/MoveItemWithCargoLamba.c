

class MoveItemWithCargoLamba: ReplaceItemWithNewLambdaBase
{
	PlayerBase m_Player;
	
	void MoveItemWithCargoLamba( EntityAI old_item, string new_item_type, PlayerBase player )
	{
		m_Player = player;
	}
	
	override void CopyOldPropertiesToNew( notnull EntityAI old_item, EntityAI new_item )
	{
		MiscGameplayFunctions.TransferInventory( old_item, new_item, m_Player )
	}
	
	override protected void CreateNetworkObjectInfo (EntityAI new_item)
	{
		super.CreateNetworkObjectInfo( new_item );
		
		GetGame().RemoteObjectTreeCreate( m_OldItem );
	}
	
	override protected void RemoveOldItemFromLocation()
	{
		m_RemoveFromLocationPassed = true;
	}
	
	override void OnSuccess( EntityAI new_item )
	{
		GetGame().ObjectDelete( m_OldItem );
	}
	
	override protected void DeleteOldEntity()	{}
	
	override protected void UndoRemoveOldItemFromLocation()	{}
}