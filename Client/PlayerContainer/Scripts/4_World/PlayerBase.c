modded class PlayerBase
{
	ref Timer timerNetworkBubble;
	ref InventoryLocation currentIL;
	
	bool hasContainerBeenRestored;
	override void OnScheduledTick( float deltaTime )
	{
		super.OnScheduledTick( deltaTime );
		
		// Only do this once per player initialization
		if (!hasContainerBeenRestored )
		{
			// Give the player their container if they already have a one, otherwise create a new one
			RestoreOrCreatePlayerContainer();
			hasContainerBeenRestored = true;
		}
	}
	
	override bool PhysicalPredictiveDropItem( EntityAI entity, bool heavy_item_only = true )
	{
		if ( entity.GetType() == "Alpha_PlayerContainter" )	return false;
		
		return super.PhysicalPredictiveDropItem( entity, heavy_item_only );
	}
	
	override bool CanDropEntity ( notnull EntityAI item )
	{ 
		Alpha_PlayerContainter plrContainer;
		
		if ( CastTo( plrContainer, item ) )
			return false;
		
		return super.CanDropEntity( item ); 
	}
	
	override void EEKilled( Object killer )
	{
		if (GetGame().IsServer())
		{
			Alpha_PlayerContainter plrContainer;
			
			// If the container is not in the attachment slot, it should be in hands, so use it instead
			if ( !CastTo( plrContainer, FindAttachmentBySlotName( "PlayerContainer" ) ) )
				CastTo( plrContainer, GetItemInHands() )
			
			StorePlayerContainer( plrContainer );
		}
		
		super.EEKilled( killer );
	}
	
	void StorePlayerContainer( Alpha_PlayerContainter containerToStore )
	{
		ServerDropEntity(EntityAI.Cast(containerToStore));
		containerToStore.SetPosition( Vector( 0, 0, 0 ) );
		containerToStore.SetPlayerOwner( GetIdentity().GetId() );
	}
	
	void RestoreOrCreatePlayerContainer()
	{
		auto containers = new array<Alpha_PlayerContainter>;
		containers = GetAllPlrContainers();
		
		// For each container that is stored at the { 0, 0, 0 } position
		foreach ( Alpha_PlayerContainter container: containers )
		{
			// Return if the container doesn't belong to this player
			if ( container.GetPlayerOwner() != GetIdentity().GetId() )	continue;
			
			// Move container to player's network bubble ( May not be necessary, not tested )
			container.SetPosition( GetPosition() );
			
			// Use lamba to replace the player container ( serverside )
			if ( GetGame().IsServer() )
			{
				InventoryLocation targetIL = new InventoryLocation;
				bool found = GetInventory().FindFirstFreeLocationForNewEntity( container.GetType(), FindInventoryLocationType.ANY, targetIL );
				if ( found )
				{
					MoveItemWithCargoLamba lambda = new MoveItemWithCargoLamba( container, container.GetType(), this );
					lambda.OverrideNewLocation( targetIL );
					ServerReplaceItemWithNew( lambda );
				}
			}
			
			return;
		}
		
		// If this point is reached, the player did not have a container stored, so create them a new one
		GetInventory().CreateAttachment( "Alpha_PlayerContainter" );
	}
	
	array<Alpha_PlayerContainter> GetAllPlrContainers()
	{
		auto objects = new array<Object>;
		auto proxyCargos = new array<CargoBase>;
		vector position = Vector( 0, 0, 0 );
		float radius = 1;
		
		GetGame().GetObjectsAtPosition3D( position, radius, objects, proxyCargos );
		
		auto containers = new array<Alpha_PlayerContainter>();
		
		foreach ( Object obj: objects )
			if ( Alpha_PlayerContainter.Cast( obj ) )
				containers.Insert( Alpha_PlayerContainter.Cast( obj ) );
		
		return containers;
	}
}