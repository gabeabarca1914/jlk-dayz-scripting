
// This will prevent throwing of the player container
modded class HandActionThrow
{
	override void Action( HandEventBase e )
	{
		HandEventThrow throwEvent = HandEventThrow.Cast( e );

		GameInventory.LocationSyncMoveEntity( e.GetSrc(), e.GetDst() );

		DayZPlayer player = DayZPlayer.Cast( e.m_Player );

		if ( player.GetInstanceType() != DayZPlayerInstanceType.INSTANCETYPE_REMOTE )
		{
			InventoryItem item = InventoryItem.Cast( e.GetSrcEntity() );
			
			if( item && !item.GetType( "Alpha_PlayerContainter" ) )
			{
				item.ThrowPhysically(player, throwEvent.GetForce());
			}
		}

		player.OnItemInHandsChanged();
	}
};