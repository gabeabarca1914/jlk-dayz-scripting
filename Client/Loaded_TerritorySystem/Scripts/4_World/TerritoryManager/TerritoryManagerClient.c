class TerritoryManagerClient 
{
    //--- Cause dayz sucks and i wanna get my info.
    string PlayerUID;

    //--- Territories that player is added to.
    ref array<ref Loaded_Territory> Territories = new array<ref Loaded_Territory>;
    //--- Array of connected players, make sure to only use this for Territory Management UI!!
    ref array<ref LoadedTerritoryMember> AllServerPlayers = new array<ref LoadedTerritoryMember>;
    //--- Territory Invites
    ref array<ref LoadedTerritoryInvite> Invites = new array<ref LoadedTerritoryInvite>;
    //--- Territory System Config
    ref LoadedTerritoryConfig Config;
    //--- Territory Flag Config
    ref LoadedTerritoryFlagsConfig FlagConfig;
    //--- Territory Markers Array
    ref array<ref BasicMapCircleMarker> TerritoryMarkers = new array<ref BasicMapCircleMarker>;
    //--- Trigger Territory
    ref Loaded_Territory TriggerTerritory;

    void TerritoryManagerClient() 
    {
        GetRPCManager().AddRPC("Loaded_Territory_Client", "CreateTerritoryResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "TerritoryCheckResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "GetAllPlayerListResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "UpdateTerritoryDataResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "AddToTerritoryResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "RemoveFromTerritoryResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "PayTerritoryResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "UpgradeTerritoryResponse", this);
        GetRPCManager().AddRPC("Loaded_Territory_Client", "UpdateTriggerTerritoryResponse", this);

        GetRPCManager().AddRPC("Loaded_Territory_Client", "AddInviteResponse", this);

        //--- Send request to server to check if your added to any territories
        GetRPCManager().SendRPC("Loaded_Territory_Server", "TerritoryCheckRequest", null, true, null);
    }

    void CreateTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        NotificationSystem.AddNotificationExtended(3, "Territory Creation", string.Format("Your territory %1 has been created! Congrats!", data.param1.Name), "set:ccgui_enforce image:Icon40Move");
    
        Territories.Insert(data.param1);

        vector MarkerPosition = data.param1.Position;

        BasicMapCircleMarker Territory_Marker = new BasicMapCircleMarker("", MarkerPosition, "BasicMap\\gui\\images\\house.paa", {255, 0, 0});
        Territory_Marker.SetCanEdit(true);
        Territory_Marker.SetShowCenterMarker(true);
        Territory_Marker.SetHideIntersects(false);
        Territory_Marker.SetPosition(MarkerPosition);
        Territory_Marker.SetRadius(data.param1.Level.GetRadius());
        Territory_Marker.Name = string.Format("Territory : %1 - %2", data.param1.Name, data.param1.Level.GetName());
        BasicMap().AddMarker(BasicMap().TERRITORY_KEY, Territory_Marker);
        TerritoryMarkers.Insert(Territory_Marker);
    }

    void TerritoryCheckResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param4<ref array<ref Loaded_Territory>, string, ref LoadedTerritoryConfig, ref LoadedTerritoryFlagsConfig> data;
        if (!ctx.Read(data)) return;

        PlayerUID = data.param2;
        Config = data.param3;
        FlagConfig = data.param4;

        if (data.param1.Count() <= 0)
            return;
        
        Territories = data.param1;
    
        foreach (Loaded_Territory territory: Territories)
        {
            BasicMapCircleMarker Territory_Marker = new BasicMapCircleMarker("", territory.Position, "BasicMap\\gui\\images\\house.paa", {255, 0, 0});
            Territory_Marker.SetCanEdit(true);
            Territory_Marker.SetShowCenterMarker(true);
            Territory_Marker.SetHideIntersects(false);
            Territory_Marker.SetPosition(territory.Position);
            Territory_Marker.SetRadius(territory.Level.GetRadius());
            Territory_Marker.Name = string.Format("Territory : %1 - %2", territory.Name, territory.Level.GetName());
            BasicMap().AddMarker(BasicMap().TERRITORY_KEY, Territory_Marker);
            TerritoryMarkers.Insert(Territory_Marker);

            //--- Warn members on log in of their base's not being paid.
            //--- Less than 24 hours, warn group.
            if (territory.MinutesTillRentDue >= (Config.TerritoryRentDueTimerMinutes - 1440))
            {
                //NotificationSystem.SendNotificationToPlayerIdentityExtended(GetPlayerIdentityByUID(TerrMember.UID), 3, "SIX Base Building", "Your territory rent is due soon..", "set:ccgui_enforce image:Icon40Move");
            }
        }
    }

    void UpdateTerritoryDataResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        if (!data.param1)
            return;
        
        for (int i = 0; i < Territories.Count(); i++)
        {
            Loaded_Territory territory = Territories[i];
            if (territory.GUID == data.param1.GUID)
            {
                Territories.RemoveOrdered(Territories.Find(Territories[i]));
                Territories.Insert(data.param1);
                vector MarkerPosition = data.param1.Position;
                BasicMapMarker OldMarker = BasicMap().GetMarkerByVector(MarkerPosition, 1);
                TerritoryMarkers.RemoveOrdered(TerritoryMarkers.Find(OldMarker));
                BasicMap().RemoveMarkerByVector(MarkerPosition);
                

                BasicMapCircleMarker Territory_Marker = new BasicMapCircleMarker("", MarkerPosition, "BasicMap\\gui\\images\\house.paa", {255, 0, 0});
                Territory_Marker.SetCanEdit(true);
                Territory_Marker.SetShowCenterMarker(true);
                Territory_Marker.SetHideIntersects(false);
                Territory_Marker.SetPosition(MarkerPosition);
                Territory_Marker.SetRadius(territory.Level.GetRadius());
                Territory_Marker.Name = string.Format("Territory : %1 - %2", territory.Name, territory.Level.GetName());
                BasicMap().AddMarker(BasicMap().TERRITORY_KEY, Territory_Marker);
                TerritoryMarkers.Insert(Territory_Marker);
            }
        }
    }

    void AddToTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        if (!data.param1)
            return;
            
        Territories.Insert(data.param1);

        vector MarkerPosition = data.param1.Position;

        BasicMapCircleMarker Territory_Marker = new BasicMapCircleMarker("", MarkerPosition, "BasicMap\\gui\\images\\house.paa", {255, 0, 0});
        Territory_Marker.SetCanEdit(true);
        Territory_Marker.SetShowCenterMarker(true);
        Territory_Marker.SetHideIntersects(false);
        Territory_Marker.SetPosition(MarkerPosition);
        Territory_Marker.SetRadius(data.param1.Level.GetRadius());
        Territory_Marker.Name = string.Format("Territory : %1 - %2", data.param1.Name, data.param1.Level.GetName());
        BasicMap().AddMarker(BasicMap().TERRITORY_KEY, Territory_Marker);
        TerritoryMarkers.Insert(Territory_Marker);
    }

    void RemoveFromTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        if (!data.param1)
            return;
        
        foreach (Loaded_Territory territory: Territories)
        {
            if (territory.GUID == data.param1.GUID)
                Territories.RemoveOrdered(Territories.Find(territory));
        }
    }

    void GetAllPlayerListResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref array<ref LoadedTerritoryMember>> data;
        if (!ctx.Read(data)) return;

        if (data.param1)
            AllServerPlayers = data.param1;
    }

    void InviteReceived(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref LoadedTerritoryInvite> data;
        if (!ctx.Read(data)) return;
        
        Invites.Insert(data.param1);
        UpdateMenu();
    }

    void PayTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<string> data;
        if (!ctx.Read(data)) return;

        if (data.param1 == "SUCCESS" && g_TerritoryPaymentMenu != null)
        {
            NotificationSystem.AddNotificationExtended(3, "Territory", "You have paid your territory!", "set:ccgui_enforce image:Icon40Move");
            GetTerritoryPaymentMenu().UpdateMenu();
        }
    }

    void UpgradeTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<string> data;
        if (!ctx.Read(data)) return;

        if (data.param1 == "SUCCESS" && g_TerritoryPaymentMenu != null) 
        {
            NotificationSystem.AddNotificationExtended(3, "Territory", "You have upgraded your territory!", "set:ccgui_enforce image:Icon40Move");
            GetTerritoryPaymentMenu().UpdateMenu();
        }
        else
            NotificationSystem.AddNotificationExtended(3, "Territory", "Territory upgrading failed!", "set:ccgui_enforce image:Icon40Move");
    }

    void UpdateTriggerTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref Loaded_Territory> data;
        if (!ctx.Read(data)) return;

        TriggerTerritory = data.param1;

        //--- Do Territory Notification
        if (TriggerTerritory)
        {
            int x = Math.Floor(TriggerTerritory.Position[0]);
            int y = Math.Floor(TriggerTerritory.Position[1]);
            int z = Math.Floor(TriggerTerritory.Position[2]);

            GetTownNotificationsClient().SimulateLocationWidget( TriggerTerritory.Name, TriggerTerritory.CreatedAt, string.Format("%1 %2 %3", x, y, z));
        }
    }

    Loaded_Territory GetCurrentTerritoryByLocation(vector position = vector.Zero) 
    {
        //--- Make sure player actually is in any territories
        if (!IsTerritoryMember())
        {
            return null;
        }
        
        if (position == vector.Zero)
            position = GetGame().GetPlayer().GetPosition();
        
        foreach (Loaded_Territory territory: Territories)
        {
            vector TerritoryPosition = territory.Position;
            float TerritoryRadius = territory.Level.Radius;

            //--- Check to check if its within the territory Radius
            if (vector.Distance(TerritoryPosition, position) <= TerritoryRadius)
                return territory;
        }

        return null;
    }

    bool IsInOwnTerritory() 
    {
        Man player = GetGame().GetPlayer();

        Loaded_Territory CurrentTerritory = GetCurrentTerritoryByLocation();

        if (!CurrentTerritory)
            return false;

        foreach (LoadedTerritoryMember territoryMemeber: CurrentTerritory.Members)
            if (territoryMemeber.UID == PlayerUID)
                return true;

        return false;
    }

    bool IsInEnemyTerritory() 
    {
        if (IsInOwnTerritory()) return false;

        //--- TODO: Change to actually get the current enemy's territory radius size via Trigger(1.14)
        return ItemIsNearby("TerritoryFlag", GetGame().GetPlayer().GetPosition(), 80);
    }

    bool IsTerritoryMember() return (Territories.Count() > 0);

    void OpenTerritoryManagement() 
    {
        //--- Check If In Own Territory
        if (!IsTerritoryMember())
        {
            NotificationSystem.AddNotificationExtended(3, "Territory Management Menu", "You need to be apart of a territory first!", "set:ccgui_enforce image:Icon40Move");
            return;
        }
        
        PlayerBase m_Player = PlayerBase.Cast(GetGame().GetPlayer());
        Loaded_Territory NearestTerritory = GetCurrentTerritoryByLocation(m_Player.GetPosition());
        
        //--- No Near Territory/Not Within Limits
        if (!NearestTerritory) 
        {
            NotificationSystem.AddNotificationExtended(3, "Territory Management Menu", "You need to be within your territory!", "set:ccgui_enforce image:Icon40Move");
            return;
        }
        
        GetGame().GetUIManager().CloseAll();
        GetGame().GetUIManager().ShowScriptedMenu(GetTerritoryManagementMenu(), NULL);
    }

    void OpenBaseBuildingMenu() 
    {
        //--- Check If In Own Territory
        if (!IsTerritoryMember())
        {
            NotificationSystem.AddNotificationExtended(3, "Base Building Menu", "You need to be apart of a territory first!", "set:ccgui_enforce image:Icon40Move");
            return;
        }
        
        PlayerBase m_Player = PlayerBase.Cast(GetGame().GetPlayer());
        Loaded_Territory NearestTerritory = GetCurrentTerritoryByLocation(m_Player.GetPosition());
        
        //--- No Near Territory/Not Within Limits
        if (!NearestTerritory) 
        {
            NotificationSystem.AddNotificationExtended(3, "Base Building Menu", "You need to be within your territory!", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        GetGame().GetUIManager().CloseAll();
        GetGame().GetUIManager().ShowScriptedMenu(GetBaseBuildingMenu(), NULL);
    }

    void UpdateMenu() 
    {
        if (g_TerritoryManagementMenu && g_TerritoryManagementMenu.isOpen) g_TerritoryManagementMenu.UpdateMenu();
    }

    LoadedTerritoryInvite GetActiveInvite() 
    {
        if (Invites.Count()> 0) 
            return Invites[0];
        
        return null;

    }

    void DeclineInvite() 
    {
        if (Invites.Count() > 0) 
            Invites.RemoveOrdered(0);

        UpdateMenu();
    }

    void AcceptInvite() 
    {
        if (IsTerritoryMember()) 
            NotificationSystem.AddNotificationExtended(3, "Territory", "Can't build here this area is a no build zone", "set:ccgui_enforce image:Icon40Move");

        LoadedTerritoryInvite invite = GetActiveInvite();
        if (invite) 
        {
            GetRPCManager().SendRPC("Loaded_Territory_Server", "AcceptInviteRequest", new Param1<ref LoadedTerritoryInvite>(invite), true, null);
            Invites.RemoveOrdered(0);
        }

        UpdateMenu();
    }

    bool CanBuild(ItemBase item) 
    {
        TStringArray RaidItems = {};
        TStringArray TentItems = {};

        string itemName = item.GetType();
        PlayerBase player = GetGame().GetPlayer();
        
        if (itemName == "TerritoryFlagKit") 
        {
            if (IsInNoBuildZone()) 
            {
                NotificationSystem.AddNotificationExtended(3, "Territory", "Can't build here this area is a no build zone", "set:ccgui_enforce image:Icon40Move");
                if (player) 
                    player.TogglePlacingLocal();
                return false;
            }
            
            return CanPlacePlotPole();
        }

        if (IsTerritoryMember()) 
        {
            Loaded_Territory territory = GetCurrentTerritoryByLocation();
            if (!territory)
            {
                if (territory && territory.IsMaxObjectReached())
                    return false;
            }
        }

        if (IsTerritoryMember() && IsTerritoryDecaying()) return false;
        if (IsTerritoryMember() && IsInOwnTerritory()) return true;
        
        if (IsInEnemyTerritory()) 
            if (GetTerritoryConfig().ItemsAllowedInEnemyTerritory.Find(itemName) != -1) 
                return true;
        else if (GetTerritoryConfig().NeutralItems.Find(itemName) != -1) 
            return true;
            
        return false;
    }
    
    LoadedTerritoryConfig GetTerritoryConfig() return Config;

    bool CanDismantle(Object targetObject) 
    {
        float snapping_9;
        float snapping_10;
        array<string> poopie = {"T1_Gate", "T2_Gate", "T3_Gate"};

        if (targetObject)
        {
            if (poopie.Find(targetObject.GetType()) != -1)
            {
                snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), GetGame().GetPlayer().GetPosition());
                snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), GetGame().GetPlayer().GetPosition());
                if (snapping_9 < snapping_10) return true;
            }

            if (Config.NonDismantableItems.Find(targetObject.GetType()) == -1 && poopie.Find(targetObject.GetType()) == -1)
            {
                snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), GetGame().GetPlayer().GetPosition());
                snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), GetGame().GetPlayer().GetPosition());
                if (snapping_9 > snapping_10) return true;
            }
        }

        return false;
    }

    bool IsInNoBuildZone() 
    {
        if (!GetTerritoryConfig().NobuildZones) 
            return false;

        for (int i = 0; i < GetTerritoryConfig().NobuildZones.Count(); i++) 
            if (GetTerritoryConfig().NobuildZones[i].PositionIsViolating(GetGame().GetPlayer().GetPosition(), 15)) return true;

        return false;
    }

    bool CanPlacePlotPole() 
    {
        if (IsInEnemyTerritory()) 
            return false;

        return !ItemIsNearby("TerritoryFlag", GetGame().GetPlayer().GetPosition(), 80 + 100);
    }

    bool IsTerritoryDecaying() 
    {
        Object FlagObj = GetNearFlagObject(GetCurrentTerritoryByLocation().GetPosition(), GetCurrentTerritoryByLocation().Level.GetRadius());
        TerritoryFlag FlagTerritory;

        if (Class.CastTo(FlagTerritory, FlagObj)) 
            return FlagTerritory.GetIsDecaying();

        return false;
    }
};

ref TerritoryManagerClient g_TerritoryManagerClient;
TerritoryManagerClient GetTerritoryManagerClient() 
{
    if (!g_TerritoryManagerClient)
        g_TerritoryManagerClient = new TerritoryManagerClient;
    return g_TerritoryManagerClient;
}