/**
 * LoadedTerritoryConfig.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class LoadedTerritoryConfig
{
    float TerritoryRentCycleMinutes = 10080;    //--- How long Rent Is Due from last time paid/territory built - Minutes
    float TerritoryRentIncreaseMinutes = 1440;  //--- How often server adds rent to territory - Minutes
    float TerritoryDecayTimeMinutes = 2880;     //--- How long it takes to decay a base fully.
    float decayRatePercent = 0.01;
    int DecayTotalStages = 10;
    int TerritoryRentDueTimerMinutes = 15;         //---- How often server updates timer, lower the timer, worse on performance.
    int TerritoryRentIncreaseTimerMinutes = 15;    //---- How often server updates timer, lower the timer, worse on performance.
    int MinimumTerritoryNameLength = 4;
    int ShowTerritoryMembersInAllPlayersList = 1;
    int AutoBuildTerritoryFlag = 1;

    ref TStringArray ItemsAllowedInEnemyTerritory = new TStringArray;
    ref TStringArray NonDismantableItems = new TStringArray;
    ref TStringArray NeutralItems = new TStringArray;
    ref array<ref LoadedNoBuildZone> NobuildZones = new array<ref LoadedNoBuildZone>;
    ref array<ref LoadedTerritoryLevel> TerritoryLevels = new array<ref LoadedTerritoryLevel>;
    ref array<ref LoadedBaseItemPrice> BaseItemPrices = new array<ref LoadedBaseItemPrice>;
    ref array<ref LoadedTerritoryProtectionLevel> TerritoryProtectionLevels = new array<ref LoadedTerritoryProtectionLevel>;   
}