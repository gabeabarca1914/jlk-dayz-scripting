class Loaded_Territory 
{
    string Name;
    int GUID;
    string OwnerUID;
    string CreatedAt;
    string LastPaid;
    int TerritoryProtectionLevel = 0; //--- 0 Is none

    vector Position;
    float CurrentRentOwed = 0;
    float MinutesTillRentDue = 0;

    ref TStringArray AdminUIDS = new TStringArray;
    ref array<ref LoadedTerritoryMember> Members = new array<ref LoadedTerritoryMember>;
    ref LoadedTerritoryLevel Level;
    bool TerritoryIsDecayingPayment = false;   
    bool TerritoryIsDecayingFlag = false;
    float MinutesTillRentAdd = 0;
    
    ref LoadedTerritoryDecayData DecayData;

    TerritoryTrigger m_TerritoryTrigger;

    string GetName()    return Name;
    int GetGUID()       return GUID;
    bool GetDecayStatus()
    {
        if (TerritoryIsDecayingPayment)
            return TerritoryIsDecayingPayment;
        
        if (TerritoryIsDecayingFlag)
            return TerritoryIsDecayingFlag;
        
        return false;
    }

    int GetTerritoryProtectionLevel() return TerritoryProtectionLevel;

    int GetMemberIndex(string uid)
    {
        for (int i = 0; i < Members.Count(); i++)
            if (Members[i].UID == uid) return i;
        return -1;
    }

    bool IsMember(string uid) return GetMemberIndex(uid) != -1;

    LoadedTerritoryMember GetMember(string uid)
    {
        int index = GetMemberIndex(uid);

        if (index != -1) return Members[index];
        
        return null;
    }

    bool IsAdmin(string uid)
    {
        if (IsOwner(uid)) return true;
        
        return AdminUIDS.Find(uid) != -1;
    }

    bool IsOwner(string uid) return OwnerUID == uid;
    LoadedTerritoryLevel GetLevel() return Level;

    int GetItemPrice(string classname) 
    {
        array<ref LoadedBaseItemPrice> PricesArray = GetTerritoryManagerServer().Config.BaseItemPrices;
        foreach(LoadedBaseItemPrice PriceData: PricesArray)
            if (PriceData.GetClassname() == classname) 
                return PriceData.GetObjectPrice();
        return 0;
    }

    int CalculateLifetimeMinutesRemaining()
    {
        LoadedTerritoryConfig config;
        if (GetGame().IsServer())
            config = GetTerritoryManagerServer().Config;
        else
            config = GetTerritoryManagerClient().Config;

        int totalLifeTimeMinutes = config.TerritoryRentCycleMinutes;

        return totalLifeTimeMinutes - MinutesTillRentDue;
    }

    string GetRemainingLifeTimeDisplay()
    {
        int totalLifeTimeMinutes = CalculateLifetimeMinutesRemaining();

        //--- Maths
        int Days =  totalLifeTimeMinutes / (24 * 60);
        int Hours = (totalLifeTimeMinutes % (24 * 60)) / 60;
        int Minutes = (totalLifeTimeMinutes % (24 * 60)) % 60;

        return "" + Days + " Days " + Hours + " Hours And " + Minutes + "  Minutes";
    }

    vector GetPosition() return Position;

    map<string, int> GetPartCountMap() 
    {
        map<string, int> partCountMap = new map<string, int>;

        array<Object> parts = GetPartArray();

        foreach(Object Obj: parts)
        {
            if (partCountMap.Contains(Obj.GetType()))
            {
                int currentCount;
                partCountMap.Find(Obj.GetType(), currentCount);
                partCountMap.Set(Obj.GetType(), currentCount + 1);
            }
            else
                partCountMap.Insert(Obj.GetType(), 1);
        }

        return partCountMap;
    }
   
    array<Object> GetPartArray(bool ignoreStorage = false, bool ignoreFlag = true) 
    {
        array<Object> nearest_objects = new array<Object>;
        array<CargoBase> proxy_cargos = new array<CargoBase>;
        GetGame().GetObjectsAtPosition3D(GetPosition(), Level.GetRadius(), nearest_objects, proxy_cargos);
        
        array<Object> parts = new array<Object>;

        foreach(Object Obj: nearest_objects)
        {
            if (!ignoreFlag && Obj.IsInherited(TerritoryFlag)) 
                parts.Insert(Obj);

            if (!ignoreStorage && (Obj.IsInherited(LargeTent) || Obj.IsInherited(DeployableContainer_Base))) 
                parts.Insert(Obj);

            if (Obj.IsInherited(Loaded_BaseBuildingObject) || Obj.IsInherited(BaseBuildingBase))
                parts.Insert(Obj);
        }
        return parts;
    }

    bool TerritoryHasItem(string classname) 
    {
        array<Object> Parts = GetPartArray();

        foreach (Object item : Parts)
        {
            string className = item.GetType();
            if (classname == className)
                return true;
        }

        return false;
    }

    bool IsMaxObjectReached() 
    {
        int TerritoryMaxObjects = this.GetLevel().GetMaxObjectCount();
        map<string, int> partCountMap = this.GetPartCountMap();
        int itemCount = 0;

        for (int i = 0; i < partCountMap.Count(); i++)
        {
            string currentIndexKey = partCountMap.GetKey(i);
            int currentIndexValue = partCountMap.GetElement(i);
            itemCount += currentIndexValue;
        }

        return itemCount >= TerritoryMaxObjects;
    }
};