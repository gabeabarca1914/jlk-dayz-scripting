/**
 * LoadedTerritoryLevel.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class LoadedTerritoryLevel
{
    string Name;
    int BuyPrice;
    int RentalPrice;
    int CostPerMember;
    float Radius;
    int MaxObjectCount;
   
    void LoadedTerritoryLevel(string name, int price, int rent, int memberCost, float radius, int maxObjectCount) 
    {
        Name = name;
        BuyPrice = price;
        RentalPrice = rent;
        CostPerMember = memberCost;
        Radius = radius;
        MaxObjectCount = maxObjectCount;
    }

    string GetName() return Name;
    int GetPrice() return BuyPrice;
    int GetRentPrice() return RentalPrice;
    int GetPerMemberPrice() return CostPerMember;
    float GetRadius() return Radius;
    int GetMaxObjectCount() return MaxObjectCount;
}