/**
 * BaseBuildingManagerClient.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Sep 19 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class BaseBuildingManagerClient
{
    ref BaseBuildingConfig Config;

    /*
    * Array of all base building recipes
    */
    ref array<ref LoadedRecipe> Recipes = new array<ref LoadedRecipe>;

    int CurrBuildingMode;
    
    void BaseBuildingManagerClient() 
    {
        GetRPCManager().AddRPC("LoadedBuildingManager_Client", "GetConfigResponse", this);
        
        Print("BaseBuilding Debug - GetConfigRequest - Sending");
        GetRPCManager().SendRPC("LoadedBaseBuildingManager_Server", "GetConfigRequest", NULL, true, NULL);

        SetBuildingMode(BuildingMode.FREE);
    }

    /*
    * Get config response
    */
    void GetConfigResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Print("BaseBuilding Debug - GetConfigResponse");
        Param2<ref BaseBuildingConfig, array<ref LoadedRecipe>> data;
        if (!ctx.Read(data))
            return;
        Config = data.param1;
        Recipes = data.param2;
    }

    void SetBuildingMode(int mode) CurrBuildingMode = mode;
    int GetBuildingMode() return CurrBuildingMode;
}

ref BaseBuildingManagerClient g_BaseBuildingManagerClient;
BaseBuildingManagerClient GetBaseBuildingManagerClient()
{
    if (!g_BaseBuildingManagerClient)
        g_BaseBuildingManagerClient = new BaseBuildingManagerClient;
    return g_BaseBuildingManagerClient;
}