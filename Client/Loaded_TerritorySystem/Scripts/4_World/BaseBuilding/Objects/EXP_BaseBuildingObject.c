/**
 * Loaded_BaseBuildingObject.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class Loaded_BaseBuildingObject extends House
{
    const float MAX_ACTION_DETECTION_ANGLE_RAD 		= 1.3;		//1.3 RAD = ~75 DEG
	const float MAX_ACTION_DETECTION_DISTANCE 		= 2.0;		//meters

	protected bool m_IsBaseObject = false;
	protected bool m_IsDoorOpened = false;
	protected bool m_HasCodelock = false;

	//--- 1: Normal Codelock, 2: Personal Codelock
	protected int m_CodelockType = 0;

	protected int m_OwnerIDOne = 0;
	protected int m_OwnerIDTwo = 0;
	protected int m_OwnerIDThree = 0;
	protected int m_OwnerIDFour = 0;
	protected int m_OwnerIDFive = 0;

    string KitReturn() return "";
	bool IsDoor() return false;
	bool IsBaseObject() return true;
	bool IsDoorOpened() return false;
	bool HasCodelock() return m_HasCodelock;
	int GetCodelockType() return m_CodelockType;

	array<int> GetAllUsers() 
	{
		//--- build array of slots
		array<int> PermissionSlot = {m_OwnerIDOne, m_OwnerIDTwo, m_OwnerIDThree, m_OwnerIDFour, m_OwnerIDFive};
		//--- Build Return Array
		array<int> DefinedUsers = new array<int>;

		//--- loop through all slots
		foreach (int slot: PermissionSlot)
		{
			//--- make sure slot has a user defined to it
			if (slot != 0)
			{
				//--- Inert slot into return array
				DefinedUsers.Insert(slot);
			}
		}

		//--- Return the Array of slots
		return DefinedUsers;
	}

    bool IsFacingPlayer( PlayerBase player)
	{
		vector fence_pos = GetPosition();
		vector player_pos = player.GetPosition();
		vector ref_dir = GetDirection();
		
		//vector fence_player_dir = player_pos - fence_pos;
		vector fence_player_dir = player.GetDirection();
		fence_player_dir.Normalize();
		fence_player_dir[1] = 0; 	//ignore height
		
		ref_dir.Normalize();
		ref_dir[1] = 0;			//ignore height
		
		if ( ref_dir.Length() != 0 )
		{
			float angle = Math.Acos( fence_player_dir * ref_dir );
			
			if ( angle >= MAX_ACTION_DETECTION_ANGLE_RAD )
			{
				return true;
			}
		}
		
		return false;
	}

    bool IsFacingCamera()
	{
		vector ref_dir = GetDirection();
		vector cam_dir = GetGame().GetCurrentCameraDirection();
		
		//ref_dir = GetGame().GetCurrentCameraPosition() - GetPosition();
		ref_dir.Normalize();
		ref_dir[1] = 0;		//ignore height
		
		cam_dir.Normalize();
		cam_dir[1] = 0;		//ignore height
		
		if ( ref_dir.Length() != 0 )
		{
			float angle = Math.Acos( cam_dir * ref_dir );
			
			if ( angle >= MAX_ACTION_DETECTION_ANGLE_RAD )
			{
				return true;
			}
		}

		return false;
	}

	void DestroyObject() 
	{
		for (int i = 0; i < 10; i++)
		{
			vector position = "10932.994141 6.197820 2889.081543";
			float xMax = position[0] + Math.RandomFloatInclusive(-1, 1);
			float yMax = position[1] + Math.RandomFloatInclusive(0, 1);	//--- Dont allow to go below current value, it would go under ground.
			float zMax = position[2] + Math.RandomFloatInclusive(-1, 1);
			position = Vector(xMax, yMax, zMax);
			GetGame().CreateObject("Apple", position);
		}
	}
};