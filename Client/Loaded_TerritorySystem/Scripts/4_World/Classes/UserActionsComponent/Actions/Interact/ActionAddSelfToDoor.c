/**
 * ActionAddSelfToDoor.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionAddSelfToDoor: ActionInteractBase
{
	void ActionAddSelfToDoor()
	{
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_INTERACTONCE;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_HUDCursorIcon = CursorIcons.CloseHood;
	}

    override void CreateConditionComponents()  
	{
		m_ConditionTarget = new CCTObject(4);
		m_ConditionItem = new CCINone;
	}

	override string GetText()
	{
		return "Add Self To Personal Door";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
    {
        if (GetGame().IsServer())
            return true;
        
        Loaded_BaseBuildingObject baseObject = Loaded_BaseBuildingObject.Cast(target.GetObject());
        Building building = Building.Cast(target.GetObject());

        //--- dont show vanilla action on base objects
        if (baseObject && baseObject.IsDoor())
        {
            //--- Make sure they are in their own territory
            if (!GetTerritoryManagerClient().IsInOwnTerritory())
                return false;

            //--- Door does not have any codelock 
            if (!baseObject.HasCodelock())
                return false;
            
            //--- Not door with personal codelock
            if (baseObject.GetCodelockType() != CodelockType.Personal)
                return false;

            //--- Make sure door is open and found
            int doorIndex = building.GetDoorIndex(target.GetComponentIndex());
            if (doorIndex == -1 || !building.IsDoorOpen(doorIndex))
                return false;
            
            //--- Too many users on door
            if (baseObject.GetAllUsers().Count() >= 5)
                return false;
            
            return true;
        }
        
		return false;
    }
};