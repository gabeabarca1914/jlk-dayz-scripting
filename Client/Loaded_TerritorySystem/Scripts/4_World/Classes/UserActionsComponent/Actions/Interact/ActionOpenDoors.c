/**
 * ActionOpenDoors.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Mon Nov 08 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionOpenDoors: ActionInteractBase
{
    string interactionText = "";
    
    override string GetText()
	{
		return interactionText;
	}

    override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if (GetGame().IsServer())
            return true;
        
        Loaded_BaseBuildingObject baseObject = Loaded_BaseBuildingObject.Cast(target.GetObject());

        //--- dont show vanilla action on base objects
        if (baseObject && baseObject.IsDoor())
        {
            //--- If door is NOT code locked, just check normal status
            if (!baseObject.HasCodelock())
            {
                interactionText = "#open";
                return super.ActionCondition(player, target, item);
            }

            //--- Door is personal code lock door.
            if (baseObject.HasCodelock() && baseObject.GetCodelockType() == CodelockType.Personal)
            {
                if (!GetTerritoryManagerClient().IsInOwnTerritory())
                    return false;

                //--- Check they are added to door permissions.
                if (baseObject.GetAllUsers().Find(GetTerritoryManagerClient().PlayerUID.ToInt()) != -1)
                {
                    interactionText = "Open Personal Door";
                    return super.ActionCondition(player, target, item);
                }
            }

            //--- Make sure they are in their own territory
            if (GetTerritoryManagerClient().IsInOwnTerritory())
            {
                interactionText = "#open";
                return super.ActionCondition(player, target, item);
            }
            
            return false;
        }
        
		return super.ActionCondition(player, target, item);
	}
};