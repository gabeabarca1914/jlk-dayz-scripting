/**
 * ActionDeconstructBaseItemEnemy.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : Own Territory
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionDeconstructBaseItemEnemyCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{		
        m_ActionData.m_ActionComponent = new CAContinuousTime( 30 );
	}
};

class ActionDeconstructBaseItemEnemy: ActionContinuousBase 
{
    void ActionDeconstructBaseItemEnemy()
	{
		m_CallbackClass = ActionDeconstructBaseItemEnemyCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_ASSEMBLE;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT;
	}
	
	override void CreateConditionComponents()  
	{	
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTNonRuined( UAMaxDistances.SMALL );
	}
		
	override string GetText() return "Dismantal Enemy";

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{   
		if (GetGame().IsServer())
			return true;
			
		if (vector.Distance(target.GetObject().GetPosition(), player.GetPosition()) < 1)
        {
			if (target.GetObject().IsInherited(TerritoryFlag))
				return false;
			if ((target.GetObject().IsInherited(Loaded_BaseBuildingObject)) || (target.GetObject().IsInherited(BaseBuildingBase)))
           		return !GetTerritoryManagerClient().IsInOwnTerritory() && GetTerritoryManagerClient().CanDismantle(target.GetObject());
			return false;
		}
		return false;
    }
};