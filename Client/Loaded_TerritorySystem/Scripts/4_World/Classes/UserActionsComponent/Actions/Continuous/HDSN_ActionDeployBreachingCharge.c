/**
 * HDSN_ActionDeployBreachingCharge.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class HDSN_ActionDeployBreachingCharge: ActionContinuousBase
{
    /*override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{	
		if(!HDSN_DestructionManager.GetInstance().IsRaidingAllowed())
		{
			return false; 
		}
		
		if(!GetGame().IsClient())
		{
			return true; 
		}
		
		if(!player.IsPlacingLocal())
		{
			return false;
		}
		
		player.GetHologramLocal().SetActionDeployBreachingCharge(this);		
				
		HDSN_BreachingChargeBase charge = HDSN_BreachingChargeBase.Cast(item);
		if(charge)
		{
			raycastDistance = charge.GetPlacementDistance();	
		}						
	
		vector playerHeadPos;
		MiscGameplayFunctions.GetHeadBonePos(player, playerHeadPos);
		float cameraToPlayerDistance = vector.Distance(GetGame().GetCurrentCameraPosition(), playerHeadPos);
		vector startPosition = GetGame().GetCurrentCameraPosition();
		vector endPosition = startPosition + (GetGame().GetCurrentCameraDirection() * (raycastDistance + cameraToPlayerDistance));
		
		vector hitPosition; 
		vector hitDirection; 
		int hitComponent; 
		Object hitObject; 
		
		if(HDSN_MiscFunctions.CustomRaycast(startPosition, endPosition, hitPosition, hitDirection, hitComponent, hitObject, player, player.GetHologramLocal().GetProjectionEntity()))
		{									
			if(hitObject)
			{	
	
				//slight offset along the mesh-normal of the hit object -> charges tend to clip inside some models a tiny bit too much
				//this tries to prevent the charges from being completely hidden inside an object 
				hitPosition += hitDirection.Normalized() * GetOffsetDistance(hitObject, item, hitComponent); 
				
				int levelIndex = 0; 
				int wallIndex = 0;
				
				if(hitObject.GetType() == "Watchtower")
				{
					levelIndex = HDSN_DestructionManager.GetInstance().GetWatchtowerFloorIndex(hitObject, hitPosition);
					wallIndex = HDSN_DestructionManager.GetInstance().GetWatchtowerWallIndex(hitObject, hitPosition, levelIndex);	
				}
								
				if(HDSN_DestructionManager.GetInstance().IsChargeAllowed(hitObject, item, hitComponent, levelIndex, wallIndex))
				{
					bool isTargetValid = true; 	
													
					if(hitObject.GetType() == "Watchtower")
					{									
						if(HDSN_DestructionManager.GetInstance().IsWatchtowerWallBuildOnLevel(hitObject, levelIndex))
						{
							//Charge not in valid area or specified wall isn't build			
							if(wallIndex == -1 || !HDSN_DestructionManager.GetInstance().IsWallBuild(hitObject, levelIndex, wallIndex))
							{
								isTargetValid = false; 
							}
						}	
						else 
						{
							//Charge not in valid area or base on floor is not build		
							if(wallIndex == -1 || !HDSN_DestructionManager.GetInstance().IsWatchtowerBaseBuildOnLevel(hitObject, levelIndex))
							{
								isTargetValid = false; 
							}
							
							if(wallIndex > 0)
							{									
								if(!HDSN_DestructionManager.GetInstance().IsWallBuild(hitObject, levelIndex, wallIndex))
								{
									isTargetValid = false; 
								}
							}
						}																						
					}
											
					if(isTargetValid)
					{
						raycastTarget = hitObject; 
						HDSN_BreachingChargeBase.Cast(item).SetTarget(raycastTarget);
						actionText = GetCustomActionTextPlanting(raycastTarget.GetDisplayName(), item.GetDisplayName());
						contactPos = hitPosition;
						contactDir = hitDirection;
						return true;
					}							
				}										
			}															
		}
		
		contactPos = vector.Zero;
		contactDir = vector.Zero;
		
		raycastTarget = null; 
		HDSN_BreachingChargeBase.Cast(item).SetTarget(null);	
		actionText = "No target";	
		
		return false;
	}*/
};