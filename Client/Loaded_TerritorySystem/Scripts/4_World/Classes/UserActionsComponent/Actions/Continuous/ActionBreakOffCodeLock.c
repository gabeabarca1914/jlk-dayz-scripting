/**
 * ActionBreakOffCodeLock.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Nov 09 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionBreakOffCodeLock: ActionContinuousBase
{
    static int CYCLES = 4;
	
	void ActionDestroyPart()
	{
		m_CallbackClass = ActionBreakOffCodeLockCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_DISASSEMBLE;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT;
	}
	
	override void CreateConditionComponents()  
	{	
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTNone;
	}
		
	override string GetText() return "Destroy Codelock";

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{	
		if (GetGame().IsServer())
            return true;
			
		if ( player && !player.IsLeaning() )
		{
			Object target_object = target.GetObject();
            Loaded_BaseBuildingObject baseObject = Loaded_BaseBuildingObject.Cast(target_object);
			
			if (baseObject && baseObject.IsBaseObject() && baseObject.IsDoor() && baseObject.HasCodelock())
			{
				string part_name = target_object.GetType();
				
                //--- camera and position checks
                if ( !player.GetInputController().CameraIsFreeLook() && IsInReach(player, target, UAMaxDistances.DEFAULT) && !player.GetInputController().CameraIsFreeLook() )
                {
                    //--- Camera check (client-only)
                    if ( GetGame() && ( !GetGame().IsMultiplayer() || GetGame().IsClient() ) )
                        if (baseObject.IsFacingCamera()) 
                            return false;

                    return true;			
                }
			}
		}
		
		return false;
	}
};