/*
 * BaseBuildingControlsMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Mar 07 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class BaseBuildingControlsMenu: UIScriptedMenu
{
    TextWidget header;
    TextWidget footerText;
    MultilineTextWidget BaseBuidlingTipTxt;
    Widget Footer;

    void BaseBuildingControlsMenu() 
    {
        Init();
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.UpdateMenu, 100, true);
    }

    void ~BaseBuildingControlsMenu()
    {
        GetGame().GetCallQueue(CALL_CATEGORY_GUI).Remove(this.UpdateMenu);
    }

    override Widget Init()
    {
        layoutRoot = GetGame().GetWorkspace().CreateWidgets("Loaded_TerritorySystem/Layouts/BaseBuildingInfoPanel.layout");
        header = TextWidget.Cast(layoutRoot.FindAnyWidget("header"));
        BaseBuidlingTipTxt = MultilineTextWidget.Cast(layoutRoot.FindAnyWidget("BaseBuidlingTipTxt"));
        Footer = layoutRoot.FindAnyWidget("bottomPanel");
        footerText = TextWidget.Cast(layoutRoot.FindAnyWidget("footer"));
        
        return layoutRoot;
    }

    override void OnShow() 
    {
        if (PlayerBase.Cast(GetGame().GetPlayer()).IsPlacingLocal())
        {
            EntityAI entity_in_hands = PlayerBase.Cast(GetGame().GetPlayer()).GetHumanInventory().GetEntityInHands();
            
            if (!entity_in_hands)
            {
                BaseBuidlingTipTxt.SetText("You dont have anything in your hands? what...");
                return;
            }

            LoadedBaseBuildingKit KitData = LoadedBaseBuildingKit.Cast(entity_in_hands);
            if (KitData)
            {
                switch (KitData.KitProduces())
                {
                    case "T1_Door": 
                    {
                        break;
                    }

                    case "T2_Door": 
                    {
                        break;
                    }

                    case "T3_Door": 
                    {
                        break;
                    }
                }
            }
        }
    }

    void UpdateMenu()
    {  
        if (PlayerBase.Cast(GetGame().GetPlayer()).IsPlacingLocal())
        {
            layoutRoot.Show(true);

            if (GetBaseBuildingManagerClient().GetBuildingMode() == BuildingMode.SNAP)
                header.SetText("SNAP MODE");
            else
                header.SetText("FREE MODE");

            ItemBase item_in_hands = ItemBase.Cast( PlayerBase.Cast(GetGame().GetPlayer()).GetHumanInventory().GetEntityInHands() );
            if (item_in_hands && item_in_hands.GetType() == "TerritoryFlagKit")
            {       
                Footer.Show(false);
                return;
            }

            if (!GetTerritoryManagerClient().IsTerritoryMember())
            {
                Footer.Show(true);
                footerText.SetText("You need to be apart of a territory!");
                return;
            }
            
            if (!GetTerritoryManagerClient().IsTerritoryMember())
            {
                Footer.Show(true);
                footerText.SetText("You need to be apart of a territory!");
                return;
            }

            if (!GetTerritoryManagerClient().IsInOwnTerritory())
            {
                Footer.Show(true);
                footerText.SetText("You need to be inside your territory!");
                return;
            }

            if (GetTerritoryManagerClient().GetCurrentTerritoryByLocation().GetDecayStatus())
            {
                Footer.Show(true);
                footerText.SetText("Your territory is decaying!");
                return;
            }

            if (GetTerritoryManagerClient().GetCurrentTerritoryByLocation().IsMaxObjectReached())
            {
                Footer.Show(true);
                footerText.SetText("You have reached the max object limit in your territory!");
                return;
            }

            if (Footer.IsVisible())
                Footer.Show(false);
        }
        else
        {
            if (layoutRoot)
                layoutRoot.Show(false);
        }
    }
}

ref BaseBuildingControlsMenu g_BaseBuildingControlsMenu;
BaseBuildingControlsMenu GetBaseBuildingControlsMenu()
{
    if (!g_BaseBuildingControlsMenu)
        g_BaseBuildingControlsMenu = new BaseBuildingControlsMenu;
    return g_BaseBuildingControlsMenu;
}