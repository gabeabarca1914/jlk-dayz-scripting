/**
 * CreateTerritoryMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Nov 07 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class CreateTerritoryMenu: UIScriptedMenu
{
    PlayerBase m_Player;
    
    vector m_TerritoryFlagPosition;
    vector m_TerritoryFlagOrientation;

    EditBoxWidget territoryNameBox;
    
    ButtonWidget cancelBtn;
    ButtonWidget confirmBtn;

    Widget FlagPreviewPanel;

    TextListboxWidget FlagListBox;

    ItemPreviewWidget FlagPreview;

    EntityAI PreviewFlagObject;

    bool isOpen = false;

    int itemRotationX;
	int itemRotationY;
	int itemScaleDelta;
	vector itemOrientation;

    ref array<string> VanillaFlags = 
    {
        "Flag_Chernarus",
        "Flag_Chedaki",
        "Flag_NAPA",
        "Flag_CDF",
        "Flag_Livonia",
        "Flag_Altis",
        "Flag_SSahrani",
        "Flag_NSahrani",
        "Flag_DayZ",
        "Flag_LivoniaArmy",
        "Flag_White",
        "Flag_Bohemia",
        "Flag_APA",
        "Flag_UEC",
        "Flag_Pirates",
        "Flag_Cannibals",
        "Flag_Bear",
        "Flag_Wolf",
        "Flag_BabyDeer",
        "Flag_Rooster",
        "Flag_LivoniaPolice",
        "Flag_CMC",
        "Flag_TEC",
        "Flag_CHEL",
        "Flag_Zenit",
        "Flag_HunterZ",
        "Flag_BrainZ",
        "Flag_Refuge",
        "Flag_RSTA",
        "Flag_Snake"
    };

    void CreateTerritoryMenu(PlayerBase plyer, vector pos, vector ori) 
    {
        m_Player = plyer;

        m_TerritoryFlagPosition = pos;
        m_TerritoryFlagOrientation = ori;
    }

    void ~CreateTerritoryMenu() 
    {
        OnClose();
    }

    override Widget Init() 
    {
        layoutRoot = GetGame().GetWorkspace().CreateWidgets("Loaded_TerritorySystem/Layouts/EXPNameTerritoryMenu.layout");
        layoutRoot.Show(true);

        territoryNameBox = EditBoxWidget.Cast(layoutRoot.FindAnyWidget("territoryNameBox"));
        cancelBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("cancelBtn"));
        confirmBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("confirmBtn"));
        FlagPreviewPanel = Widget.Cast(layoutRoot.FindAnyWidget("FlagPreviewPanel"));
        FlagListBox = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("FlagListBox"));
        FlagPreview = ItemPreviewWidget.Cast(layoutRoot.FindAnyWidget("FlagPreview"));
    
        return layoutRoot;
    }

    void OnShow() 
    {
        g_NameTerritoryMenu = this;

        isOpen = true;

        GetGame().GetInput().ChangeGameFocus(1);
        GetGame().GetUIManager().ShowUICursor(true);
        GetGame().GetMission().GetHud().Show(false);
        PPEffects.SetBlurInventory(1);

        PopulateListBox();
    }

    void OnClose() 
    {
        g_NameTerritoryMenu = NULL;

        isOpen = false;
        
        SetFocus(NULL);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);
        PPEffects.SetBlurInventory(0);

        Close();
    }

    void Hide()
    {
        if (!g_Game.GetUIManager().IsMenuOpen(MENU_INGAME))
        {
            g_Game.GetUIManager().CloseAll();
            GetGame().GetInput().ResetGameFocus();
            GetGame().GetUIManager().ShowUICursor(false);
            GetGame().GetMission().GetHud().Show(true);
        }

        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);
        
        layoutRoot.Show(false);
    }

    void PopulateListBox() 
    {
        FlagListBox.ClearItems();

        for (int i = 0; i < GetTerritoryManagerClient().FlagConfig.FlagList.Count(); i++)
        {
            string flagClassname = GetTerritoryManagerClient().FlagConfig.FlagList[i];
            
            if (VanillaFlags.Find(flagClassname) != -1)
                flagClassname.Replace("Flag_", "");

            if (flagClassname && flagClassname != string.Empty)
                FlagListBox.AddItem(flagClassname, null, 0);
        }
    }

    override bool OnMouseButtonDown(Widget w, int x, int y, int button)
    {
        super.OnMouseButtonDown(w, x, y, button);
        GetGame().GetDragQueue().Call(this, "UpdateRotation");
        g_Game.GetMousePos(itemRotationX, itemRotationY);
        return true;
    }

    void UpdateRotation(int mouse_x, int mouse_y, bool is_dragging)
    {
        vector o = itemOrientation;
        o[0] = o[0] + (itemRotationY - mouse_y);
        o[1] = o[1] - (itemRotationX - mouse_x);

        FlagPreview.SetModelOrientation(o);

        if (!is_dragging) 
            itemOrientation = o;
    }

    override bool OnMouseWheel(Widget w, int x, int y, int wheel)
    {
        super.OnMouseWheel(w, x, y, wheel);

        if (w == FlagPreviewPanel)
        {
            itemScaleDelta = wheel;
            UpdateScale();
        }
        return true;
    }

    void UpdateScale()
    {
        float w, h, x, y;
        FlagPreview.GetPos(x, y);
        FlagPreview.GetSize(w, h);
        w = w + (itemScaleDelta / 4);
        h = h + (itemScaleDelta / 4);
        if (w > 0.5 && w < 3)
        {
            FlagPreview.SetSize(w, h);

            // align to center
            int screen_w, screen_h;
            GetScreenSize(screen_w, screen_h);
            float new_x = x - (itemScaleDelta / 8);
            float new_y = y - (itemScaleDelta / 8);
            FlagPreview.SetPos(new_x, new_y);
        }
    }

    override bool OnClick(Widget w, int x, int y, int button) 
    {
        super.OnClick(w, x, y, button);
        
        switch (w)
        {
            case cancelBtn:
                return OnCancelBtnClick();
            case confirmBtn:
                return OnConfirmBtnClick();
            case FlagListBox:
                return OnListBoxClick();
            default:
                break;
        }
        return true;
    }
     
    bool OnListBoxClick() 
    {
        if (GetSelectedFlag() != string.Empty)
            SpawnPreview();
        return true;
    }

    string GetSelectedFlag()
    {
        int index = FlagListBox.GetSelectedRow();
        if (index != -1)
        {
            string classname;
            FlagListBox.GetItemText(index, 0, classname);
            
            //--- Attempt to find classname in vanilla flags before altering the variable
            if (VanillaFlags.Find("Flag_" + classname) != -1)
                classname = "Flag_" + classname;

            return classname;
        }

        return string.Empty;
    }

    void SpawnPreview() 
    {
        if (PreviewFlagObject) 
            GetGame().ObjectDelete(PreviewFlagObject);

        PreviewFlagObject = EntityAI.Cast( GetGame().CreateObject(GetSelectedFlag(), "0 0 0", ECE_PLACE_ON_SURFACE) );

        FlagPreview.SetItem(PreviewFlagObject);
        FlagPreview.SetModelPosition(Vector(0, 0, 0.5));
        FlagPreview.SetModelOrientation(Vector(0, 0, 0));
        itemOrientation = Vector(0, 0, 0);
    }

    bool OnCancelBtnClick()
    {
        Hide();
        return true;
    }

    bool OnConfirmBtnClick() 
    {
        //--- Get the text content of the name box
        string name = territoryNameBox.GetText();

        //--- Ensure the name is long enough
        if (name.Length() >= 5)
        {
            if (GetSelectedFlag() != string.Empty)
            {
                GetRPCManager().SendRPC("Loaded_Territory_Server", "CreateTerritoryRequest", new Param5<string, PlayerBase, vector, vector, string>(name, m_Player, m_TerritoryFlagPosition, m_TerritoryFlagOrientation, GetSelectedFlag()), true, NULL);
                Hide();
            }
            else if (GetSelectedFlag() == string.Empty)
                NotificationSystem.AddNotificationExtended(3, "Territory Creation", "You Need To Select A Flag First!", "set:ccgui_enforce image:Icon40Move");
        }
        else
            NotificationSystem.AddNotificationExtended(3, "Territory Creation", "Your Territory Name Is Too Short!", "set:ccgui_enforce image:Icon40Move");
        
        return true;
    }
}

ref CreateTerritoryMenu g_NameTerritoryMenu;
CreateTerritoryMenu GetNameTerritoryMenu(PlayerBase plyer, vector pos, vector ori)
{
    if (!g_NameTerritoryMenu)
        g_NameTerritoryMenu = new CreateTerritoryMenu(plyer, pos, ori);
    return g_NameTerritoryMenu;
}