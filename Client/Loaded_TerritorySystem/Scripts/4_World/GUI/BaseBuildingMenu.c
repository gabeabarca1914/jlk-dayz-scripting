/**
 * BaseBuildingMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Nov 07 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class BaseBuildingMenu : UIScriptedMenu
{
    void BaseBuildingMenu() {

    }

    void ~BaseBuildingMenu() {
        OnClose();
    }

    Widget mainPanel;
    Widget infoPanel;
    Widget itemPreviewPanel;
    ItemPreviewWidget selectedItemPreview;
    TextListboxWidget needPartsList;
    TextListboxWidget needToolsList;
    ButtonWidget craftBtn;
    ButtonWidget closeBtn;
    ScrollWidget previewPanel;
    WrapSpacerWidget spacer;

    bool isOpen = false;


    private int itemRotationX;
	private int itemRotationY;
	private int itemScaleDelta;
	private vector itemOrientation;

    EntityAI selectedItem;
    ref array<ref EntityAI> itemPrewiews = new array<ref EntityAI>;

    LoadedRecipe m_Recipe;

    override Widget Init() {
        layoutRoot = GetGame().GetWorkspace().CreateWidgets("Loaded_TerritorySystem/Layouts/EXPBaseBuildingMenu.layout");
        layoutRoot.Show(false);

        mainPanel = layoutRoot.FindAnyWidget("main_panel");
        itemPreviewPanel = layoutRoot.FindAnyWidget("itemPreviewPanel");
        previewPanel = ScrollWidget.Cast(layoutRoot.FindAnyWidget("preview_panel"));
        spacer = WrapSpacerWidget.Cast(layoutRoot.FindAnyWidget("spacer"));
        infoPanel = layoutRoot.FindAnyWidget("info_panel");
        selectedItemPreview = ItemPreviewWidget.Cast(layoutRoot.FindAnyWidget("selected_item_preview"));
        needPartsList = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("needed_parts"));
        needToolsList = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("needed_tools"));
        craftBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("craft_btn"));
        closeBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("close_btn"));

        return layoutRoot;
    }

    void OnShow() {  
        isOpen = true;
        
        GetGame().GetInput().ChangeGameFocus(1);
        GetGame().GetUIManager().ShowUICursor(true);
        GetGame().GetMission().GetHud().Show(false);

        PPEffects.SetBlurInventory(1);
        LoadRecipes();
    }

    void OnClose() {
        isOpen = false;

        SetFocus(NULL);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);

        PPEffects.SetBlurInventory(0);

        if (selectedItem) {
            GetGame().ObjectDelete(selectedItem);
        }
        
        for (int i = 0; i < itemPrewiews.Count(); i++) {
            GetGame().ObjectDelete(itemPrewiews[i]);
        }
        
        Close();
    }

    void LoadRecipes() 
    {
        Loaded_Territory NearestTerritory;
        array<ref LoadedRecipe> list = GetBaseBuildingManagerClient().Recipes;
        foreach (LoadedRecipe item: list)
        {
            if (item.PreviewClassName.Contains("Workbench"))
            {
                AddRecipeItem(item);
                continue;
            }

            if (item.PreviewClassName.Contains("T1"))
            {
                if (GetBaseBuildingManagerClient().Config.RequireT1WorkbenchInTerritory)
                {
                    NearestTerritory = GetTerritoryManagerClient().GetCurrentTerritoryByLocation(GetGame().GetPlayer().GetPosition());
                    if (NearestTerritory && NearestTerritory.TerritoryHasItem("T1_Workbench"))
                        AddRecipeItem(item);
                }
                else
                    AddRecipeItem(item);
            }

            if (item.PreviewClassName.Contains("T2"))
            {
                if (GetBaseBuildingManagerClient().Config.RequireT2WorkbenchInTerritory)
                {
                    NearestTerritory = GetTerritoryManagerClient().GetCurrentTerritoryByLocation(GetGame().GetPlayer().GetPosition());
                    if (NearestTerritory && NearestTerritory.TerritoryHasItem("T2_Workbench"))
                        AddRecipeItem(item);
                }
                else
                    AddRecipeItem(item);
            }
            

            if (item.PreviewClassName.Contains("T3"))
            {
                if (GetBaseBuildingManagerClient().Config.RequireT3WorkbenchInTerritory)
                {
                    NearestTerritory = GetTerritoryManagerClient().GetCurrentTerritoryByLocation(GetGame().GetPlayer().GetPosition());
                    if (NearestTerritory && NearestTerritory.TerritoryHasItem("T3_Workbench"))
                        AddRecipeItem(item);
                }
                else
                    AddRecipeItem(item);
            }
        }
    }

    void AddRecipeItem(LoadedRecipe recipe) {

        Widget recipeItem = GetGame().GetWorkspace().CreateWidgets("Loaded_TerritorySystem/Layouts/EXPBaseBuildingRecipeItem.layout", spacer);

        ItemPreviewWidget itemPreviewWidget = ItemPreviewWidget.Cast(recipeItem.FindAnyWidget("itemPreviewWidget"));
        Widget headerPanel = recipeItem.FindAnyWidget("namePanel");
        TextWidget itemName = TextWidget.Cast(recipeItem.FindAnyWidget("itemName"));
        ButtonWidget itemButton = ButtonWidget.Cast(recipeItem.FindAnyWidget("itemButton"));
        MultilineTextWidget itemDescription = MultilineTextWidget.Cast(recipeItem.FindAnyWidget("ItemDescription"));

        itemButton.SetUserData(recipe);

        if (itemPreviewWidget) 
        {
            EntityAI previewItem = GetGame().CreateObject(recipe.PreviewClassName, "0 0 0", true, false, true);

            itemPrewiews.Insert(previewItem);

            itemPreviewWidget.SetItem(previewItem);
            itemPreviewWidget.SetModelPosition(Vector(0, 0, 0.5));
            itemPreviewWidget.SetModelOrientation("0 0 0");

            recipeItem.SetSize(0.23, 0.23);
            
            itemName.SetText(recipe.Name);

            itemDescription.SetText(recipe.Description);

            if (recipe.PreviewClassName.Contains("T1"))
            {
                headerPanel.SetColor(ARGB(255, 36, 120, 244));
            }
            
            if (recipe.PreviewClassName.Contains("T2")) 
            {
                headerPanel.SetColor(ARGB(255, 240, 140, 46));
            }
            
            if (recipe.PreviewClassName.Contains("T3")) 
            {
                headerPanel.SetColor(ARGB(255, 201, 55, 210));
            }
        }
    }

    void LoadItemInfo(LoadedRecipe recipe) {
        ItemPreviewWidget selected_item_preview = ItemPreviewWidget.Cast(infoPanel.FindAnyWidget("selected_item_preview"));
        TextWidget itemName = TextWidget.Cast(infoPanel.FindAnyWidget("itemName"));
        if (selected_item_preview && itemName)
        {
            if (selectedItem) GetGame().ObjectDelete(selectedItem);
            
            selectedItem = GetGame().CreateObject(recipe.PreviewClassName, "0 0 0", true, false, true);

            selected_item_preview.SetItem(selectedItem);
            selected_item_preview.SetModelPosition(Vector(0, 0, 0.5));

            needPartsList.ClearItems();
            needToolsList.ClearItems();

            for (int i = 0; i < recipe.RecipeItems.Count(); i++) {
                
                needPartsList.AddItem(recipe.RecipeItems[i].DisplayName + " x " + recipe.RecipeItems[i].Quantity, NULL, 0);
            }

            for (int x = 0; x < recipe.RecipeTools.Count(); x++){
                needToolsList.AddItem(recipe.RecipeTools[x].DisplayName, NULL, 0);
            }

            selectedItemPreview.SetModelOrientation(Vector(0, 0, 0));
            itemOrientation = Vector(0, 0, 0);

            itemName.SetText(recipe.Name);
        }
    }

    override bool OnClick(Widget w, int x, int y, int button) {
        super.OnClick(w, x, y, button);

        if ( w == closeBtn ) {
            OnClose();
        }

        if ( w == craftBtn ) {
            if (m_Recipe)
                GetRPCManager().SendRPC( "LoadedBaseBuildingManager_Server", "CraftRequest", new Param2< ref LoadedRecipe, Man >( m_Recipe, GetGame().GetPlayer()), true, NULL );
        }

        if (w.GetParent().GetParent() == spacer) { 
            m_Recipe = NULL;
            w.GetUserData(m_Recipe);
            if (m_Recipe) {
                LoadItemInfo(m_Recipe);
            }
        }

        return false;
    }

    override bool OnMouseEnter(Widget w, int x, int y) {
        super.OnMouseEnter(w, x, y);

        if (w.GetParent().GetParent() == spacer)
        {
            w.SetColor(ARGB(127, 88, 92, 96));
        }
        
        return false;
    }

    override bool OnMouseLeave(Widget w, Widget enterW, int x, int y) {
        super.OnMouseLeave(w, enterW, x, y);

        if (w.GetParent().GetParent() == spacer)
        {
            w.SetColor(ARGB(127, 63, 68, 73));
        }

        return false;
    }

    override bool OnMouseButtonDown(Widget w, int x, int y, int button)
    {
        super.OnMouseButtonDown(w, x, y, button);
        GetGame().GetDragQueue().Call(this, "UpdateRotation");
        g_Game.GetMousePos(itemRotationX, itemRotationY);
        return true;
    }

    void UpdateRotation(int mouse_x, int mouse_y, bool is_dragging)
    {
        vector o = itemOrientation;
        o[0] = o[0] + (itemRotationY - mouse_y);
        o[1] = o[1] - (itemRotationX - mouse_x);

        selectedItemPreview.SetModelOrientation(o);

        if (!is_dragging) itemOrientation = o;
    }

    override bool OnMouseWheel(Widget w, int x, int y, int wheel)
    {
        super.OnMouseWheel(w, x, y, wheel);

        if (w == itemPreviewPanel)
        {
            itemScaleDelta = wheel;
            UpdateScale();
        }
        return true;
    }

    void UpdateScale()
    {
        float w, h, x, y;
        selectedItemPreview.GetPos(x, y);
        selectedItemPreview.GetSize(w, h);
        w = w + (itemScaleDelta / 4);
        h = h + (itemScaleDelta / 4);
        if (w > 0.5 && w < 3)
        {
            selectedItemPreview.SetSize(w, h);

            // align to center
            int screen_w, screen_h;
            GetScreenSize(screen_w, screen_h);
            float new_x = x - (itemScaleDelta / 8);
            float new_y = y - (itemScaleDelta / 8);
            selectedItemPreview.SetPos(new_x, new_y);
        }
    }
}

ref BaseBuildingMenu g_BaseBuildingMenu;
BaseBuildingMenu GetBaseBuildingMenu()
{
    if (!g_BaseBuildingMenu)
        g_BaseBuildingMenu = new BaseBuildingMenu;
    return g_BaseBuildingMenu;
}