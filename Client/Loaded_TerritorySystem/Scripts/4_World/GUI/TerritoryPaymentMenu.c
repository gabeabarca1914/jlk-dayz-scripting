/**
 * TerritoryPaymentMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Nov 07 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class TerritoryPaymentMenu : UIScriptedMenu
{
    void TerritoryPaymentMenu();

    void ~TerritoryPaymentMenu() 
    {
        OnClose();
    }

    Widget mainPanel;
    Widget contentPanel;

    ButtonWidget UpgradeLvlBtn;
    ButtonWidget PayBtn;
    ButtonWidget UpgradeProtectionBtn;
	ButtonWidget CloseMenuBtn;

    TextListboxWidget partsList;
    TextListboxWidget TerritoryListBox;

    TextWidget timeText;
    TextWidget territoryName;
    TextWidget territoryLevel;
    TextWidget PartCountText;
    TextWidget TerritoryListHeader;
    TextWidget UpgradeLevelDesc;
    TextWidget UpgradeProtectionDesc;
    TextWidget priceText;
    TextWidget BaseRentFee;
    TextWidget ObjectFee;
    TextWidget MemberFee;
    TextWidget RegionFee;
    TextWidget TerritoryProtectionFee;

    override Widget Init()
    {
        layoutRoot = GetGame().GetWorkspace().CreateWidgets("Loaded_TerritorySystem/Layouts/EXPTerritoryPaymentMenu.layout");
        layoutRoot.Show(true);

        mainPanel = layoutRoot.FindAnyWidget("MainPanel");
        contentPanel = layoutRoot.FindAnyWidget("contentPanel");

        UpgradeLvlBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("UpgradeLvlBtn"));
        PayBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("PayBtn"));
        UpgradeProtectionBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("UpgradeProtectionBtn"));
		CloseMenuBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("CloseMenuBtn"));

        partsList = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("partsList"));
        TerritoryListBox = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("TerritoryList"));

        timeText = TextWidget.Cast(layoutRoot.FindAnyWidget("timeText"));
        territoryName = TextWidget.Cast(layoutRoot.FindAnyWidget("territoryName"));
        territoryLevel = TextWidget.Cast(layoutRoot.FindAnyWidget("territoryLevel"));
        PartCountText = TextWidget.Cast(layoutRoot.FindAnyWidget("PartCountText"));
        TerritoryListHeader = TextWidget.Cast(layoutRoot.FindAnyWidget("TerritoryListHeader"));
        UpgradeLevelDesc = TextWidget.Cast(layoutRoot.FindAnyWidget("UpgradeLevelDesc"));
        UpgradeProtectionDesc = TextWidget.Cast(layoutRoot.FindAnyWidget("UpgradeProtectionDesc"));
        priceText = TextWidget.Cast(layoutRoot.FindAnyWidget("priceText"));

        
        BaseRentFee = TextWidget.Cast(layoutRoot.FindAnyWidget("BaseRentFee"));
        ObjectFee = TextWidget.Cast(layoutRoot.FindAnyWidget("ObjectFee"));
        MemberFee = TextWidget.Cast(layoutRoot.FindAnyWidget("MemberFee"));
        RegionFee = TextWidget.Cast(layoutRoot.FindAnyWidget("RegionFee"));
        TerritoryProtectionFee = TextWidget.Cast(layoutRoot.FindAnyWidget("TerritoryProtection"));
            
        return layoutRoot;
    }

    void OnShow()
    {  
        g_TerritoryPaymentMenu = this;

        //UpdateMenu();

        PopulateTerritoryList();
        GetGame().GetInput().ChangeGameFocus(1);
        GetGame().GetUIManager().ShowUICursor(true);
        GetGame().GetMission().GetHud().Show(false);

        PPEffects.SetBlurInventory(1);

        //--- Player Cannot Press Buttons
        PayBtn.Enable(false);
        UpgradeLvlBtn.Enable(false);

        PayBtn.SetTextColor(ARGB( 255, 65, 65, 65 ));
        UpgradeLvlBtn.SetTextColor(ARGB( 255, 65, 65, 65 ));
    }

    void PopulateTerritoryList() 
    {
        if (GetTerritoryManagerClient().IsTerritoryMember()) 
        {
            //--- List of territories
            array<ref Loaded_Territory> Territories = GetTerritoryManagerClient().Territories;

            TerritoryListBox.ClearItems();

            for (int i = 0; i < Territories.Count(); i++)
            {
                Loaded_Territory territory = Territories[i];

                if (territory)
                {
                    TerritoryListBox.AddItem(territory.GetName() + " - " + territory.GetLevel().GetName(), GetTerritoryManagerClient().Territories[i], 0);
                }
            }
        }
    }

    void OnTerritoryListClick()
    {
        //--- Update UI
        if (GetSelectedTerritory())
        {
            UpdateTerritoryInfo();

            //--- Allow For Button Press
            PayBtn.Enable(true);
            UpgradeLvlBtn.Enable(true);

            PayBtn.SetTextColor(ARGB( 255, 76, 169, 255 ));
            UpgradeLvlBtn.SetTextColor(ARGB( 255, 76, 169, 255 ));
        }
    }
    
    Loaded_Territory GetSelectedTerritory()
    {
        int index = TerritoryListBox.GetSelectedRow();
        if (index != -1)
        {
            Loaded_Territory Territory;
            TerritoryListBox.GetItemData(index, 0, Territory);
            if (Territory)
                return Territory;
        }

        return null;
    }

    void UpdateTerritoryInfo()
    {
        if (GetTerritoryManagerClient().IsTerritoryMember())
        {
            Loaded_Territory territory = GetSelectedTerritory();
            if (territory) 
            {
                UpdatePartsList();

                territoryName.SetText(territory.GetName());
                territoryLevel.SetText(territory.Level.GetName());
                timeText.SetText(territory.GetRemainingLifeTimeDisplay() + " Remaining");

                array<Object> parts = territory.GetPartArray();
                float partsPrice = 0;
                foreach(Object BasePart: parts)
                    partsPrice += territory.GetItemPrice(BasePart.GetType());

                BaseRentFee.SetText(territory.GetLevel().GetPrice().ToString());
                ObjectFee.SetText(partsPrice.ToString());
                MemberFee.SetText((territory.Members.Count() * 400).ToString());
                RegionFee.SetText("N/A");
                TerritoryProtectionFee.SetText("N/A");
                
                priceText.SetText("Current Rent Owed $" + Math.Ceil(territory.CurrentRentOwed));

                //--- Next Territory Info
                int NextTerritoryLvlIndex = GetTerritoryManagerClient().Config.TerritoryLevels.Find(territory.GetLevel());
                
                //--- Cannot increment on 0
                LoadedTerritoryLevel NextTerritoryLvl;
                if (NextTerritoryLvlIndex == -1)
                    NextTerritoryLvl = GetTerritoryManagerClient().Config.TerritoryLevels[1];
                else
                    NextTerritoryLvl = GetTerritoryManagerClient().Config.TerritoryLevels[NextTerritoryLvlIndex++];
                
                if (NextTerritoryLvl)
                {
                    string NextTerritoryName    = NextTerritoryLvl.GetName();
                    int NextTerritoryObjCount   = NextTerritoryLvl.GetMaxObjectCount();
                    float NextTerritoryRadius   = NextTerritoryLvl.GetRadius();
                    int NextTerritoryPrice      = NextTerritoryLvl.GetPrice();

                    UpgradeLevelDesc.SetText(string.Format("Next Level: '%1' (%2 Objects - %3 m ) $%4", NextTerritoryName, NextTerritoryObjCount, NextTerritoryRadius, NextTerritoryPrice));
                }
            }
        }
    }

    void UpdatePartsList() 
    {
        Loaded_Territory territory = GetSelectedTerritory();
        map<string, int> partCountMap = territory.GetPartCountMap();

        int itemCount = 0;
        
        partsList.ClearItems();

        for (int i = 0; i < partCountMap.Count(); i++)
        {
            string currentIndexKey = partCountMap.GetKey(i);
            int currentIndexValue = partCountMap.GetElement(i);
            itemCount += currentIndexValue;
            
            partsList.AddItem(GetDisplayName(currentIndexKey) + " x " + currentIndexValue, null, 0);
        }
                
        PartCountText.SetText(string.Format("Total Parts In Territory : %1", itemCount));
    }

    void OnClose()
    {
        g_TerritoryPaymentMenu = null;
        
        SetFocus(null);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);

        PPEffects.SetBlurInventory(0);

        Close();
    }

    override bool OnClick(Widget w, int x, int y, int button)
    {
        super.OnClick(w, x, y, button);
        switch (w)
        {
            case PayBtn:
                if (GetSelectedTerritory())
                {
                    GetRPCManager().SendRPC("Loaded_Territory_Server", "PayTerritoryRequest", new Param1<ref Loaded_Territory>(GetSelectedTerritory()), true);
                    OnClose();
                }
                return true;
            break;

            case UpgradeLvlBtn:
                if (GetSelectedTerritory())
                {
                    GetRPCManager().SendRPC("Loaded_Territory_Server", "UpgradeTerritoryRequest", new Param1<ref Loaded_Territory>(GetSelectedTerritory()), true);
                    OnClose();
                }
                return true;
            break;
			
			case CloseMenuBtn:
				OnClose();
				return true;
			break;

            case TerritoryListBox:
                OnTerritoryListClick();
                return true;
            break;
        }
        return true;
    }

    void UpdateMenu() UpdateTerritoryInfo();

    void Hide()
    {
        SetFocus(null);
        
        if (!g_Game.GetUIManager().IsMenuOpen(MENU_INGAME))
        {
            g_Game.GetUIManager().CloseAll();
            GetGame().GetInput().ResetGameFocus();
            GetGame().GetUIManager().ShowUICursor(false);
            GetGame().GetMission().GetHud().Show(true);
        }

        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);

        layoutRoot.Show(false);
    }
}

ref TerritoryPaymentMenu g_TerritoryPaymentMenu;
TerritoryPaymentMenu GetTerritoryPaymentMenu()
{
    if (!g_TerritoryPaymentMenu)
        g_TerritoryPaymentMenu = new TerritoryPaymentMenu;
    return g_TerritoryPaymentMenu;
}