modded class MissionGameplay 
{
    Man                     player;
    PlayerBase              playerPB;
    DayZPlayerImplement		playerImp;
	
	UIScriptedMenu        	menu;
	Input                 	input;
    UIManager               uiManager;

    override void OnUpdate(float timeslice) 
    {
        super.OnUpdate(timeslice);

        EOnUpdate();
    }

    void EOnUpdate()
    {   
        player 		= GetGame().GetPlayer();
		playerPB 	= PlayerBase.Cast( player );
        playerImp 	= DayZPlayerImplement.Cast( player );

		if ( !playerPB || !player ) return;

		uiManager   = GetGame().GetUIManager();
        menu        = uiManager.GetMenu();
		input       = GetGame().GetInput();
        
        if (player) 
        {
            if (input.LocalPress("Loaded_BaseBuilding_OpenTerritoryManagement", true))
                GetTerritoryManagerClient().OpenTerritoryManagement();
            
            if (input.LocalPress("Loaded_BaseBuilding_OpenBaseBuilding", true))
                GetTerritoryManagerClient().OpenBaseBuildingMenu();
            
            //--- hologram rotation
            if (menu == NULL && playerPB.IsPlacingLocal() && playerPB.GetHologramLocal().GetParentEntity().PlacementCanBeRotated()) 
            {
                if (input.LocalPress("Loaded_BaseBuilding_Vector_Back", false) || input.LocalHold("Loaded_BaseBuilding_Vector_Back", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionYaw(3);
                    return;
                }
                
                if (input.LocalPress("Loaded_BaseBuilding_Vector_Forward", false) || input.LocalHold("Loaded_BaseBuilding_Vector_Forward", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionYaw(-3);
                    return;
                }

                if (input.LocalPress("Loaded_BaseBuilding_Vector_Right", false) || input.LocalHold("Loaded_BaseBuilding_Vector_Right", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionRoll(3);
                    return;
                }
                
                if (input.LocalPress("Loaded_BaseBuilding_Vector_Left", false) || input.LocalHold("Loaded_BaseBuilding_Vector_Left", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionRoll(-3);
                    return;
                }
            }
        }
    }

    override void OnKeyPress(int key)
	{
		super.OnKeyPress(key);
        bool isBuilding = PlayerBase.Cast(GetGame().GetPlayer()).IsPlacingLocal();

        if ((key == KeyCode.KC_B) && isBuilding) 
        {
            if (GetBaseBuildingManagerClient().GetBuildingMode() == BuildingMode.SNAP)
                GetBaseBuildingManagerClient().SetBuildingMode(BuildingMode.FREE);
            else
                GetBaseBuildingManagerClient().SetBuildingMode(BuildingMode.SNAP);
        }
    }

    override void OnMissionStart()
    {
		super.OnMissionStart();
		
        g_TerritoryManagerClient = null;
        GetTerritoryManagerClient();

        g_BaseBuildingManagerClient = null;
        GetBaseBuildingManagerClient();
	}
    
    override void OnMissionFinish()
	{
		super.OnMissionFinish();
        
        if (g_TerritoryManagerClient)
        {
            delete g_TerritoryManagerClient;
            g_TerritoryManagerClient = null;
        }

        if (g_BaseBuildingManagerClient)
        {
            delete g_BaseBuildingManagerClient;
            g_BaseBuildingManagerClient = null;
        }

        if (g_BaseBuildingControlsMenu)
        {
            delete g_BaseBuildingControlsMenu;
            g_BaseBuildingControlsMenu = null;
        }
	}
};