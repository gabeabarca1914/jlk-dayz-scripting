/**
* config.cpp
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

class CfgPatches
{
	class Loaded_TerritorySystem {
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]= {
			"DZ_Scripts",
			"DZ_Data",
			"DZ_Weapons_Lights",
			"HDSN_BreachingCharge",
			"RPC_Scripts",
			"SIX_TownNotifications",
			"Props"
		};
	};
};

class CfgAddons
{
	class PreloadBanks
	{
	};
	class PreloadAddons
	{
		class DayZ 
		{
			list[] = 
			{
				"HDSN_BreachingCharge",
				"Props",
				"SIX_TownNotifications"
			};
		};
	};
};


class CfgMods
{
	class Loaded_TerritorySystem {
		dir = "Loaded_TerritorySystem";
		picture = "";
		action = "";
		hideName = 1;
		hidePicture = 1;
		name = "Loaded_TerritorySystem";
		credits = "0";
		author = "6IX";
		authorID = "0"; 
		version = "1.0"; 
		extra = 0;
		type = "mod";
		inputs = "Loaded_TerritorySystem/inputs.xml";
		dependencies[] = { "Game", "World", "Mission" };
		class defs {
			class gameScriptModule {
				value = "";
				files[] = { "Loaded_TerritorySystem/Scripts/3_Game" };
			};

			class worldScriptModule {
				value = "";
				files[] = { "Loaded_TerritorySystem/Scripts/4_World" };
			};

			class missionScriptModule {
				value = "";
				files[] = { "Loaded_TerritorySystem/Scripts/5_Mission" };
			};
		};
	};
};

class CfgVehicles
{
	class Inventory_Base;
	class Edible_Base: Inventory_Base{};
	class Apple: Edible_Base {};
	class DebugApple: Apple
	{
		scope = 2;
		displayName = "Debug Apple";
		descriptionShort = "Apple for debuging idk";
	};
};