/**
 * MapSelectionServer.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 10 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class MapSelectionServer 
{
    ref array<ref MapLocation> m_MapLocationArray = new array<ref MapLocation>;

    [NonSerialized()]
    ref array<ref ActiveSession> m_ActiveMapSessions = new array<ref ActiveSession>;

    void MapSelectionServer() 
    {
        LoadConfigs();
        StartSessions();
    }

    void LoadConfigs() 
    {
        if (!FileExist("$profile:MapSelection"))
            MakeDirectory("$profile:MapSelection");
            
        if (!FileExist("$profile:MapSelection\\Config.json"))
        {
            CreateDefault();
            JsonFileLoader<MapSelectionServer>.JsonSaveFile("$profile:MapSelection\\Config.json", this );
        }
        else
        {
            JsonFileLoader<MapSelectionServer>.JsonLoadFile("$profile:MapSelection\\Config.json", this );
        }
    }

    void StartSessions() 
    {
        for (int i = 0; i < m_MapLocationArray.Count(); i++)
        {
            m_ActiveMapSessions.Insert(new ActiveSession(m_MapLocationArray[i]));
        }
    }

    void CreateDefault() 
    {
        MapLocation defaultTemplate = new MapLocation;
        
        defaultTemplate.MapLocationName = "Swarog";
        defaultTemplate.PMCSpawnPoints.Insert("4883.82 528.133 2205.56");
        defaultTemplate.PMCExtractPoints.Insert("5027.37 517.633 2151.96");

        m_MapLocationArray.Insert(defaultTemplate);
    }

    ActiveSession GetSessionByID(int id)
    {
        for (int i = 0; i < m_ActiveMapSessions.Count(); i++)
        {
            if (m_ActiveMapSessions[i].m_SessionID == id)
                return m_ActiveMapSessions[i];
        }

        return null;
    }
};

ref MapSelectionServer g_MapSelectionServer;
MapSelectionServer GetMapSelectionServer() 
{
    if (!g_MapSelectionServer)
        g_MapSelectionServer = new MapSelectionServer;
    return g_MapSelectionServer;
}