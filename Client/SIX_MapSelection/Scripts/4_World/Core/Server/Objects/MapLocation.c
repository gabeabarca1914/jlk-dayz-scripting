/**
 * MapLocation.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 10 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class MapLocation 
{
    string MapLocationName = string.Empty;
    int MaxPlayersAllowed = 25;
    int MapSessionTimeLength = 600; //--- Seconds
    int MapSessionCleanUpTime = 120; //--- Seconds
    
    ref TVectorArray PMCSpawnPoints = new TVectorArray;
    ref TVectorArray ScavagerSpawnPoints = new TVectorArray;

    ref TVectorArray PMCExtractPoints = new TVectorArray;
    ref TVectorArray ScavagerExtractPoints = new TVectorArray;

    void GetRandomSpawnPMC(out vector point) 
    {
        point = PMCSpawnPoints.GetRandomElement();
    }
};