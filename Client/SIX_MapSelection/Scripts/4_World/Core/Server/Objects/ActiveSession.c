/**
 * ActiveSession.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 10 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActiveSession 
{
    int m_SessionID;
    
    ref MapLocation m_Config;

    int m_SessionStatus = SessionStatusEnum.CLOSED;

    //--- List of players currently in session.
    ref array<Man> m_SessionPlayers = new array<Man>;

    ref array<ExtractTrigger> m_ExtractPoints = new array<ExtractTrigger>;
    
    void ActiveSession(MapLocation config)
    {
        //--- Get Random ID
        m_SessionID = Math.RandomInt(int.MIN, int.MAX);

        m_Config = config;

        BuildExtractPoints();
    }

    //--- Add player to session
    void AddPlayer(Man player) 
    {
        //--- Make sure player object is not null
        if (!player)
            return;

        //--- Player not in array already
        if (m_SessionPlayers.Find(player) == -1)
            m_SessionPlayers.Insert(player);
        
        //--- TP Player to random point

        //--- Get Random Point in map area
        vector randomSpawnPoint;
        m_Config.GetRandomSpawnPMC(randomSpawnPoint);

        player.SetPosition(randomSpawnPoint);
    }

    void RemovePlayer(Man player)
    {
        //--- Make sure player object is not null
        if (!player)
            return;

        //--- Player already not in array
        if (m_SessionPlayers.Find(player) != -1)
            m_SessionPlayers.Remove(m_SessionPlayers.Find(player));
    }

    void SlaySessionPlayers() 
    {
        for (int i = 0; i < m_SessionPlayers.Count(); i++)
        {
            if (m_SessionPlayers[i] && m_SessionPlayers[i].IsAlive())
            {
                m_SessionPlayers[i].SetHealth(0);
                
                if (m_SessionPlayers.Find(m_SessionPlayers[i]) != -1)
                    m_SessionPlayers.Remove(m_SessionPlayers.Find(m_SessionPlayers[i]));
            }
        }
    }

    void BuildExtractPoints() 
    {
        for (int i = 0; i < m_Config.PMCExtractPoints.Count(); i++)
        {
            ExtractTrigger m_Trigger = ExtractTrigger.Cast(GetGame().CreateObject("ExtractTrigger", m_Config.PMCExtractPoints[i]));
            m_Trigger.SetCollisionCylinder(3, 3);   //--- 3x3 Cylinder
            m_Trigger.SetSessionParentID(m_SessionID);
            m_ExtractPoints.Insert(m_Trigger);
        }
    }
};