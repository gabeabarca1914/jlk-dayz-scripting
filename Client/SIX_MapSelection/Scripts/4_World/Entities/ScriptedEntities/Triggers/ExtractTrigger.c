/**
 * ExtractTrigger.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 10 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ExtractTrigger extends CylinderTrigger 
{
    //--- Session ID
    int m_ParentSessionID;

	ref array<ref ExtractingPlayer> m_ExtractingPlayers = new array<ref ExtractingPlayer>;

    override void OnEnter(Object obj)
    {
		if (obj.IsMan() && PlayerBase.Cast(obj).GetIdentity())
		{
			Register(PlayerBase.Cast(obj));
		}
    }

    override void OnLeave(Object obj)
    {
        if (obj.IsMan() && PlayerBase.Cast(obj).GetIdentity())
    	{
			Discharge(PlayerBase.Cast(obj));
		}
    }

    void SetSessionParentID(int id) 
    {
        m_ParentSessionID = id;
    }

	void Register(PlayerBase player) 
	{
		//--- crash check
		if (!player)
			return;
		
		//--- Check if player is already registered
		for (int i = 0; i < m_ExtractingPlayers.Count(); i++)
		{
			//--- if current player is found in any of the current extracting player objects, dont create a new one.
			if (player == m_ExtractingPlayers[i].player)
				return;
		}

		//--- Create ExtractingPlayer object for current player. This will result in player being tp'd back to safezone.
		m_ExtractingPlayers.Insert(new ExtractingPlayer(player, m_ParentSessionID));
	}

	void Discharge(PlayerBase player)
	{
		//--- crash check
		if (!player)
			return;
		
		//--- Remove ExtractingPlayer object from memory and stop timer
		for (int i = 0; i < m_ExtractingPlayers.Count(); i++)
		{
			if (m_ExtractingPlayers[i].player == player)
				m_ExtractingPlayers[i].Discharge();
		}
	}
};