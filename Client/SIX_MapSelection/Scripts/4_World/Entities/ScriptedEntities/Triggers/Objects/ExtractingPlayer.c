class ExtractingPlayer 
{
    PlayerBase m_Player;
    int m_SessionID;

    ref Timer m_ExtractTimer;

    void ExtractingPlayer(PlayerBase plyer, int sessionid)
    {
        m_Player = plyer;
        m_SessionID = sessionid;

        StartTimer();
    }

    void ~ExtractingPlayer() 
    {
        m_ExtractTimer.Stop();
    }

    void Discharge(ExtractTrigger parent)
    {
        //--- Remove object from array
        parent.m_ExtractingPlayers.Remove(parent.m_ExtractingPlayers.Find(this));

        //--- Delete object out of memory
        delete this;
    }

    void StartTimer()
    {
        m_ExtractTimer = new Timer();

        m_ExtractTimer.Run(5, this, "ExtractPlayer");
    }

    //--- Homie is free
    void ExtractPlayer() 
    {
        //--- Check if homie died while trying to leave
        if (!m_Player || !m_Player.IsAlive())
            return;
            
        //--- TP Back to safezone
        vector safezonePoint = "3812.03 0 8500.38";
        safezonePoint[1] = GetGame().SurfaceY(safezonePoint[0], safezonePoint[2]);
        m_Player.SetPosition(safezonePoint);

        //--- Remove player from session by session id
        GetMapSelectionServer().GetSessionByID(m_SessionID).RemovePlayer(m_Player);
    }
};