/**
 * RuleAgreeMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 03 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class RuleAgreeMenu extends UIScriptedMenu 
{
    ButtonWidget        m_IAgree;
    TextListboxWidget   m_ServerRulesList;

    ref TStringArray RulesList = new TStringArray;

    void RuleAgreeMenu(TStringArray paramRules) 
    {
        RulesList = paramRules;
    }

    void ~RuleAgreeMenu() 
    {
        if ( layoutRoot ) 
        {
            layoutRoot.Unlink();
        }
    }

    override Widget Init()
    {
        layoutRoot          = GetGame().GetWorkspace().CreateWidgets("SIX_RuleAgreeMenu/Scripts/Layouts/IntroAgreementScreen.layout");
        m_ServerRulesList   = TextListboxWidget.Cast( layoutRoot.FindAnyWidget( "RuleListBox" ) );
        m_IAgree            = ButtonWidget.Cast( layoutRoot.FindAnyWidget( "bAgree" ) );
        
        layoutRoot.Show(true);

        return layoutRoot;
    }

    override void OnShow()
    {
        super.OnShow();
        PPEffects.SetBlurMenu( 1 );
        m_ServerRulesList.ClearItems();
        LoadServerRules();
    }

    void LoadServerRules() 
    {
        for ( int i = 0; i < RulesList.Count(); i++ ) 
        {
            string index = RulesList[i];
            m_ServerRulesList.AddItem( index, null, 0 );
        }
    }
    
    override void OnHide() 
    {
        super.OnHide();
        PPEffects.SetBlurMenu( 0 );
        GetGame().GetMission().PlayerControlEnable(false);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor( false );
        GetGame().GetMission().GetHud().Show( true );
    }

    override bool OnClick(Widget w, int x, int y, int button)
    {
        switch (w) {
            case m_IAgree: {
                //--- Close Ui and update config
                GetRPCManager().SendRPC("EXPRuleAgreeRPCs", "AgreesToRules", new Param1<PlayerBase>(PlayerBase.Cast(GetGame().GetPlayer())), true);
                Close();
                return true;
            }
        }
        return super.OnClick(w, x, y, button);
    }
};

ref RuleAgreeMenu g_RuleAgreeMenu;
RuleAgreeMenu GetRuleAgreeMenu(TStringArray rules) 
{
    if (!g_RuleAgreeMenu)
        g_RuleAgreeMenu = new RuleAgreeMenu(rules);
    return g_RuleAgreeMenu;
}