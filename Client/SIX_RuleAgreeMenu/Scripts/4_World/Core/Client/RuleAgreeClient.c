/**
 * RuleAgreeClient.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 03 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class RuleAgreeClient 
{
    void RuleAgreeClient() 
    {
        GetRPCManager().AddRPC("RuleAgreeClient", "CreateRuleAgreeMenu", this);
    }

    void CreateRuleAgreeMenu(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref TStringArray> data;
        if (!ctx.Read(data)) return;

        UIManager uiManager = g_Game.GetUIManager();
        uiManager.ShowScriptedMenu(GetRuleAgreeMenu(data.param1), null);
    }
};

ref RuleAgreeClient g_RuleAgreeClient;
RuleAgreeClient GetRuleAgreeClient() 
{
    if (!g_RuleAgreeClient)
        g_RuleAgreeClient = new RuleAgreeClient;
    return g_RuleAgreeClient;
}