/**
 * RuleAgreeServer.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Dec 03 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class RuleAgreeServer 
{
    ref TStringArray m_RulesList = new TStringArray;

    void RuleAgreeServer() 
    {
        GetRPCManager().AddRPC("RuleAgreeServer", "UpdateRuleValue", this);

        //--- Make sure the folder structure is there
        if (!FileExist("$profile:\\RuleAgreeMenu")) 
            MakeDirectory("$profile:\\RuleAgreeMenu");
        
        if (!FileExist(PlayerDBPath)) 
            MakeDirectory(PlayerDBPath);
            
        //--- Check if config is created and load it - if not, create one
        if (!FileExist(RuleAgreeConfigPath))
        {
            JsonFileLoader<RuleAgreeServer>.JsonSaveFile(RuleAgreeConfigPath, this);
        }
        else
            JsonFileLoader<RuleAgreeServer>.JsonLoadFile(RuleAgreeConfigPath, this);
    }

    void Register(PlayerIdentity plyident) 
    {
        string ID = plyident.GetPlainId();
        if (!FileExist(string.Format("%1\\%2.json", PlayerDBPath, ID)))
        {
            //--- Create player profile
            PlayerProfile profile = new PlayerProfile;
            profile.PlayerID = ID;
            JsonFileLoader<PlayerProfile>.JsonSaveFile(string.Format("%1\\%2.json", PlayerDBPath, ID), profile);
        }

        CheckIfAgreedToRules();
    }

    void CheckIfAgreedToRules(PlayerIdentity plyident) 
    {
        string ID = plyident.GetPlainId();
        if (!FileExist(string.Format("%1\\%2.json", PlayerDBPath, ID)))
            Register(plyident);
        
        PlayerProfile profile = new PlayerProfile;
        JsonFileLoader<PlayerProfile>.JsonLoadFile(string.Format("%1\\%2.json", PlayerDBPath, ID), profile);

        if (profile)
        {
            if (!profile.AgreedToRules)
                GetRPCManager().SendRPC("RuleAgreeClient", "CreateRuleAgreeMenu", new Param1<ref TStringArray>(m_RulesList), true, plyident);
        }
    }

    void UpdateRuleValue(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<bool> data;
        if (!ctx.Read(data)) return;

        string ID = sender.GetPlainId();
        if (!FileExist(string.Format("%1\\%2.json", PlayerDBPath, ID)))
            Register(sender);
        
        //--- Load
        PlayerProfile profile = new PlayerProfile;
        JsonFileLoader<PlayerProfile>.JsonLoadFile(string.Format("%1\\%2.json", PlayerDBPath, ID), profile);

        //--- Write
        profile.AgreedToRules = data.param1;
        JsonFileLoader<PlayerProfile>.JsonSaveFile(string.Format("%1\\%2.json", PlayerDBPath, ID), profile);
    }
};

ref RuleAgreeServer g_RuleAgreeServer;
RuleAgreeServer GetRuleAgreeServer() 
{
    if (!g_RuleAgreeServer)
        g_RuleAgreeServer = new RuleAgreeServer;
    return g_RuleAgreeServer;
}