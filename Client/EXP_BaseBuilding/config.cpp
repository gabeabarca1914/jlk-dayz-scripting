#define EXP_BASEBUILDING

class CfgPatches
{
    class EXP_BaseBuilding
    {
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] =
        {
            "DZ_Data",
            "RPC_Scripts",
			"HDSN_BreachingCharge",
			"Props",
			"EXP_Script"
        };
    };
};
class CfgMods
{
    class EXP_BaseBuilding
    {
        dir = "EXP_BaseBuilding";
        picture = "";
        action = "";
        hideName = 1;
        hidePicture = 1;
        name = "EXP_BaseBuilding";
        credits = "SIX & Shix";
        author = "SIX & Shix";
        authorID = "0";
        version = "1.0";
        extra = 0;
		inputs = "EXP_BaseBuilding/inputs.xml";
        type = "mod";
        dependencies[] = {"Mission", "World", "Game"};

        class defs
        {
            class gameScriptModule
            {
                value = "";
                files[] = {"EXP_BaseBuilding/scripts/3_Game"};
            };
            class worldScriptModule
            {
                value = "";
                files[] = {"EXP_BaseBuilding/scripts/4_World"};
            };
            class missionScriptModule
            {
                value = "";
                files[] = {"EXP_BaseBuilding/scripts/5_Mission"};
            };
        };
    };
};

class CfgAddons
{
	class PreloadBanks
	{
	};
	class PreloadAddons
	{
		class DayZ 
		{
			list[] = 
			{
				"HDSN_BreachingCharge",
				"Props",
				"EXP_Script"
			};
		};
	};
};

class CfgVehicles
{
    class Inventory_Base;
    class EXPTerritoryFlagKit: Inventory_Base
	{
		scope=2;
		displayName="$STR_CfgVehicles_TerritoryFlagKit0";
		descriptionShort="$STR_CfgVehicles_TerritoryFlagKit1";
		model="\DZ\gear\camping\territory_flag_kit.p3d";
		rotationFlags=17;
		itemSize[]={1,5};
		weight=280;
		itemBehaviour=1;
		attachments[]=
		{
			"Rope"
		};
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints=200;
					healthLevels[]=
					{
						
						{
							1,
							{}
						},
						
						{
							0.69999999,
							{}
						},
						
						{
							0.5,
							{}
						},
						
						{
							0.30000001,
							{}
						},
						
						{
							0,
							{}
						}
					};
				};
				class GlobalArmor
				{
					class Projectile
					{
						class Health
						{
							damage=0;
						};
						class Blood
						{
							damage=0;
						};
						class Shock
						{
							damage=0;
						};
					};
					class FragGrenade
					{
						class Health
						{
							damage=0;
						};
						class Blood
						{
							damage=0;
						};
						class Shock
						{
							damage=0;
						};
					};
				};
			};
		};
		class AnimationSources
		{
			class AnimSourceShown
			{
				source="user";
				animPeriod=0.0099999998;
				initPhase=0;
			};
			class AnimSourceHidden
			{
				source="user";
				animPeriod=0.0099999998;
				initPhase=1;
			};
			class Inventory: AnimSourceHidden
			{
			};
			class Placing: AnimSourceHidden
			{
			};
		};
		class AnimEvents
		{
			class SoundWeapon
			{
				class crafting_1
				{
					soundSet="FenceKit_crafting_1_SoundSet";
					id=1111;
				};
				class crafting_2
				{
					soundSet="FenceKit_crafting_2_SoundSet";
					id=1112;
				};
				class crafting_3
				{
					soundSet="FenceKit_crafting_3_SoundSet";
					id=1113;
				};
				class crafting_4
				{
					soundSet="FenceKit_crafting_4_SoundSet";
					id=1114;
				};
				class crafting_5
				{
					soundSet="FenceKit_crafting_5_SoundSet";
					id=1115;
				};
			};
		};
	};
	class EXPTerritoryFlagKitPlacing: EXPTerritoryFlagKit
	{
		displayName="This is a hologram";
		descriptionShort="Nothing to see here, move along";
		scope=2;
		model="\DZ\gear\camping\territory_flag_kit_placing.p3d";
		storageCategory=10;
		hiddenSelections[]=
		{
			"placing"
		};
		hiddenSelectionsTextures[]=
		{
			"dz\gear\consumables\data\pile_of_planks_co.tga"
		};
		hiddenSelectionsMaterials[]=
		{
			"dz\gear\camping\data\fence_pile_of_planks.rvmat"
		};
		hologramMaterial="tent_medium";
		hologramMaterialPath="dz\gear\camping\data";
		alignHologramToTerain=0;
		slopeTolerance=0.30000001;
	};
};