/**
 * missionGameplay.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class MissionGameplay
{
    Man                     player;
    PlayerBase              playerPB;
    DayZPlayerImplement		playerImp;
	
	UIScriptedMenu        	menu;
	Input                 	input;
    UIManager               uiManager;

    override void OnUpdate(float timeslice) 
    {
        super.OnUpdate(timeslice);

        EOnUpdate();
    }

    void EOnUpdate()
    {   
        player 		= GetGame().GetPlayer();
		playerPB 	= PlayerBase.Cast( player );
        playerImp 	= DayZPlayerImplement.Cast( player );

		if ( !playerPB || !player ) return;

		uiManager   = GetGame().GetUIManager();
        menu        = uiManager.GetMenu();
		input       = GetGame().GetInput();
        
        if (player) 
        {
            if (input.LocalPress("EXP_BaseBuilding_OpenTerritoryManagement", true))
            {
                uiManager.CloseAll();
                uiManager.ShowScriptedMenu(GetTerritoryManagementMenu(), NULL);
            }

            if (input.LocalPress("EXP_BaseBuilding_OpenBaseBuilding", true))
            {
                if (!GetTerritoryManagerClient().IsTerritoryMember())
                {    
                    NotificationSystem.AddNotificationExtended(3, "Territory Management", "You need to be apart of a territory first!", "set:ccgui_enforce image:Icon40Move");
                    return;
                }

                if (!GetTerritoryManagerClient().IsInOwnTerritory())
                {    
                    NotificationSystem.AddNotificationExtended(3, "Territory Management", "You need to be in your own territory!", "set:ccgui_enforce image:Icon40Move");
                    return;
                }

                uiManager.CloseAll();
                uiManager.ShowScriptedMenu(GetBaseBuildingMenu(), NULL);
            }

            //--- hologram rotation
            if (menu == NULL && playerPB.IsPlacingLocal() && playerPB.GetHologramLocal().GetParentEntity().PlacementCanBeRotated()) 
            {
                if (input.LocalPress("EXP_BaseBuilding_Vector_Back", false) || input.LocalHold("EXP_BaseBuilding_Vector_Back", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionYaw(3);
                    return;
                }
                
                if (input.LocalPress("EXP_BaseBuilding_Vector_Forward", false) || input.LocalHold("EXP_BaseBuilding_Vector_Forward", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionYaw(-3);
                    return;
                }

                if (input.LocalPress("EXP_BaseBuilding_Vector_Right", false) || input.LocalHold("EXP_BaseBuilding_Vector_Right", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionRoll(3);
                    return;
                }
                
                if (input.LocalPress("EXP_BaseBuilding_Vector_Left", false) || input.LocalHold("EXP_BaseBuilding_Vector_Left", false)) 
                {
                    playerPB.GetHologramLocal().ModifyProjectionRoll(-3);
                    return;
                }
            }
        }
    }

    override void OnMissionStart()
    {
		super.OnMissionStart();
		
        GetTerritoryManagerClient();
        GetBaseBuildingManagerClient();
	}
    
    override void OnMissionFinish()
	{
		super.OnMissionFinish();
        
        delete g_TerritoryManagerClient;
        g_TerritoryManagerClient = null;

        delete g_BaseBuildingManagerClient;
        g_BaseBuildingManagerClient = null;
	}

    override void OnKeyPress(int key)
	{
		super.OnKeyPress(key);
        bool isBuilding = PlayerBase.Cast(GetGame().GetPlayer()).IsPlacingLocal();

        if ((key == KeyCode.KC_B) && isBuilding) 
        {
            if (GetBaseBuildingManagerClient().GetBuildingMode() == BuildingMode.SNAP)
                GetBaseBuildingManagerClient().SetBuildingMode(BuildingMode.FREE);
            else
                GetBaseBuildingManagerClient().SetBuildingMode(BuildingMode.SNAP)
        }
    }
}