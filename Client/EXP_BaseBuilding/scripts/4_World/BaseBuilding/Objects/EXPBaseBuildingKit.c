/**
 * EXPBaseBuildingKit.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Wed Mar 17 2021
 * Modified By: SIX
 * 
 * 
 * Notes : Hologram is defined as KitClassname + "Placing"(NOT FINAL OBJECT CLASSNAME). 
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class EXPBaseBuildingKit extends ItemBase 
{
    string KitProduces() return "";

    override bool IsBasebuildingKit() return true;

	override bool IsDeployable() return true;

    override void OnPlacementComplete( Man player, vector position = "0 0 0", vector orientation = "0 0 0" )
	{
		if ( GetGame().IsServer() )
		{
			EXP_BaseBuildingObject BaseObj = EXP_BaseBuildingObject.Cast( GetGame().CreateObject( this.KitProduces(), position) );
			BaseObj.SetPosition( position );
			BaseObj.SetOrientation( orientation );
			BaseObj.SetAnimationPhase( "Hologram", 0 );
		}
	} 

    override void SetActions()
	{
		super.SetActions();
		AddAction(ActionTogglePlaceObject);
		AddAction(ActionPlaceObject);
	}
};