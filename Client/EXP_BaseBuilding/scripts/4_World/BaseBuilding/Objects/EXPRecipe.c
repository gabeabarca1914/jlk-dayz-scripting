/**
 * EXPRecipe.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class EXPRecipe
{
    /*
     * Name of the recipe
     */
    string Name;

    /*
     * Description of the recipe
     */
    string Description;

    /*
     * Class name of the item the recipe produces
     */
    string Produces;

    /*
     * Class name of the item the recipe displays in t's preview
     */
    string PreviewClassName;
    
    /*
     * Array of recipe items needed to make this item
     */
    ref array<ref EXPRecipeItem> RecipeItems = new array<ref EXPRecipeItem>;

    /*
     * Array of tools needed to make this item
     */
    ref array<ref EXPRecipeTool> RecipeTools = new array<ref EXPRecipeTool>;

    /*
     * EXPRecipe Constructor
     */
    void EXPRecipe(string name, string description, string produces, string previewClass) {
        Name = name;
        Description = description;
        Produces = produces;
        PreviewClassName = previewClass;
    }
}