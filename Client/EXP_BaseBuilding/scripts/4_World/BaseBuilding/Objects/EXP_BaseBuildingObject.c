/**
 * EXP_BaseBuildingObject.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Mon Apr 05 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class EXP_BaseBuildingObject extends House
{
    const float MAX_ACTION_DETECTION_ANGLE_RAD 		= 1.3;		//1.3 RAD = ~75 DEG
	const float MAX_ACTION_DETECTION_DISTANCE 		= 2.0;		//meters

	protected bool m_IsBaseObject = false;
	protected bool m_IsDoorOpened = false;
	protected bool m_IsLocked = false;
	protected bool m_HasCodelock = false;

	protected int m_OwnerIDOne = 0;
	protected int m_OwnerIDTwo = 0;
	protected int m_OwnerIDThree = 0;
	protected int m_OwnerIDFour = 0;
	protected int m_OwnerIDFive = 0;

	void EXP_BaseBuildingObject() 
	{
		RegisterNetSyncVariableBool("m_IsBaseObject");
		RegisterNetSyncVariableBool("m_IsDoorOpened");
		RegisterNetSyncVariableBool("m_IsLocked");
		RegisterNetSyncVariableBool("m_HasCodelock");
		
		RegisterNetSyncVariableInt("m_OwnerIDOne");
		RegisterNetSyncVariableInt("m_OwnerIDTwo");
		RegisterNetSyncVariableInt("m_OwnerIDThree");
		RegisterNetSyncVariableInt("m_OwnerIDFour");
		RegisterNetSyncVariableInt("m_OwnerIDFive");
	}
    
    string KitReturn() return "";
	bool IsDoor() return false;
	bool IsBaseObject() return true;
	bool IsDoorOpened() return false;
	bool IsLocked() return m_IsLocked;
	bool HasCodelock() return m_HasCodelock;

	void SetCodeLock(bool status) 
	{
		m_HasCodelock = status;
		SetSynchDirty();
	}
	void SetLocked(bool status) m_IsLocked = status;
	void ShowCodeLock(bool status) 
	{
		if (status)
			ShowSelection("hide_codelock");
		else
			HideSelection("hide_codelock");
	}

	void InsertUser(int id) 
	{
		//--- build array of slots
		array<int> PermissionSlot = {m_OwnerIDOne, m_OwnerIDTwo, m_OwnerIDThree, m_OwnerIDFour, m_OwnerIDFive};
		
		//--- loop through all slots
		foreach (int slot: PermissionSlot)
		{
			//--- make sure slot is not currently defined
			if (slot == 0)
			{
				//--- assign id
				slot = id;

				//--- exit method
				return;
			}
		}
	}

	void RemoveUser(int id) 
	{
		//--- build array of slots
		array<int> PermissionSlot = {m_OwnerIDOne, m_OwnerIDTwo, m_OwnerIDThree, m_OwnerIDFour, m_OwnerIDFive};
		
		//--- loop through all slots
		foreach (int slot: PermissionSlot)
		{
			//--- make sure slot is not currently defined
			if (slot == id)
			{
				//--- assign id default value
				slot = 0;

				//--- exit method
				return;
			}
		}
	}

	array<int> GetAllUsers() 
	{
		//--- build array of slots
		array<int> PermissionSlot = {m_OwnerIDOne, m_OwnerIDTwo, m_OwnerIDThree, m_OwnerIDFour, m_OwnerIDFive};
		//--- Build Return Array
		array<int> DefinedUsers = new array<int>;

		//--- loop through all slots
		foreach (int slot: PermissionSlot)
		{
			//--- make sure slot has a user defined to it
			if (slot != 0)
			{
				//--- Inert slot into return array
				DefinedUsers.Insert(slot);
			}
		}

		//--- Return the Array of slots
		return DefinedUsers;
	}

	override void OnStoreSave(ParamsWriteContext ctx)
    {
        super.OnStoreSave(ctx);
		
        ctx.Write(m_IsDoorOpened);
		ctx.Write(m_IsLocked);
		ctx.Write(m_HasCodelock);

		ctx.Write(m_OwnerIDOne);
		ctx.Write(m_OwnerIDTwo);
		ctx.Write(m_OwnerIDThree);
		ctx.Write(m_OwnerIDFour);
		ctx.Write(m_OwnerIDFive);
    }
    
    override bool OnStoreLoad(ParamsReadContext ctx, int version)
    {
		bool result = super.OnStoreLoad(ctx, version);

        ctx.Read(m_IsDoorOpened);
		ctx.Read(m_IsLocked);
		ctx.Read(m_HasCodelock);
		ctx.Read(m_OwnerIDOne);
		ctx.Read(m_OwnerIDTwo);
		ctx.Read(m_OwnerIDThree);
		ctx.Read(m_OwnerIDFour);
		ctx.Read(m_OwnerIDFive);

		if (m_IsLocked)
		{
			SetCodeLock(true);
			ShowCodeLock(true);
		}	

		return result;
    }

    bool IsFacingPlayer( PlayerBase player)
	{
		vector fence_pos = GetPosition();
		vector player_pos = player.GetPosition();
		vector ref_dir = GetDirection();
		
		//vector fence_player_dir = player_pos - fence_pos;
		vector fence_player_dir = player.GetDirection();
		fence_player_dir.Normalize();
		fence_player_dir[1] = 0; 	//ignore height
		
		ref_dir.Normalize();
		ref_dir[1] = 0;			//ignore height
		
		if ( ref_dir.Length() != 0 )
		{
			float angle = Math.Acos( fence_player_dir * ref_dir );
			
			if ( angle >= MAX_ACTION_DETECTION_ANGLE_RAD )
			{
				return true;
			}
		}
		
		return false;
	}

    bool IsFacingCamera()
	{
		vector ref_dir = GetDirection();
		vector cam_dir = GetGame().GetCurrentCameraDirection();
		
		//ref_dir = GetGame().GetCurrentCameraPosition() - GetPosition();
		ref_dir.Normalize();
		ref_dir[1] = 0;		//ignore height
		
		cam_dir.Normalize();
		cam_dir[1] = 0;		//ignore height
		
		if ( ref_dir.Length() != 0 )
		{
			float angle = Math.Acos( cam_dir * ref_dir );
			
			if ( angle >= MAX_ACTION_DETECTION_ANGLE_RAD )
			{
				return true;
			}
		}

		return false;
	}
};