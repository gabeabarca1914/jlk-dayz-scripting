/**
 * BaseBuildingManagerServer.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class BaseBuildingManagerServer
{
    ref BaseBuildingConfig Config;

    void BaseBuildingManagerServer() 
    {
        Config = BaseBuildingConfig.Read();

        GetRPCManager().AddRPC("EXPBaseBuilding", "GetConfigRequest", this);
        GetRPCManager().AddRPC("EXPBaseBuilding", "CraftRequest", this);
    }

    /*
     * Get config request
     */
    void GetConfigRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Print("BaseBuilding Debug - GetConfigRequest");
        Print("BaseBuilding Debug - GetConfigResponse - Sent");
        GetRPCManager().SendRPC("EXPBaseBuilding", "GetConfigResponse", new Param2<ref BaseBuildingConfig, array<ref EXPRecipe>>(Config, Config.Recipes), true, sender);
    }

    void CraftRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        Param2<ref EXPRecipe, Man> data;
        if (!ctx.Read(data)) return;

        if (type == CallType.Server)
        {
            if (CanCraft(data.param2, data.param1))
            {
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "EXP Base Building", "Your Item Has Been Crafted", "set:ccgui_enforce image:Icon40Move");

                ItemBase item = ItemBase.Cast(GetGame().CreateObject(data.param1.Produces, data.param2.GetPosition()));
                item.SetPosition(data.param2.GetPosition());
                
                if (item.HasQuantity())
                    item.SetQuantity(1);

                if (data.param1.RecipeTools)
                {
                    for (int a = 0; a < data.param1.RecipeTools.Count(); a++)
                    {
                        array<EntityAI> itemsArray = new array<EntityAI>;
                        data.param2.GetInventory().EnumerateInventory(InventoryTraversalType.PREORDER, itemsArray);
                        for (int j = 0; j < itemsArray.Count(); j++)
                        {
                            ItemBase inventoryItem = ItemBase.Cast(itemsArray[j]);
                            if (inventoryItem && inventoryItem.GetType() == data.param1.RecipeTools[a].ClassName)
                                inventoryItem.DecreaseHealth(20);
                        }
                    }
                }
            }
            else
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "EXP Base Building", "Cafting Stopped, you do not have the required Materials or Tools!", "set:ccgui_enforce image:Icon40Move");
        }
    }

    bool CanCraft(Man player, EXPRecipe recipe)
    {
        bool missingTools = false;
        bool missingParts = false;

        array< ref EXPListOfParts > partsToBeTaken = new array< ref EXPListOfParts >;
        array< ref EXPRequiredTools > requiredTools = new array< ref EXPRequiredTools >;
        array< ref EXPRequiredParts > requiredParts = new array< ref EXPRequiredParts >;

        foreach(EXPRecipeTool tool : recipe.RecipeTools) requiredTools.Insert(new EXPRequiredTools(tool.ClassName));

        foreach(EXPRequiredTools requiredTool : requiredTools)
        {
            //--- TODO: Check for tools on player
        }

        foreach(EXPRecipeItem part : recipe.RecipeItems) requiredParts.Insert(new EXPRequiredParts(part.ClassName, part.Quantity));

        foreach(EXPRequiredParts requiredPart : requiredParts)
            if (!HasParts(player, requiredPart.m_ClassName, requiredPart.Quantity)) missingParts = true;

        if (missingTools) return false;

        if  (!missingParts)  
            foreach(EXPRequiredParts takePart : requiredParts) partsToBeTaken.Insert(new EXPListOfParts(takePart.m_ClassName, takePart.Quantity));

        if (!TakeParts(player, partsToBeTaken)) missingParts = true;

        if (missingParts) return false;

        return true;
    }

    bool HasParts(Man player, string part, int count)
    {
        int hasPartsCount;
        bool hasParts;
        
        array<EntityAI> itemsArray = new array<EntityAI>;
        player.GetInventory().EnumerateInventory(InventoryTraversalType.PREORDER, itemsArray);

        foreach(ItemBase requiredItem : itemsArray)
        {
            if (requiredItem.GetType() == part)
            {
                if (requiredItem.GetQuantity() > 1) hasPartsCount += requiredItem.GetQuantity();
                else hasPartsCount += 1;
            }
        }

        if (hasPartsCount >= count) hasParts = true;
        if (hasPartsCount < count) return false;
        if (!hasParts) return false;
        return true;
    }

    bool TakeParts(Man player, array< ref EXPListOfParts > partsToBeTaken)
    {
        int totalCount;

        int takenPartsCount;
        bool hasTakenParts;

        array<EntityAI> partsArray = new array<EntityAI>;
        array<EntityAI> itemsArray = new array<EntityAI>;
        player.GetInventory().EnumerateInventory(InventoryTraversalType.PREORDER, itemsArray);

        foreach(EXPListOfParts takenParts : partsToBeTaken)
        {
            foreach(ItemBase requiredItem : itemsArray)
            {
                if (requiredItem.GetType() == takenParts.m_ClassName)
                {
                    if (requiredItem.HasQuantity())
                    {
                        if (requiredItem.GetQuantity() == takenParts.m_Quantity && !takenParts.m_IsTaken)
                        {
                            GetGame().ObjectDelete(requiredItem);
                            takenPartsCount += takenParts.m_Quantity;

                            takenParts.m_IsTaken = true;
                        }
                        else if (requiredItem.GetQuantity() > takenParts.m_Quantity && !takenParts.m_IsTaken)
                        {
                            requiredItem.SetQuantity(requiredItem.GetQuantity() - takenParts.m_Quantity);
                            takenPartsCount += takenParts.m_Quantity;

                            takenParts.m_IsTaken = true;
                        }
                        else if (requiredItem.GetQuantity() < takenParts.m_Quantity && !takenParts.m_IsTaken)
                        {
                            takenParts.m_QuantityTaken += requiredItem.GetQuantity();
                            if (takenParts.m_QuantityTaken >= takenParts.m_Quantity) takenParts.m_IsTaken = true;

                            if (takenParts.m_QuantityTaken > takenParts.m_Quantity) requiredItem.SetQuantity(takenParts.m_QuantityTaken - takenParts.m_Quantity);
                            else GetGame().ObjectDelete(requiredItem);
                            takenPartsCount += requiredItem.GetQuantity();                            
                        }
                    }
                    else 
                    {
                        GetGame().ObjectDelete(requiredItem);
                        takenPartsCount += 1;
                    }
                }
            }

            totalCount += takenParts.m_Quantity;
        }
        return true;
    }
}

ref BaseBuildingManagerServer g_BaseBuildingManagerServer;
BaseBuildingManagerServer GetBaseBuildingManagerServer()
{
    if (!g_BaseBuildingManagerServer)
        g_BaseBuildingManagerServer = new BaseBuildingManagerServer;
    return g_BaseBuildingManagerServer;
}