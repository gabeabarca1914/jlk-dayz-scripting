class TerritoryManagerClient 
{
    ref EXPTerritoryConfig Config;
    ref EXPTerritory Territory;
    string OwnUID;
    ref array<ref EXPTerritoryMember> AllPlayers = new array<ref EXPTerritoryMember >;
    ref array<ref EXPTerritoryInvite> Invites = new array<ref EXPTerritoryInvite >;
    ref array<ref EXPTerritory> Territories = new array<ref EXPTerritory>;

    bool IsInOwnTerritory;

    void TerritoryManagerClient() 
    {
        GetRPCManager().AddRPC("EXPTerritory", "GetConfigResponse", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetPlayerTerritoryResponse", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetOwnUIDResponse", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetAllPlayerListResponse", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetAllTerritoriesResponse", this);
        GetRPCManager().AddRPC("EXPTerritory", "InviteReceived", this);
        GetRPCManager().AddRPC("EXPTerritory", "PayTerritoryResponse", this);
        GetRPCManager().AddRPC("EXPTerritory", "UpgradeTerritoryResponse", this);
        
        GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.CheckIfInOwnTerritory, 1000, true);
        GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.OnInit, 2500, false);
    }

    void OnInit() 
    {
        Print("Territory Debug - GetConfigRequest - Sent");
        GetRPCManager().SendRPC("EXPTerritory", "GetConfigRequest", null, true);
        Print("Territory Debug - GetOwnUIDRequest - Sent");
        GetRPCManager().SendRPC("EXPTerritory", "GetOwnUIDRequest", null, true);
        Print("Territory Debug - GetTerritoryRequest - Sent");
        GetRPCManager().SendRPC("EXPTerritory", "GetTerritoryRequest", null, true);
    }

    void ~TerritoryManagerClient() GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(CheckIfInOwnTerritory);

    void GetConfigResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryConfig> data;
        if (!ctx.Read(data)) return;
        
        Config = data.param1;
        PrintFormat("Territory Debug - GetConfigResponse - Config:%1", Config);
    }

    void GetPlayerTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritory> data;
        if (!ctx.Read(data)) return;
        
        Territory = data.param1;
        PrintFormat("Territory Debug - GetPlayerTerritoryResponse - Territory:%1", Territory);
        UpdateMenu();

        //string.Format("Territory : %1 - %2", Territory.Name, Territory.Level.GetName())
        BasicMapCircleMarker Territory_Marker = new BasicMapCircleMarker("", Vector(Territory.pos_x, Territory.pos_y, Territory.pos_z), "BasicMap\\gui\\images\\flag.paa", {255, 0, 0});
        Territory_Marker.SetCanEdit(true);
        Territory_Marker.SetShowCenterMarker(true);
        Territory_Marker.SetHideIntersects(false);
        Territory_Marker.SetPosition(Vector(Territory.pos_x, Territory.pos_y, Territory.pos_z));
        Territory_Marker.SetRadius(Territory.Level.GetRadius());
        Territory_Marker.Name = string.Format("Territory : %1 - %2", Territory.Name, Territory.Level.GetName());
        BasicMap().AddMarker(BasicMap().TERRITORY_KEY, Territory_Marker);
    }

    void GetOwnUIDResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<string> data;
        if (!ctx.Read(data)) return;
        
        OwnUID = data.param1;
        PrintFormat("Territory Debug - GetOwnUIDResponse - OwnUID:%1", OwnUID);
    }

    void GetAllTerritoriesResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<array <ref EXPTerritory>> data;
        if (!ctx.Read(data)) return;
        
        Territories = data.param1;
    }

    void GetAllPlayerListResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<array <ref EXPTerritoryMember>> data;
        if (!ctx.Read(data)) return;
        
        AllPlayers = data.param1;
        UpdateMenu();
    }

    void InviteReceived(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryInvite> data;
        if (!ctx.Read(data)) return;
        
        Invites.Insert(data.param1);
        UpdateMenu();
    }

    void PayTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPPayTerritoryResponse> data;
        if (!ctx.Read(data)) return;

        if (data.param1.Response == "SUCCESS" && g_TerritoryPaymentMenu != null)
        {
            NotificationSystem.AddNotificationExtended(3, "Territory", "You have paid your territory!", "set:ccgui_enforce image:Icon40Move");
            GetTerritoryPaymentMenu().UpdateMenu();
        }
    }

    void UpgradeTerritoryResponse(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPPayTerritoryResponse> data;
        if (!ctx.Read(data)) return;

        if (data.param1.Response == "SUCCESS" && g_TerritoryPaymentMenu != null) 
        {
            NotificationSystem.AddNotificationExtended(3, "Territory", "You have upgraded your territory!", "set:ccgui_enforce image:Icon40Move");
            GetTerritoryPaymentMenu().UpdateMenu();
        }
        else
            NotificationSystem.AddNotificationExtended(3, "Territory", "Territory upgrading failed!", "set:ccgui_enforce image:Icon40Move");
    }

    bool IsTerritoryMember() return Territory != null;

    bool IsTerritoryOwner() 
    {
        if (IsTerritoryMember()) return Territory.IsOwner(OwnUID);
        return false;
    }

    bool IsTerritoryAdmin() 
    {
        if (IsTerritoryMember()) return Territory.IsAdmin(OwnUID);
        return false;
    }

    void UpdateMenu() 
    {
        if (g_TerritoryManagementMenu && g_TerritoryManagementMenu.isOpen) g_TerritoryManagementMenu.UpdateMenu();
    }

    EXPTerritoryInvite GetActiveInvite() 
    {
        if (Invites.Count()> 0) return Invites[0];
        return null;
    }

    void DeclineInvite() 
    {
        if (Invites.Count()> 0) Invites.RemoveOrdered(0);

        UpdateMenu();
    }

    void AcceptInvite() 
    {
        if (IsTerritoryMember()) NotificationSystem.AddNotificationExtended(3, "Territory", "Can't build here this area is a no build zone", "set:ccgui_enforce image:Icon40Move");

        EXPTerritoryInvite invite = GetActiveInvite();
        if (invite) 
        {
            GetRPCManager().SendRPC("EXPTerritory", "AcceptInviteRequest", new Param1<ref EXPTerritoryInvite> (invite), true, null);
            Invites.RemoveOrdered(0);
        }

        UpdateMenu();
    }

    EXPTerritory GetTerritoryAtPosition(vector pos, float radius = 100) 
    {
        foreach(EXPTerritory territory: Territories)
        {
            vector TerritoryPos = territory.GetPosition();
            float distance = vector.Distance(pos, TerritoryPos);
            if (distance < radius)
                return territory;
        }

        return null;
    }

    bool IsInOwnTerritory() 
    {   
        if (GetGame().GetPlayer() && IsTerritoryMember()) return (vector.Distance(GetGame().GetPlayer().GetPosition(), Territory.GetPosition()) <= Territory.Level.GetRadius());
        return false;
    }

    EXPTerritoryConfig GetTerritoryConfig() return Config;

    bool CanBuild(ItemBase item) 
    {
        TStringArray RaidItems = {};
        TStringArray TentItems = {};

        string itemName = item.GetType();
        PlayerBase player = GetGame().GetPlayer();

        if (itemName == "TerritoryFlagKit") 
        {
            if (IsInNoBuildZone()) 
            {
                NotificationSystem.AddNotificationExtended(3, "Territory", "Can't build here this area is a no build zone", "set:ccgui_enforce image:Icon40Move");
                if (player) 
                    player.TogglePlacingLocal();
                return false;
            }
            
            return CanPlacePlotPole();
        }

        if (IsTerritoryMember() && IsMaxObjectReached()) return false;

        if (IsTerritoryMember() && IsTerritoryDecaying()) return false;

        if (IsTerritoryMember() && IsInOwnTerritory()) return true;

        if (IsInEnemyTerritory()) 
            if (GetTerritoryConfig().ItemsAllowedInEnemyTerritory.Find(itemName) != -1) 
                return true;
        else if (GetTerritoryConfig().NeutralItems.Find(itemName) != -1) 
            return true;

        return false;
    }

    bool CanDismantle(Object targetObject) 
    {
        float snapping_9;
        float snapping_10;
        array<string> poopie = {"T1_Gate", "T2_Gate", "T3_Gate"};

        if (targetObject)
        {
            if (poopie.Find(targetObject.GetType()) != -1)
            {
                snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), GetGame().GetPlayer().GetPosition());
                snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), GetGame().GetPlayer().GetPosition());
                if (snapping_9 < snapping_10) return true;
            }

            if (Config.NonDismantableItems.Find(targetObject.GetType()) == -1 && poopie.Find(targetObject.GetType()) == -1)
            {
                snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), GetGame().GetPlayer().GetPosition());
                snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), GetGame().GetPlayer().GetPosition());
                if (snapping_9 > snapping_10) return true;
            }
        }

        return false;
    }

    bool IsInNoBuildZone() 
    {
        for (int i = 0; i<Config.NobuildZones.Count(); i++) 
            if (Config.NobuildZones[i].PositionIsViolating(GetGame().GetPlayer().GetPosition(), 15)) return true;

        return false;
    }

    bool CanPlacePlotPole() 
    {
        if (IsInEnemyTerritory() || IsTerritoryMember()) return false;

        return !ItemIsNearby("TerritoryFlag", GetGame().GetPlayer().GetPosition(), 80 + 100);
    }

    bool IsInEnemyTerritory() 
    {
        if (IsInOwnTerritory()) return false;

        return ItemIsNearby("TerritoryFlag", GetGame().GetPlayer().GetPosition(), 80);
    }

    void CheckIfInOwnTerritory() 
    {
        if (!IsTerritoryMember()) IsInOwnTerritory = false;

        IsInOwnTerritory = IsInOwnTerritory();
    }

    bool IsMaxObjectReached() 
    {
        int TerritoryMaxObjects = Territory.GetLevel().GetMaxObjectCount();
        map<string, int> partCountMap = Territory.GetPartCountMap();
        int itemCount = 0;

        for (int i = 0; i < partCountMap.Count(); i++)
        {
            string currentIndexKey = partCountMap.GetKey(i);
            int currentIndexValue = partCountMap.GetElement(i);
            itemCount += currentIndexValue;
        }

        return itemCount >= TerritoryMaxObjects;
    }

    bool IsTerritoryDecaying() 
    {
        Object FlagObj = GetNearFlagObject(Territory.GetPosition(), Territory.Level.GetRadius());
        TerritoryFlag FlagTerritory;

        if (Class.CastTo(FlagTerritory, FlagObj)) return FlagTerritory.GetIsDecaying();

        return false;
    }

    bool GetIsInOwnTerritory() return IsInOwnTerritory;
}

ref TerritoryManagerClient g_TerritoryManagerClient;
TerritoryManagerClient GetTerritoryManagerClient() 
{
    if (!g_TerritoryManagerClient)
        g_TerritoryManagerClient = new TerritoryManagerClient;
    return g_TerritoryManagerClient;
}