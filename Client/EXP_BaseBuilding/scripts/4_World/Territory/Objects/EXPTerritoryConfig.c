/**
 * EXPTerritoryConfig.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Apr 02 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class EXPTerritoryConfig
{
    int PlotPoleLifeTime = 14;
    int BaseStatusCheckFrequency = 5;
    float MaintenanceCalculationTimer = 28.8;
    float BasePaymentTimer = 120;   
    int MinimumTerritoryNameLength = 4;
    float decayRatePercent = 0.01;
    int ShowTerritoryMembersInAllPlayersList = 1;
    int AutoBuildTerritoryFlag = 1;

    ref TStringArray ItemsAllowedInEnemyTerritory = new TStringArray;
    ref TStringArray NonDismantableItems = new TStringArray;
    ref TStringArray NeutralItems = new TStringArray;
    ref array<ref EXPNoBuildZone> NobuildZones = new array<ref EXPNoBuildZone>;
    ref array<ref EXPTerritoryLevel> TerritoryLevels = new array<ref EXPTerritoryLevel>;
    ref array<ref EXPBaseItemPrice> BaseItemPrices = new array<ref EXPBaseItemPrice>;
    ref array<ref EXPTerritoryProtectionLevel> TerritoryProtectionLevels = new array<ref EXPTerritoryProtectionLevel>;

    static EXPTerritoryConfig Read()
    {
        string fileName;
        FileAttr fileAttributes;
        FindFileHandle file = FindFile(TERRITORY_CONFIG_FILE, fileName, fileAttributes, 0);

        if (!file) 
            return WriteDefault();

        EXPTerritoryConfig config = new EXPTerritoryConfig;
        JsonFileLoader<EXPTerritoryConfig>.JsonLoadFile(TERRITORY_CONFIG_FILE, config);

        return config;
    }

   
    static EXPTerritoryConfig WriteDefault()
    {
        if (!FileExist(TERRITORY_DIR)) MakeDirectory(TERRITORY_DIR);

        EXPTerritoryConfig config = new EXPTerritoryConfig;

        /*config.NobuildZones.Insert(new EXPNoBuildZone("Default", 0, 0, 0, 10));
        config.TerritoryLevels.Insert(new EXPTerritoryLevel("Level 1", 5000, 2500, 10, 30));
        config.BaseItemPrices.Insert(new EXPBaseItemPrice("EXP_T1_Wall", 100));
        config.BaseItemPrices.Insert(new EXPBaseItemPrice("EXP_T2_Wall", 300));*/

        JsonFileLoader<EXPTerritoryConfig>.JsonSaveFile(TERRITORY_CONFIG_FILE, config);

        return config;
    }
}