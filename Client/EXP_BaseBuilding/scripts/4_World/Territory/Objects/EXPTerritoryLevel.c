class EXPTerritoryLevel
{
    protected string Name;
    protected int Price;
    protected int RentalPrice;
    protected float Radius;
    protected int MaxObjectCount;
   
    void EXPTerritoryLevel(string name, int price, int rent, float radius, int maxObjectCount) 
    {
        Name = name;
        Price = price;
        RentalPrice = rent;
        Radius = radius;
        MaxObjectCount = maxObjectCount;
    }

    string GetName() return Name;
    int GetPrice() return Price;
    int GetRentPrice() return RentalPrice;
    float GetRadius() return Radius;
    int GetMaxObjectCount() return MaxObjectCount;
}