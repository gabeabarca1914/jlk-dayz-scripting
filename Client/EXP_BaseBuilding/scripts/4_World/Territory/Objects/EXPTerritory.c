/**
 * EXPTerritory.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class EXPTerritory
{
    string Name;
    string GUID;
    string OwnerUID;
    string CreatedAt; 
    string LastPaid;
    int TerritoryProtectionLevel = 0; //--- 0 Is none

    float CurrentMaintenancePrice = 0;
    int MinutesSincePaid = 0;
    float pos_x;
    float pos_y;
    float pos_z;
    
    ref TStringArray AdminUIDS = new TStringArray;
    ref array<ref EXPTerritoryMember> Members = new array<ref EXPTerritoryMember>;
    ref EXPTerritoryLevel Level;
    ref EXPTerritoryDecayData DecayData;

    bool TerritoryIsDecayingPayment;   
    bool TerritoryIsDecayingFlag; 

    void EXPTerritory(string guid, string ownerUID, vector position) 
    {        
        GUID = guid;
        OwnerUID = ownerUID;

        pos_x =  position[0];
        pos_y =  position[1];
        pos_z =  position[2];

        CreatedAt = GenerateDateTimeStamp();
        LastPaid = GenerateDateTimeStamp();

        Level = GetTerritoryManagerServer().Config.TerritoryLevels[0];

        TerritoryIsDecayingPayment = false;

        int ThreadUpdateTimeDecay = Math.Floor(60000 * GetTerritoryManagerServer().Config.MaintenanceCalculationTimer);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.AttemptDecay, ThreadUpdateTimeDecay, true);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.AttemptDecay, 10 * 1000, false);

        int ThreadUpdateTimePayment = Math.Floor(60000 * GetTerritoryManagerServer().Config.BasePaymentTimer);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.CalculatePaymentPrice, ThreadUpdateTimePayment, true);

        int ThreadCheckTerritoryStatus = Math.Floor(60000 * GetTerritoryManagerServer().Config.BaseStatusCheckFrequency);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).CallLater(this.CheckTerritoryVariables, ThreadCheckTerritoryStatus, true);
    }

    void ~EXPTerritory() 
    {
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(CalculatePaymentPrice);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(CheckTerritoryVariables);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(AttemptDecay);
    }

    void SetName(string name)   Name = name;
    string GetName()            return Name;
    string GetGUID()            return GUID;
    bool GetDecayStatus()       return TerritoryIsDecayingPayment;
    int GetTerritoryProtectionLevel() return TerritoryProtectionLevel;

    int GetMemberIndex(string uid)
    {
        for (int i = 0; i < Members.Count(); i++)
            if (Members[i].UID == uid) return i;
        return -1;
    }

    bool IsMember(string uid) return GetMemberIndex(uid) != -1;
   
    EXPTerritoryMember GetMember(string uid)
    {
        int index = GetMemberIndex(uid);

        if (index != -1) return Members[index];
        
        return null;
    }

   
    void AddMember(string name, string uid)
    {
        if (!IsMember(uid)) Members.Insert(new EXPTerritoryMember(name, uid));
    }

   
    void RemoveMember(string uid)
    {
        if (IsMember(uid))
        {
            int index = GetMemberIndex(uid);
            Members.RemoveOrdered(index);
        }
    }

   
    bool IsAdmin(string uid)
    {
        if (IsOwner(uid)) return true;
        
        return AdminUIDS.Find(uid) != -1;
    }

   
    void AddAdmin(string uid)
    {
        if ((IsMember(uid) && !IsAdmin(uid)) || AdminUIDS.Count() == 0) AdminUIDS.Insert(uid);
    }

   
    void RemoveAdmin(string uid)
    {
        int index = AdminUIDS.Find(uid);
        if (index != -1) AdminUIDS.RemoveOrdered(index);
    }

    void SetOwner(string ownerUID) OwnerUID = ownerUID;   
    bool IsOwner(string uid) return OwnerUID == uid;
    EXPTerritoryLevel GetLevel() return Level;
    static string GenerateStorageFilePath(string guid = "") return TERRITORY_STORAGE_DIR + guid + ".json";

    void UpdateMemberName(string name, string uid)
    {
        int index = GetMemberIndex(uid);
        
        if (index != -1)
        {
            EXPTerritoryMember member = Members[index];
            
            if (member && member.Name != name)
            {
                member.Name = name;
                Write();
            }
        }
    }

   
    void Write()
    {
        if (!FileExist(TERRITORY_DIR)) MakeDirectory(TERRITORY_DIR);
        if (!FileExist(TERRITORY_STORAGE_DIR)) MakeDirectory(TERRITORY_STORAGE_DIR);
        JsonFileLoader<EXPTerritory>.JsonSaveFile(GenerateStorageFilePath(this.GUID), this);
    }
   
    static EXPTerritory Read(string guid)
    {
        string filePath = GenerateStorageFilePath(guid);
        string fileName;
        FileAttr fileAttributes;
        FindFileHandle file = FindFile(filePath, fileName, fileAttributes, 0);

        if (file)
        {
            ref EXPTerritory territory = new EXPTerritory(guid, "", vector.Zero);
            JsonFileLoader<EXPTerritory>.JsonLoadFile(filePath, territory);

            return territory;
        }

        return null;
    }

    void CheckTerritoryVariables() 
    {
        int Days = (1440 * GetTerritoryManagerServer().Config.PlotPoleLifeTime);
        if (MinutesSincePaid < Days && (MinutesSincePaid == (Days - 1))) WarnMembers();
        TerritoryFlag TerryCruzObject = TerritoryFlag.Cast(GetTerritoryFlagObject());
        
        EntityAI IsFlagAttached;
        if (TerryCruzObject)
            IsFlagAttached = TerryCruzObject.FindAttachmentBySlotName("Material_FPole_Flag");
        
        if (TerryCruzObject && !IsFlagAttached)
        {
            if (!TerritoryIsDecayingFlag)
            {
                TerritoryIsDecayingFlag = !TerritoryIsDecayingFlag;
                TerryCruzObject.SetIsDecaying(true);
                Write();
            }
            
            if (DecayData.oriBaseItemCount == 0)
            {    
                DecayData.oriBaseItemCount = GetPartArray().Count();
                Write();
            }
        }
 
        if (MinutesSincePaid > Days) 
        {
            if (TerryCruzObject && !TerritoryIsDecayingPayment)
            {
                TerritoryIsDecayingPayment = !TerritoryIsDecayingPayment;
                TerryCruzObject.SetIsDecaying(TerritoryIsDecayingPayment);
                Write();
            }
            
            if (DecayData.oriBaseItemCount == 0)
            {    
                DecayData.oriBaseItemCount = GetPartArray().Count();
                Write();
            }
        }
        
        if (TerritoryIsDecayingFlag && IsFlagAttached)
            TerritoryIsDecayingFlag = !TerritoryIsDecayingFlag;
        
        if (TerritoryIsDecayingPayment && MinutesSincePaid < Days) 
            TerritoryIsDecayingPayment = !TerritoryIsDecayingPayment;
        
        if (!TerritoryIsDecayingPayment && !TerritoryIsDecayingFlag)
        {
            if (TerryCruzObject && !TerryCruzObject.GetIsDecaying() == false)
                TerryCruzObject.SetIsDecaying(false);
            
            if (!DecayData.oriBaseItemCount == 0 && !DecayData.nextDecay == 0)
            {
                DecayData.oriBaseItemCount = 0;
                DecayData.nextDecay = 0;
                Write();
            }
        }
    }

    void CalculatePaymentPrice() 
    {
        array<Object> parts = GetPartArray();

        float increase = 0;

        foreach(Object BasePart: parts)
            increase += GetItemPrice(BasePart.GetType()) * GetTerritoryManagerServer().Config.MaintenanceCalculationTimer;

        increase += GetLevel().GetPrice();
        increase += (Members.Count() * 400);
        
        CurrentMaintenancePrice += increase;
        MinutesSincePaid += GetTerritoryManagerServer().Config.MaintenanceCalculationTimer;

        Write();
        UpdateAllMembers();
    }

    void AttemptDecay() 
    {        
        if (TerritoryIsDecayingPayment || TerritoryIsDecayingFlag) Decay();
    }

    void Decay() 
    {
        DecayData.nextDecay += (DecayData.oriBaseItemCount * GetTerritoryManagerServer().Config.decayRatePercent);

        array<Object> parts = GetPartArray(true);
        
        while (DecayData.nextDecay >= 1)
        {
            if (parts.Count() > 0)
            {
                DecayData.nextDecay--;
                GetGame().ObjectDelete(parts.GetRandomElement());
                Write();
            }
            else
            {
                DecayData.nextDecay = 0;
                Delete();
            }
        }
    }

    int GetItemPrice(string classname) 
    {
        array<ref EXPBaseItemPrice> PricesArray = GetTerritoryManagerServer().Config.BaseItemPrices;
        foreach(EXPBaseItemPrice PriceData: PricesArray)
            if (PriceData.GetClassname() == classname) return PriceData.GetObjectPrice();
        return 0;
    }

   
    void Paid() 
    {
        Print("Paid Rent");
        LastPaid = GenerateDateTimeStamp();
        CurrentMaintenancePrice = 0;
        MinutesSincePaid = 0;
        array<Object> parts = GetPartArray();

        foreach (Object obj: parts)
        {
            EntityAI entity = EntityAI.Cast(obj);
            entity.IncreaseLifetime();
        }

        Write();
        UpdateAllMembers();
    }

    void Upgrade(EXPTerritoryLevel IncomingLevel) 
    {
        Level = IncomingLevel;
        Write();
        UpdateAllMembers();
    }

    int CalculateLifetimeMinutesRemaining()
    {
        EXPTerritoryConfig config;
        if (GetGame().IsServer())
            config = GetTerritoryManagerServer().Config;
        else
            config = GetTerritoryManagerClient().Config;

        int totalLifeTimeMinutes = 1440 * config.PlotPoleLifeTime;

        return totalLifeTimeMinutes - MinutesSincePaid;
    }

   
    string GetRemainingLifeTimeDisplay()
    {
        int totalLifeTimeMinutes = CalculateLifetimeMinutesRemaining();

        int Days =  totalLifeTimeMinutes / (24 * 60);
        int Hours = (totalLifeTimeMinutes % (24 * 60)) / 60;
        int Minutes = (totalLifeTimeMinutes % (24 * 60)) % 60;

        return "" + Days + " Days " + Hours + " Hours And " + Minutes + "  Minutes";
    }

   
    void Delete() 
    {
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(CalculatePaymentPrice);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(CheckTerritoryVariables);
        GetGame().GetCallQueue(CALL_CATEGORY_SYSTEM).Remove(AttemptDecay);
        
        //--- TODO: Notify server of base being deleted

        array<Object> parts = GetPartArray(true);

        foreach(Object BaseObject: parts)
            GetGame().ObjectDelete(BaseObject);

        parts = GetPartArray();

        foreach(Object Storage: parts)
        {
            if (Storage)
            {
                vector storagePos = Storage.GetPosition();
                storagePos[1] = GetGame().SurfaceY(storagePos[0], storagePos[2]);
                Storage.SetPosition(storagePos);
            }
        }

        RemoveAllMembers();

        foreach (EXPTerritoryMember TerritoryMember: Members)
        {
            string MemberUID = TerritoryMember.UID;
            
            RemoveMember(MemberUID);
            RemoveAdmin(MemberUID);
            Write();            
            UpdateAllMembers();
        }

        DeleteFile(GenerateStorageFilePath(this.GUID));
    }

    vector GetPosition() return Vector(pos_x, pos_y, pos_z);

    map<string, int> GetPartCountMap() 
    {
        map<string, int> partCountMap = new map<string, int>;

        array<Object> parts = GetPartArray();

        foreach(Object Obj: parts)
        {
            if (partCountMap.Contains(Obj.GetType()))
            {
                int currentCount;
                partCountMap.Find(Obj.GetType(), currentCount);
                partCountMap.Set(Obj.GetType(), currentCount + 1);
            }
            else
                partCountMap.Insert(Obj.GetType(), 1);
        }

        return partCountMap;
    }
   
    array<Object> GetPartArray(bool ignoreStorage = false) 
    {
        array<Object> nearest_objects = new array<Object>;
        array<CargoBase> proxy_cargos = new array<CargoBase>;
        GetGame().GetObjectsAtPosition3D(GetPosition(), Level.GetRadius(), nearest_objects, proxy_cargos);
        
        array<Object> parts = new array<Object>;

        foreach(Object Obj: nearest_objects)
        {
            if (Obj.IsInherited(TerritoryFlag)) continue;
            if (!ignoreStorage && (Obj.IsInherited(LargeTent) || Obj.IsInherited(DeployableContainer_Base))) parts.Insert(Obj);
            if (Obj.IsInherited(EXP_BaseBuildingObject) || Obj.IsInherited(BaseBuildingBase)) parts.Insert(Obj);
        }
        return parts;
    }

    Object GetTerritoryFlagObject() 
    {
        array<Object> nearest_objects = new array<Object>;
        array<CargoBase> proxy_cargos = new array<CargoBase>;
        GetGame().GetObjectsAtPosition3D(GetPosition(), Level.GetRadius(), nearest_objects, proxy_cargos);
    
        foreach(Object Obj: nearest_objects)
            if (Obj.IsInherited(TerritoryFlag)) return Obj;
        return null; 
    }

    //--- TODO
    void WarnMembers();

    void UpdateAllMembers()
    {
        foreach(EXPTerritoryMember TerrMember: Members)
        {
            PlayerIdentity identity = GetPlayerIdentityByUID(TerrMember.UID);

            if (identity) 
                GetRPCManager().SendRPC("EXPTerritory", "GetPlayerTerritoryResponse", new Param1<ref EXPTerritory>(this), true, identity);
        }
    }

    void RemoveAllMembers() 
    {
        foreach(EXPTerritoryMember TerrMember: Members) 
        {
            PlayerIdentity identity = GetPlayerIdentityByUID(TerrMember.UID);

            if (identity) GetRPCManager().SendRPC("EXPTerritory", "GetPlayerTerritoryResponse", new Param1<ref EXPTerritory>(null), true, identity);
        }
    }
}