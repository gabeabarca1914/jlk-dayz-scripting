/**
 * TerritoryManagerServer.c
 *
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 *
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 *
 *
 * Notes : N/A
 *
 *
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX).
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class TerritoryManagerServer
{
    ref EXPTerritoryConfig Config;
    ref array<ref EXPTerritory> Territories = new array<ref EXPTerritory>;

    void TerritoryManagerServer()
    {
        Config = EXPTerritoryConfig.Read();

        GetRPCManager().AddRPC("EXPTerritory", "GetConfigRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetOwnUIDRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetTerritoryRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "GetAllPlayerListRequest", this);


        GetRPCManager().AddRPC("EXPTerritory", "NameTerritoryRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "AcceptInviteRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "InvitePlayerRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "KickPlayerRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "DemotePlayerRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "PromotePlayerRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "LeaveTerritoryRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "PayTerritoryRequest", this);
        GetRPCManager().AddRPC("EXPTerritory", "UpgradeTerritoryRequest", this);
    }


    void GetConfigRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        PrintFormat("Territory Debug - GetConfigRequest");
        PrintFormat("Territory Debug - GetConfigResponse - Sent : Config:%1", Config);
        GetRPCManager().SendRPC("EXPTerritory", "GetConfigResponse", new Param1<ref EXPTerritoryConfig>(Config), true, sender);
    }


    void GetOwnUIDRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        PrintFormat("Territory Debug - GetOwnUIDRequest");
        PrintFormat("Territory Debug - GetOwnUIDResponse - Sent : ID:%1", sender.GetPlainId());
        GetRPCManager().SendRPC("EXPTerritory", "GetOwnUIDResponse", new Param1<string>(sender.GetPlainId()), true, sender);
    }


    void GetTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        PrintFormat("Territory Debug - GetTerritoryRequest");
        string senderUID = sender.GetPlainId();
        string senderName = sender.GetName();

        EXPTerritory territory = GetPlayerTerritoryByUID(senderUID);

        if (territory) 
        {
            territory.UpdateMemberName(senderName, senderUID);
            PrintFormat("Territory Debug - GetPlayerTerritoryResponse - Sent : territory:%1", territory);
            GetRPCManager().SendRPC("EXPTerritory", "GetPlayerTerritoryResponse", new Param1<ref EXPTerritory>(territory), true, sender);
        }

        GetRPCManager().SendRPC("EXPTerritory", "GetAllTerritoriesResponse", new Param1<array<ref EXPTerritory>>(Territories), true, sender);
    }


    void NameTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param3<string, TerritoryFlag, PlayerBase> data;
        if (!ctx.Read(data)) return;

        string senderUID = sender.GetPlainId();
        
        EXPTerritory territory = RegisterTerritory(data.param2, data.param3);

        if (territory) 
        {
            if (territory.IsAdmin(senderUID)) 
            {
                territory.SetName(data.param1);
                territory.Write();
                territory.UpdateAllMembers();
            }
        }
        else
        {
            PrintFormat("Territory Server - NameTerritoryRequest Failed - Could Not Locate Territory - senderUID: %1 - nameRequest: %2", senderUID, data.param1);
        }
    }


    void GetAllPlayerListRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        array<Man> players = new array<Man>;
        GetGame().GetPlayers(players);
        array<ref EXPTerritoryMember> allPlayers = new array<ref EXPTerritoryMember>;
        for (int i = 0; i < players.Count(); i++)
        {
            PlayerBase player = PlayerBase.Cast(players[i]);
            if (player)
            {
                string name = player.GetIdentity().GetName();
                string uid = player.GetIdentity().GetPlainId();

                allPlayers.Insert(new EXPTerritoryMember(name, uid));
            }
            GetRPCManager().SendRPC("EXPTerritory", "GetAllPlayerListResponse", new Param1<array<ref EXPTerritoryMember>>(allPlayers), true, sender);
        }
    }


    void AcceptInviteRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryInvite> data;
        if (!ctx.Read(data)) return;
        EXPTerritoryInvite invite = data.param1;

        string invitingPlayerUID = invite.InviterUID;
        string acceptingPlayerUID = sender.GetPlainId();
        string acceptingPlayerName = sender.GetName();

        EXPTerritory territory = GetTerritoryByGUID(invite.GUID);

        if (!territory) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Territory no longer exists", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsAdmin(invitingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "This invite is no longer valid", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (PlayerHasTerritory(acceptingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are already a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.AddMember(acceptingPlayerName, acceptingPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
    }


    void InvitePlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryMember> data;
        if (!ctx.Read(data)) return;

        EXPTerritoryMember player = data.param1;

        string invitedPlayerUID = player.UID;
        PlayerIdentity invitedPlayerIdentity = GetPlayerIdentityByUID(invitedPlayerUID);
        string invitingPlayerUID = sender.GetPlainId();
        string invitingPlayerName = sender.GetName();

        if (!PlayerHasTerritory(invitingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        EXPTerritory territory = GetPlayerTerritoryByUID(invitingPlayerUID);

        if (!territory) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Failed to send invite, Can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsAdmin(invitingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only a territory admin can invite new members", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        EXPTerritoryInvite invite = new EXPTerritoryInvite(territory.GetGUID(), territory.GetName(), invitingPlayerUID, invitingPlayerName);
        GetRPCManager().SendRPC("EXPTerritory", "InviteReceived", new Param1<ref EXPTerritoryInvite>(invite), true, invitedPlayerIdentity);
        NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Invite sent", "set:ccgui_enforce image:Icon40Move");
    }


    void KickPlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryMember> data;
        if (!ctx.Read(data)) return;

        EXPTerritoryMember player = data.param1;

        string kickingPlayerUID = sender.GetPlainId();
        string kickedPlayerUID = player.UID;
        PlayerIdentity kickedPlayerIdentity = GetPlayerIdentityByUID(kickedPlayerUID);

        EXPTerritory territory = GetPlayerTerritoryByUID(kickingPlayerUID);

        if (kickingPlayerUID == kickedPlayerUID) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't kick yourself, Leave instead", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsAdmin(kickingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only a territory admin or owner can kick other members", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (territory.IsAdmin(kickedPlayerUID) && !territory.IsOwner(kickingPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only the territory owner can kick another admin", "set:ccgui_enforce image:Icon40Move");
            return;
        }


        if (territory.IsOwner(kickedPlayerUID)) 
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't kick the territory owner", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.RemoveMember(kickedPlayerUID);
        territory.RemoveAdmin(kickedPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
        GetRPCManager().SendRPC("EXPTerritory", "GetPlayerTerritoryResponse", new Param1<ref EXPTerritory>(null), true, kickedPlayerIdentity);
    }


    void DemotePlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryMember> data;
        if (!ctx.Read(data)) return;

        EXPTerritoryMember player = data.param1;

        string sendingPlayerUID = sender.GetPlainId();

        string targetPlayerUID = player.UID;

        if (sendingPlayerUID == targetPlayerUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't demote yourself", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(targetPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The selected player is not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        string sendingPlayerTerritoryGUID = GetPlayerTerritoryGUID(sendingPlayerUID);
        string targetPlayerTerritoryGUID= GetPlayerTerritoryGUID(targetPlayerUID);

        if (sendingPlayerTerritoryGUID != targetPlayerTerritoryGUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not in the same territory as the selected member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        EXPTerritory territory = GetPlayerTerritoryByUID(sendingPlayerUID);

        if (!territory)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsOwner(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only the territory owner can demote other memebers", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.RemoveAdmin(targetPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
        NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You have demoted the selected member", "set:ccgui_enforce image:Icon40Move");
    }


    void PromotePlayerRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        Param1<ref EXPTerritoryMember> data;
        if (!ctx.Read(data)) return;

        EXPTerritoryMember player = data.param1;
        string sendingPlayerUID = sender.GetPlainId();
        string targetPlayerUID = player.UID;

        if (sendingPlayerUID == targetPlayerUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't promote yourself", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!PlayerHasTerritory(targetPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The target player is not a territory member", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        string sendingPlayerTerritoryGUID = GetPlayerTerritoryGUID(sendingPlayerUID);
        string targetPlayerTerritoryGUID= GetPlayerTerritoryGUID(targetPlayerUID);

        if (sendingPlayerTerritoryGUID != targetPlayerTerritoryGUID)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You are not in the same territory as the target member", "set:ccgui_enforce image:Icon40Move");
            Print("You are not in the same territory as the target member");
            return;
        }

        EXPTerritory territory = GetPlayerTerritoryByUID(sendingPlayerUID);

        if (!territory)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server can't find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        if (!territory.IsOwner(sendingPlayerUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "Only the territory owner can promote other memebers", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.AddAdmin(targetPlayerUID);
        territory.Write();
        territory.UpdateAllMembers();
        NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You have promoted the selected member", "set:ccgui_enforce image:Icon40Move");
    }

    void LeaveTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target) 
    {
        string senderUID = sender.GetPlainId();

        if (!PlayerHasTerritory(senderUID))
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "You can't leave a territory you are not in", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        EXPTerritory territory = GetPlayerTerritoryByUID(senderUID);

        if (!territory)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory Manager", "The server could not find your territory", "set:ccgui_enforce image:Icon40Move");
            return;
        }

        territory.RemoveMember(senderUID);
        territory.RemoveAdmin(senderUID);

        if (territory.IsOwner(senderUID))
        {
            if (territory.AdminUIDS.Count() > 0) 
                territory.SetOwner(territory.AdminUIDS[0]);
            else 
            {
                if (territory.Members.Count() > 0) 
                    territory.SetOwner(territory.Members[0].UID);
                else territory.SetOwner("");
            }
        }

        territory.Write();
        territory.UpdateAllMembers();
        GetRPCManager().SendRPC("EXPTerritory", "GetPlayerTerritoryResponse", new Param1<ref EXPTerritory>(null), true, sender);
    }


    EXPTerritory RegisterTerritory(TerritoryFlag flag, PlayerBase player)
    {
        if (flag.GUID == "") flag.GUID = GenerateGUID();
        string ownerUID =  player.GetIdentity().GetPlainId();
        string ownerName =  player.GetIdentity().GetName();
        EXPTerritory territory = new EXPTerritory(flag.GUID, ownerUID, flag.GetPosition());
        EXPTerritoryDecayData DefaultDecayData = new EXPTerritoryDecayData;

        territory.AddMember(ownerName, ownerUID);
        territory.SetName(ownerName + "'s Territory");
        
        territory.Write();
        territory.UpdateAllMembers();
        
        territory.DecayData = DefaultDecayData;
        Territories.Insert(territory);

        return territory;
    }

    bool CanDismantle(PlayerBase player, Object targetObject) 
    {
        float snapping_9;
        float snapping_10;

        array<string> poopie = {"T1_Gate", "T2_Gate", "T3_Gate"};
        if (targetObject && player)
        {   
            if (poopie.Find(targetObject.GetType()) != -1)
            {
                snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), GetGame().GetPlayer().GetPosition());
                snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), GetGame().GetPlayer().GetPosition());
                if (snapping_9 > snapping_10) return true;
            }
            
            snapping_9 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_9")), player.GetPosition());
            snapping_10 = vector.Distance(targetObject.ModelToWorld(targetObject.GetMemoryPointPos("snapping_10")), player.GetPosition());
            if (snapping_9 < snapping_10) return true;
        }
        return false;
    }

    void PayTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        string senderUID = sender.GetPlainId();
        PlayerBase player = GetPlayerBaseByUID(senderUID);

        if (PlayerHasTerritory(senderUID))
        {
            EXPTerritory territory = GetPlayerTerritoryByUID(senderUID);
            EXPPayTerritoryResponse ResponseData = new EXPPayTerritoryResponse;
            int territoryPrice = territory.CurrentMaintenancePrice;

            bool MoneyResult = RemoveCurrency(player, territoryPrice);

            if (MoneyResult)
            {
                territory.Paid();
                ResponseData.Response = "SUCCESS";
                GetRPCManager().SendRPC("EXPTerritory", "PayTerritoryResponse", new Param1<ref EXPPayTerritoryResponse>(ResponseData), true, sender);
            }
            else
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You dont have enough money you cheap fuck", "set:ccgui_enforce image:Icon40Move");
        }
        else
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You don't have a territory you fucking mong", "set:ccgui_enforce image:Icon40Move");
    }

    void UpgradeTerritoryRequest(CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target)
    {
        string senderUID = sender.GetPlainId();
        PlayerBase player = GetPlayerBaseByUID(senderUID);
        if (PlayerHasTerritory(senderUID))
        {
            //--- Current Territory Level
            EXPTerritory territory = GetPlayerTerritoryByUID(senderUID);
            EXPTerritoryLevel territoryLevel = territory.GetLevel();
            
            //--- Next Territory Info
            int NextTerritoryLvlIndex = Config.TerritoryLevels.Find(territory.GetLevel());
            bool aidsEnfusionWorkAround = NextTerritoryLvlIndex == 0;
            
            //--- Cannot increment on 0
            EXPTerritoryLevel NextTerritoryLvl;
            if (aidsEnfusionWorkAround)
                NextTerritoryLvl = Config.TerritoryLevels[1];
            else
                NextTerritoryLvl = Config.TerritoryLevels[NextTerritoryLvlIndex++];

            //--- If next territory couldnt be found
            if (!NextTerritoryLvl) return;
            
            int paymentAmount = NextTerritoryLvl.GetPrice();
            bool MoneyResult = RemoveCurrency(player, paymentAmount);
            
            if (MoneyResult)
            {
                territory.Upgrade(NextTerritoryLvl);
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You have sucessfully upgraded your territory!", "set:ccgui_enforce image:Icon40Move");
            }
            else
                NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You dont have enough money you cheap fuck", "set:ccgui_enforce image:Icon40Move");
        }
        else
            NotificationSystem.SendNotificationToPlayerIdentityExtended(sender, 3, "Territory", "You don't have a territory you fucking mong", "set:ccgui_enforce image:Icon40Move");
    }

    bool RemoveCurrency(PlayerBase player, int amount)
	{
        TStringArray MoneyTypeArray =
        {
            "Poptab"
        };
		int playerCurrency = GetPlayerCurrency(player);
		if (playerCurrency < amount)
        {
			return false;
        }
		int amountTaken = 0;
		int amountRemaining = amount;
		for (int i = MoneyTypeArray.Count(); i >= 0; --i)
		{
			string currency = MoneyTypeArray[i];
			if (currency)
			{
				int currentCurrencyCount = GetCountOfSpecificItem(player, currency);
				int take = 0;

				while ((amountRemaining > 0) && (take < currentCurrencyCount))
				{
					take++;
					amountRemaining -= 1;
					amountTaken += 1;
				}

				if (take > 0)
				{
					bool removeQuantityResult = RemoveItemQuantity(player, currency, take);
					if (!removeQuantityResult) return false;
				}
			}
		}

		if (amountTaken > amount)
		{
			int change = amountTaken - amount;
			AddCurrency(player, change);
		}
		return true;
	}

    bool AddCurrency(PlayerBase player, int amount)
	{
        TStringArray MoneyTypeArray =
        {
            "Poptab"
        };
		if (!player) return false;
		int amountGiven = 0;
		int amountRemaining = amount;
		for (int i = MoneyTypeArray.Count(); i >= 0; --i)
		{
			string currency = MoneyTypeArray[i];
			
            if (currency)
			{
				int give = 0;
                
				while (amountRemaining >= 1)
				{
					give++;
					amountRemaining -= 1;
					amountGiven += 1;
				}


				if (give > 0) CreateInInventory(player, currency, give);
			}
		}
		return true;
	}


    EXPTerritory LoadTerritory(string guid)
    {
        EXPTerritory territory = EXPTerritory.Read(guid);
        Territories.Insert(territory);
        return territory;
    }

    EXPTerritory GetPlayerTerritoryByUID(string uid)
    {
        for (int i = 0; i < Territories.Count(); i++)
        {
            EXPTerritory territory = Territories[i];
            if (!territory) 
                continue;
            
            array<ref EXPTerritoryMember> memberData = territory.Members;
            if (!memberData) continue;
            
            for (int x = 0; x < memberData.Count(); x++)
                if (memberData[x].UID == uid) return territory;
        }
        return null;
    }


    EXPTerritory GetTerritoryByGUID(string guid)
    {
        if (!Territories) return null;
        
        for (int i = 0; i < Territories.Count(); i++)
        {
            if (!Territories[i]) continue;

            if (Territories[i].GetGUID() == guid)
                return Territories[i];
        }

        return null;
    }

    bool PlayerHasTerritory(string uid) return GetPlayerTerritoryByUID(uid) != null;

    string GetPlayerTerritoryGUID(string uid)
    {
        for (int i = 0; i < Territories.Count(); i++)
        {
            EXPTerritory territory = Territories[i];
            if (!territory) continue;
            array<ref EXPTerritoryMember> memberData = territory.Members;
            if (!memberData) 
                continue;
            for (int x = 0; x < memberData.Count(); x++)
                if (memberData[x].UID == uid) 
                    return territory.GetGUID();
        }
        return string.Empty;
    }

    //--- If distance is left to 0 it will just check to see if player is within radius
    bool IsPlayerInOwnTerritory(Man player, float distance = 0) 
    {
        string uid = player.GetIdentity().GetPlainId();
        vector playerPos = player.GetPosition();

        EXPTerritory Territory = GetPlayerTerritoryByUID(uid);

        if (Territory)
        {
            vector TerritoryPosition = Territory.GetPosition();
            int TerritoryRadius = Territory.GetLevel().GetRadius();

            if (distance == 0 && vector.Distance(playerPos, TerritoryPosition) > TerritoryRadius) return false;
            if (distance != 0 && vector.Distance(playerPos, TerritoryPosition) > distance) return false;

            return true;
        }
    
        return false;
    }

    bool IsPlayerInOwnTerritory(PlayerBase player, float distance = 0) 
    {
        EXPTerritory Territory = GetPlayerTerritoryByUID(player.GetIdentity().GetPlainId());
        vector playerPos = player.GetPosition();
        
        if (!playerPos) return false;

        if (Territory)
        {
            vector TerritoryPosition = Territory.GetPosition();
            int TerritoryRadius = Territory.GetLevel().GetRadius();

            if (distance == 0 && vector.Distance(playerPos, TerritoryPosition) > TerritoryRadius) return false;
            if (distance != 0 && vector.Distance(playerPos, TerritoryPosition) > distance) return false;

            return true;
        }
    
        return false;
    }
}

ref TerritoryManagerServer g_TerritoryManagerServer;
TerritoryManagerServer GetTerritoryManagerServer()
{
    if (!g_TerritoryManagerServer)
        g_TerritoryManagerServer = new TerritoryManagerServer;
    return g_TerritoryManagerServer;
}