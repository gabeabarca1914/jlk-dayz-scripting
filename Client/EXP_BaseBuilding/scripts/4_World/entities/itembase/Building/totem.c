/**
 * totem.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class TerritoryFlag
{
	bool m_IsDecaying;

	void TerritoryFlag() 
	{
		RegisterNetSyncVariableBool("m_IsDecaying");
	}

    override bool OnStoreLoad(ParamsReadContext ctx, int version)
	{
		if (!super.OnStoreLoad(ctx, version)) return false;
		
		if (GUID) 
			GetTerritoryManagerServer().LoadTerritory(GUID);
		
		return true;
	}
	
	bool GetIsDecaying() return m_IsDecaying;
	void SetIsDecaying(bool value)
	{
		m_IsDecaying = value;
		SetSynchDirty();
	}

	override void SetActions()
	{
		super.SetActions();
		
		AddAction(ActionWorldFlagActionSwitch);
		AddAction(ActionRaiseFlag);
		AddAction(ActionLowerFlag);
		AddAction(ActionFoldBaseBuildingObject);
	}
}