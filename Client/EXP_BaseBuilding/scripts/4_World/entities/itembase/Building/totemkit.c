/**
 * totemkit.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class TerritoryFlagKit
{	
	override void OnPlacementComplete( Man player, vector position = "0 0 0", vector orientation = "0 0 0" )
	{
		if ( GetGame().IsServer() )
		{
			//Create TerritoryFlag
			PlayerBase player_base = PlayerBase.Cast( player );
			vector pos = player_base.GetLocalProjectionPosition();
			vector ori = player_base.GetLocalProjectionOrientation();
			
			TerritoryFlag totem = TerritoryFlag.Cast( GetGame().CreateObjectEx( "TerritoryFlag", GetPosition(), ECE_PLACE_ON_SURFACE ) );
			totem.SetPosition( pos );
			totem.SetOrientation( ori );
			
			//make the kit invisible, so it can be destroyed from deploy UA when action ends
			HideAllSelections();
			
			SetIsDeploySound( true );

            // Auto build the territory flag if this option is enabled in the territory config
			if (GetTerritoryManagerServer().Config.AutoBuildTerritoryFlag == 1) {
				totem.GetConstruction().ForceBuildPart(player, "base");
				totem.GetConstruction().ForceBuildPart(player, "support");
				totem.GetConstruction().ForceBuildPart(player, "pole");    
				totem.GetInventory().CreateAttachment("Flag_DayZ");
			}
			
			//--- Got moved to UI/RPC call
            //GetTerritoryManagerServer().RegisterTerritory(totem, player_base);
		} else {
			UIManager uiManager = g_Game.GetUIManager();
            uiManager.CloseAll();
            uiManager.ShowScriptedMenu(GetNameTerritoryMenu(totem, player_base), NULL);
		}
	}
}