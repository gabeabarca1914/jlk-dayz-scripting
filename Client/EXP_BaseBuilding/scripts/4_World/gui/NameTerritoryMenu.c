/**
 * NameTerritoryMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class NameTerritoryMenu : UIScriptedMenu
{
    TerritoryFlag m_PlotPole;
    PlayerBase m_Player;

    void NameTerritoryMenu(TerritoryFlag plot, PlayerBase plyer) 
    {
        m_PlotPole = plot;
        m_Player = plyer;
    }

    void ~NameTerritoryMenu() 
    {
        OnClose();
    }

    EditBoxWidget territoryNameBox;
    ButtonWidget cancelBtn;
    ButtonWidget confirmBtn;


    bool isOpen = false;
    ref AbstractWave m_MenuMusic;

    override Widget Init() {
        layoutRoot = GetGame().GetWorkspace().CreateWidgets("EXP_BaseBuilding/Layouts/EXPNameTerritoryMenu.layout");
        layoutRoot.Show(false);

        territoryNameBox = EditBoxWidget.Cast(layoutRoot.FindAnyWidget("territoryNameBox"));
        cancelBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("cancelBtn"));
        confirmBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("confirmBtn"));
    
        return layoutRoot;
    }

    void OnShow() {  
        
        g_NameTerritoryMenu = this;

        isOpen = true;

        GetGame().GetInput().ChangeGameFocus(1);
        GetGame().GetUIManager().ShowUICursor(true);
        GetGame().GetMission().GetHud().Show(false);
        PPEffects.SetBlurInventory(1);
    }

    void OnClose() {

        g_NameTerritoryMenu = NULL;

        isOpen = false;
        
        SetFocus(NULL);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);
        PPEffects.SetBlurInventory(0);

        Close();
    }

    void Hide()
    {
        if (!g_Game.GetUIManager().IsMenuOpen(MENU_INGAME))
        {
            g_Game.GetUIManager().CloseAll();
            GetGame().GetInput().ResetGameFocus();
            GetGame().GetUIManager().ShowUICursor(false);
            GetGame().GetMission().GetHud().Show(true);
        }

        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);
        
        layoutRoot.Show(false);
    }

    override bool OnClick(Widget w, int x, int y, int button) {

        super.OnClick(w, x, y, button);
        switch (w)
        {
        case cancelBtn:
            return OnCancelBtnClick();
        case confirmBtn:
            return OnConfirmBtnClick();
        default:
            break;
        }
        return true;
    }

    bool OnCancelBtnClick()
    {
        Hide();
        return true;
    }

    bool OnConfirmBtnClick() {

        //--- Get the text content of the name box
        string name = territoryNameBox.GetText();

        //--- Ensure the name is long enough
        if (name.Length() >= GetTerritoryManagerClient().Config.MinimumTerritoryNameLength) 
        {
            GetRPCManager().SendRPC("EXPTerritory", "NameTerritoryRequest", new Param3<string, TerritoryFlag, PlayerBase>(name, m_PlotPole, m_Player), true, NULL);
            NotificationSystem.AddNotificationExtended(3, "Territory Creation", "You have created your territory! Congrats!", "set:ccgui_enforce image:Icon40Move");
            SoundParams soundParams = new SoundParams( "Yay_SoundSet" );
            SoundObjectBuilder soundBuilder = new SoundObjectBuilder( soundParams );
            SoundObject soundObject = soundBuilder.BuildSoundObject();
            soundObject.SetKind(WaveKind.WAVEEFFECT);
            m_MenuMusic = GetGame().GetSoundScene().Play2D(soundObject, soundBuilder);
            m_MenuMusic.Play();
            Hide();
        } else
            NotificationSystem.AddNotificationExtended(3, "Territory Creation", "Your Territory Name Is Too Short!", "set:ccgui_enforce image:Icon40Move");

        return true;
    }
}

ref NameTerritoryMenu g_NameTerritoryMenu;
NameTerritoryMenu GetNameTerritoryMenu(TerritoryFlag plot, PlayerBase plyer)
{
    if (!g_NameTerritoryMenu)
        g_NameTerritoryMenu = new NameTerritoryMenu(plot, plyer);
    return g_NameTerritoryMenu;
}