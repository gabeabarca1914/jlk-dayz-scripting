/**
 * TerritoryPaymentMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Sun Apr 04 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class TerritoryPaymentMenu : UIScriptedMenu
{
    void TerritoryPaymentMenu();

    void ~TerritoryPaymentMenu() 
    {
        OnClose();
    }

    Widget mainPanel;
    Widget contentPanel;

    ButtonWidget UpgradeLvlBtn;
    ButtonWidget PayBtn;
    ButtonWidget UpgradeProtectionBtn;
	ButtonWidget CloseMenuBtn;

    TextListboxWidget partsList;
    TextListboxWidget MemberList;

    TextWidget timeText;
    TextWidget territoryName;
    TextWidget territoryLevel;
    TextWidget PartCountText;
    TextWidget TerritoryMembers;
    TextWidget UpgradeLevelDesc;
    TextWidget UpgradeProtectionDesc;
    TextWidget priceText;
    TextWidget BaseRentFee;
    TextWidget ObjectFee;
    TextWidget MemberFee;
    TextWidget RegionFee;
    TextWidget TerritoryProtectionFee;

    override Widget Init()
    {
        layoutRoot = GetGame().GetWorkspace().CreateWidgets("EXP_BaseBuilding/Layouts/EXPTerritoryPaymentMenu.layout");
        layoutRoot.Show(true);

        mainPanel = layoutRoot.FindAnyWidget("MainPanel");
        contentPanel = layoutRoot.FindAnyWidget("contentPanel");

        UpgradeLvlBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("UpgradeLvlBtn"));
        PayBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("PayBtn"));
        UpgradeProtectionBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("UpgradeProtectionBtn"));
		CloseMenuBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("CloseMenuBtn"));

        partsList = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("partsList"));
        MemberList = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("MemberList"));

        timeText = TextWidget.Cast(layoutRoot.FindAnyWidget("timeText"));
        territoryName = TextWidget.Cast(layoutRoot.FindAnyWidget("territoryName"));
        territoryLevel = TextWidget.Cast(layoutRoot.FindAnyWidget("territoryLevel"));
        PartCountText = TextWidget.Cast(layoutRoot.FindAnyWidget("PartCountText"));
        TerritoryMembers = TextWidget.Cast(layoutRoot.FindAnyWidget("TerritoryMembers"));
        UpgradeLevelDesc = TextWidget.Cast(layoutRoot.FindAnyWidget("UpgradeLevelDesc"));
        UpgradeProtectionDesc = TextWidget.Cast(layoutRoot.FindAnyWidget("UpgradeProtectionDesc"));
        priceText = TextWidget.Cast(layoutRoot.FindAnyWidget("priceText"));

        
        BaseRentFee = TextWidget.Cast(layoutRoot.FindAnyWidget("BaseRentFee"));
        ObjectFee = TextWidget.Cast(layoutRoot.FindAnyWidget("ObjectFee"));
        MemberFee = TextWidget.Cast(layoutRoot.FindAnyWidget("MemberFee"));
        RegionFee = TextWidget.Cast(layoutRoot.FindAnyWidget("RegionFee"));
        TerritoryProtectionFee = TextWidget.Cast(layoutRoot.FindAnyWidget("TerritoryProtection"));
            
        return layoutRoot;
    }

    void OnShow()
    {  
        g_TerritoryPaymentMenu = this;

        UpdateMenu();

        GetGame().GetInput().ChangeGameFocus(1);
        GetGame().GetUIManager().ShowUICursor(true);
        GetGame().GetMission().GetHud().Show(false);

        PPEffects.SetBlurInventory(1);
    }

   
    void UpdateTerritoryInfo()
    {
        if (GetTerritoryManagerClient().IsTerritoryMember())
        {
            EXPTerritory territory = GetTerritoryManagerClient().Territory;
            if (territory) 
            {
                UpdatePartsList();

                territoryName.SetText(territory.GetName());
                territoryLevel.SetText(territory.Level.GetName());
                timeText.SetText(territory.GetRemainingLifeTimeDisplay() + " Remaining");

                array<Object> parts = territory.GetPartArray();
                float partsPrice = 0;
                foreach(Object BasePart: parts)
                    partsPrice += territory.GetItemPrice(BasePart.GetType()) * GetTerritoryManagerClient().Config.MaintenanceCalculationTimer;

                BaseRentFee.SetText(territory.GetLevel().GetPrice().ToString());
                ObjectFee.SetText(partsPrice.ToString());
                MemberFee.SetText((territory.Members.Count() * 400).ToString());
                RegionFee.SetText("N/A");
                TerritoryProtectionFee.SetText("N/A");
                
                priceText.SetText("Current Rent Owed $" + Math.Ceil(territory.CurrentMaintenancePrice));

                //--- Next Territory Info
                int NextTerritoryLvlIndex = GetTerritoryManagerClient().Config.TerritoryLevels.Find(territory.GetLevel());
                
                //--- Cannot increment on 0
                EXPTerritoryLevel NextTerritoryLvl;
                if (NextTerritoryLvlIndex == -1)
                    NextTerritoryLvl = GetTerritoryManagerClient().Config.TerritoryLevels[1];
                else
                    NextTerritoryLvl = GetTerritoryManagerClient().Config.TerritoryLevels[NextTerritoryLvlIndex++];
                
                if (NextTerritoryLvl)
                {
                    string NextTerritoryName    = NextTerritoryLvl.GetName();
                    int NextTerritoryObjCount   = NextTerritoryLvl.GetMaxObjectCount();
                    float NextTerritoryRadius   = NextTerritoryLvl.GetRadius();
                    int NextTerritoryPrice      = NextTerritoryLvl.GetPrice();

                    UpgradeLevelDesc.SetText(string.Format("Next Level: '%1' (%2 Objects - %3 m ) $%4", NextTerritoryName, NextTerritoryObjCount, NextTerritoryRadius, NextTerritoryPrice));
                }
            }
        } else MemberList.ClearItems();
    }

   
    void UpdatePartsList() 
    {
        EXPTerritory territory = GetTerritoryManagerClient().Territory;
        map<string, int> partCountMap = territory.GetPartCountMap();

        int itemCount = 0;
        
        partsList.ClearItems();

        for (int i = 0; i < partCountMap.Count(); i++)
        {
            string currentIndexKey = partCountMap.GetKey(i);
            int currentIndexValue = partCountMap.GetElement(i);
            itemCount += currentIndexValue;
            
            partsList.AddItem(GetDisplayName(currentIndexKey) + " x " + currentIndexValue, NULL, 0);
        }
                
        PartCountText.SetText(string.Format("Total Parts In Territory : %1", itemCount));
    }

    void OnClose()
    {
        g_TerritoryPaymentMenu = NULL;
        
        SetFocus(NULL);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);

        PPEffects.SetBlurInventory(0);

        Close();
    }

    override bool OnClick(Widget w, int x, int y, int button)
    {
        super.OnClick(w, x, y, button);
        switch (w)
        {
            case PayBtn:
                GetRPCManager().SendRPC("EXPTerritory", "PayTerritoryRequest", null, true);
                return true;
            break;

            case UpgradeLvlBtn:
                GetRPCManager().SendRPC("EXPTerritory", "UpgradeTerritoryRequest", null, true);
                return true;
            break;
			
			case CloseMenuBtn:
				OnClose();
				return true;
			break;
        }
        return true;
    }

    void UpdateMenu() UpdateTerritoryInfo();
    
    void UpdateAllPlayersList()
    {
        array<ref EXPTerritoryMember> players = GetTerritoryManagerClient().AllPlayers;
        
        if (MemberList)
        {
            MemberList.ClearItems();

            foreach(EXPTerritoryMember TerrMember: players)
            {
                string playerNameLower = TerrMember.Name;
                playerNameLower.ToLower();

                MemberList.AddItem(TerrMember.Name, TerrMember, 0);
            }
        }
    }

    void UpdateTerritoryMembersList()
    {
        if (GetTerritoryManagerClient().IsTerritoryMember())
        {
            array<ref EXPTerritoryMember> players = GetTerritoryManagerClient().Territory.Members;
            TStringArray Admins =  GetTerritoryManagerClient().Territory.AdminUIDS;

            if (MemberList)
            {
                MemberList.ClearItems();

                for (int i = 0; i < players.Count(); i++)
                {
                    bool isAdmin = (Admins.Find(players[i].UID) != -1);
                    bool isOwner = (GetTerritoryManagerClient().Territory.IsOwner(players[i].UID));

                    int row = -1;

                    string playerNameLower = players[i].Name;
                    playerNameLower.ToLower();

                    if (isAdmin && !isOwner)
                    {
                        row = MemberList.AddItem(players[i].Name + " [TERRITORY ADMIN]", players[i], 0);
                        MemberList.SetItemColor(row, 0, 0x7f0000ff);
                    }

                    if (isOwner)
                    {
                        row = MemberList.AddItem(players[i].Name + " [TERRITORY OWNER]", players[i], 0);
                        MemberList.SetItemColor(row, 0, 0x7f00ff00);
                    }

                    if (!isAdmin && !isOwner) MemberList.AddItem(players[i].Name, players[i], 0);
                }
            }
        }
    }

   
    void Hide()
    {
        SetFocus(NULL);
        
        if (!g_Game.GetUIManager().IsMenuOpen(MENU_INGAME))
        {
            g_Game.GetUIManager().CloseAll();
            GetGame().GetInput().ResetGameFocus();
            GetGame().GetUIManager().ShowUICursor(false);
            GetGame().GetMission().GetHud().Show(true);
        }

        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);

        layoutRoot.Show(false);
    }
}

ref TerritoryPaymentMenu g_TerritoryPaymentMenu;
TerritoryPaymentMenu GetTerritoryPaymentMenu()
{
    if (!g_TerritoryPaymentMenu)
        g_TerritoryPaymentMenu = new TerritoryPaymentMenu;
    return g_TerritoryPaymentMenu;
}