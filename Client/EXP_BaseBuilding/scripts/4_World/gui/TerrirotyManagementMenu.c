/**
 * TerrirotyManagementMenu.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class TerritoryManagementMenu : UIScriptedMenu
{    
    bool isOpen = false;

    Widget mainPanel;
    Widget contentPanel;
    ButtonWidget territoryPlayersBtn;
    ButtonWidget allPlayersBtn;
    EditBoxWidget searchBox;
    TextListboxWidget playersList;
    ButtonWidget inviteKickBtn;
    ButtonWidget promoteDemoteBtn;
    ButtonWidget leaveBtn;
    ButtonWidget closeBtn;
    TextListboxWidget partsList;
    TextWidget priceText;
    TextWidget timeText;
    TextWidget territoryName;
    TextWidget territoryLevel;
    TextWidget PartCountText;

    
    Widget invitePanel;
    TextWidget inviteText;
    ButtonWidget acceptInviteBtn;
    ButtonWidget declineInviteBtn;

    
    Widget confirmPopup;
    TextWidget popupHeader;
    TextWidget popupContent;
    ButtonWidget popupCancelButton;
    ButtonWidget popupConfirmButton;

    
    TerritoryList activeList;

    override Widget Init()
    {
        layoutRoot          = GetGame().GetWorkspace().CreateWidgets("EXP_BaseBuilding/Layouts/EXPTerritoryMenu.layout");
        layoutRoot.Show(false);

        mainPanel           = layoutRoot.FindAnyWidget("MainPanel");
        contentPanel        = layoutRoot.FindAnyWidget("contentPanel");
        territoryPlayersBtn = ButtonWidget.Cast(layoutRoot.FindAnyWidget("TerritoryPlayersBtn"));
        allPlayersBtn       = ButtonWidget.Cast(layoutRoot.FindAnyWidget("AllPlayersBtn"));
        searchBox           = EditBoxWidget.Cast(layoutRoot.FindAnyWidget("SearchBox"));
        playersList         = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("playersList"));
        inviteKickBtn       = ButtonWidget.Cast(layoutRoot.FindAnyWidget("InviteKickBtn"));
        promoteDemoteBtn    = ButtonWidget.Cast(layoutRoot.FindAnyWidget("promoteDemoteBtn"));
        leaveBtn            = ButtonWidget.Cast(layoutRoot.FindAnyWidget("LeaveBtn"));
        closeBtn            = ButtonWidget.Cast(layoutRoot.FindAnyWidget("CloseBtn"));
        partsList           = TextListboxWidget.Cast(layoutRoot.FindAnyWidget("partsList"));
        priceText           = TextWidget.Cast(layoutRoot.FindAnyWidget("priceText"));
        timeText            = TextWidget.Cast(layoutRoot.FindAnyWidget("timeText"));
        territoryName       = TextWidget.Cast(layoutRoot.FindAnyWidget("territoryName"));
        territoryLevel      = TextWidget.Cast(layoutRoot.FindAnyWidget("territoryLevel"));
        PartCountText       = TextWidget.Cast(layoutRoot.FindAnyWidget("PartCountText"));

        
        invitePanel         = layoutRoot.FindAnyWidget("InvitesPanel");
        inviteText          = TextWidget.Cast(layoutRoot.FindAnyWidget("InvitesText"));
        acceptInviteBtn     = ButtonWidget.Cast(layoutRoot.FindAnyWidget("acceptInviteBtn"));
        declineInviteBtn    = ButtonWidget.Cast(layoutRoot.FindAnyWidget("declineInviteBtn"));

        
        confirmPopup        = layoutRoot.FindAnyWidget("confirmPopup");
        popupHeader         = TextWidget.Cast(layoutRoot.FindAnyWidget("popupHeader"));
        popupContent        = TextWidget.Cast(layoutRoot.FindAnyWidget("popupContent"));
        popupCancelButton   = ButtonWidget.Cast(layoutRoot.FindAnyWidget("popupCancelButton"));
        popupConfirmButton  = ButtonWidget.Cast(layoutRoot.FindAnyWidget("popupConfirmButton"));
        //OnShow();
        return layoutRoot;
    }

    void ~TerritoryManagementMenu()
    {
        OnClose();
    }

    void OnShow()
    {  
        g_TerritoryManagementMenu = this;
        isOpen = true;
        allPlayersBtn.SetTextColor(0x7f00ff00);
        territoryPlayersBtn.SetTextColor(0x7fffffff);
        activeList = TerritoryList.All;
        
        GetRPCManager().SendRPC("EXPTerritory", "GetAllPlayerListRequest", NULL, true, NULL);

        UpdateMenu();
        GetGame().GetInput().ChangeGameFocus(1);
        GetGame().GetUIManager().ShowUICursor(true);
        GetGame().GetMission().GetHud().Show(false);
        PPEffects.SetBlurInventory(1);
    }
   
    void UpdateInvitesPanel()
    {
        EXPTerritoryInvite invite = GetTerritoryManagerClient().GetActiveInvite();

        if (invite)
        {
            invitePanel.Show(true);
            inviteText.SetText(invite.InviterName + " Has invited you to their territory");
        } else 
            invitePanel.Show(false);
    }

   
    void UpdateTerritoryInfo()
    {
        if (GetTerritoryManagerClient().IsTerritoryMember()) 
        {
            EXPTerritory territory = GetTerritoryManagerClient().Territory;
            if (territory) 
            {
                UpdatePartsList();
                territoryName.SetText(territory.GetName());
                territoryLevel.SetText(territory.Level.GetName());
                timeText.SetText(GetTerritoryManagerClient().Territory.GetRemainingLifeTimeDisplay() + " Remaining");
                priceText.SetText("Current Rent Owed $" + Math.Ceil(GetTerritoryManagerClient().Territory.CurrentMaintenancePrice));
            }
        } 
        else playersList.ClearItems();
    }

   
    void UpdatePartsList() 
    {
        EXPTerritory territory = GetTerritoryManagerClient().Territory;
        map<string, int> partCountMap = territory.GetPartCountMap();

        int itemCount = 0;
        
        partsList.ClearItems();

        for (int i = 0; i < partCountMap.Count(); i++)
        {
            string currentIndexKey = partCountMap.GetKey(i);
            int currentIndexValue = partCountMap.GetElement(i);
            itemCount += currentIndexValue;
            
            partsList.AddItem(GetDisplayName(currentIndexKey) + " x " + currentIndexValue, NULL, 0);
        }
                
        PartCountText.SetText(string.Format("Total Parts In Territory : %1", itemCount));
    }

    void OnClose()
    {
        g_TerritoryManagementMenu = NULL;
        isOpen = false;

        SetFocus(NULL);
        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);
        PPEffects.SetBlurInventory(0);

        Close();
    }

    override bool OnClick(Widget w, int x, int y, int button)
    {

        super.OnClick(w, x, y, button);
        switch (w)
        {
            case allPlayersBtn:         return OnAllPlayersBtnClick();
            case territoryPlayersBtn:   return OnTerritoryPlayersBtnClick();
            case leaveBtn:              return OnLeaveBtnClick();
            case inviteKickBtn:         return OnInviteKickBtnClick();
            case promoteDemoteBtn:      return OnPromoteDemoteBtnClick();
            case closeBtn:              return OnCloseBtnClick();
            case acceptInviteBtn:       return OnAcceptInviteClick();
            case declineInviteBtn:      return onDeclineInviteClick();
            case playersList:           return onPlayersListClick();
            case popupCancelButton:     return onPopupCancelClick();
            case popupConfirmButton:    return onPopupConfirmClick();
        }
        return true;
    }

    void UpdateMenu()
    {
        if (!GetTerritoryManagerClient().IsTerritoryMember()) activeList = TerritoryList.All;

        UpdateTerritoryInfo();
        UpdateActiveList();
        UpdateInvitesPanel();
    }

    void UpdateActiveList()
    {
        if (activeList == TerritoryList.All) 
            UpdateAllPlayersList();
        else 
            UpdateTerritoryMembersList();
    }
    
    void UpdateAllPlayersList()
    {
        array<ref EXPTerritoryMember> players = GetTerritoryManagerClient().AllPlayers;
        
        if (searchBox)
        {
            string searchLower = searchBox.GetText();
            searchLower.ToLower();
        }

        if (playersList)
        {
            playersList.ClearItems();

            for (int i = 0; i < players.Count(); i++)
            {
                string playerNameLower = players[i].Name;
                playerNameLower.ToLower();

                if (searchLower == "" || searchLower == "search..." || playerNameLower.Contains(searchLower)) 
                    playersList.AddItem(players[i].Name, players[i], 0);
            }
        }
    }

    void UpdateTerritoryMembersList()
    {
        if (GetTerritoryManagerClient().IsTerritoryMember())
        {
            array<ref EXPTerritoryMember> players = GetTerritoryManagerClient().Territory.Members;
            TStringArray Admins =  GetTerritoryManagerClient().Territory.AdminUIDS;

            if (searchBox)
            {
                string searchLower = searchBox.GetText();
                searchLower.ToLower();
            }


            if (playersList)
            {
                playersList.ClearItems();

                for (int i = 0; i < players.Count(); i++)
                {
                    bool isAdmin = (Admins.Find(players[i].UID) != -1);
                    bool isOwner = (GetTerritoryManagerClient().Territory.IsOwner(players[i].UID));

                    int row = -1;

                    string playerNameLower = players[i].Name;
                    playerNameLower.ToLower();

                    if (searchLower == "" || searchLower == "search..." || playerNameLower.Contains(searchLower))
                    {
                        if (isAdmin && !isOwner)
                        {
                            row = playersList.AddItem(players[i].Name + " [TERRITORY ADMIN]", players[i], 0);
                            playersList.SetItemColor(row, 0, 0x7f0000ff);
                        }

                        
                        if (isOwner)
                        {
                            row = playersList.AddItem(players[i].Name + " [TERRITORY OWNER]", players[i], 0);
                            playersList.SetItemColor(row, 0, 0x7f00ff00);
                        }

                        
                        if (!isAdmin && !isOwner) playersList.AddItem(players[i].Name, players[i], 0);
                    }
                }
            }
        }
    }

   
    bool OnAcceptInviteClick()
    {
        GetTerritoryManagerClient().AcceptInvite();
        return true;
    }

   
    bool onDeclineInviteClick()
    {        
        GetTerritoryManagerClient().DeclineInvite();
        return true;
    }


    bool OnLeaveBtnClick()
    {
        contentPanel.Show(false);
        popupHeader.SetText("Are you sure you want to leave ?");
        popupContent.SetText("Leaving will mean you no longer have acceess to build in this territory");
        confirmPopup.Show(true);
        return true;
    }

   
    bool OnInviteKickBtnClick()
    {
        if (!GetTerritoryManagerClient().IsTerritoryMember())
        {
            NotificationSystem.AddNotificationExtended(3, "Territory Management", "You are not a member of a territory", "set:ccgui_enforce image:Icon40Move");
            return true;
        }
        
        if (!GetTerritoryManagerClient().IsTerritoryAdmin())
        {
            NotificationSystem.AddNotificationExtended(3, "Territory Management", "You are not a admin of this territory", "set:ccgui_enforce image:Icon40Move");
            return true;
        }

        
        EXPTerritoryMember player = GetSelectedPlayer();

        if (!player) return true; 
        
        if (player.UID == GetTerritoryManagerClient().OwnUID)
        {
            if (activeList == TerritoryList.All) NotificationSystem.AddNotificationExtended(3, "Territory Management", "What are you trying to do?! You cant invite yourself...", "set:ccgui_enforce image:Icon40Move"); 
            else NotificationSystem.AddNotificationExtended(3, "Territory Management", "What are you trying to do?! You cant kick yourself...", "set:ccgui_enforce image:Icon40Move");
            return true;
        }
        
        if (activeList == TerritoryList.All) GetRPCManager().SendRPC("EXPTerritory", "InvitePlayerRequest", new Param1<ref EXPTerritoryMember>(player), true, NULL);
        else GetRPCManager().SendRPC("EXPTerritory", "KickPlayerRequest", new Param1<ref EXPTerritoryMember>(player), true, NULL);

        return true;
    }

    bool OnPromoteDemoteBtnClick()
    {
        if (!GetTerritoryManagerClient().IsTerritoryMember())
        {
            NotificationSystem.AddNotificationExtended(3, "Territory Management", "You are not a member of a territory", "set:ccgui_enforce image:Icon40Move");
            return true;
        }
        
        if (!GetTerritoryManagerClient().IsTerritoryOwner())
        {
            NotificationSystem.AddNotificationExtended(3, "Territory Management", "Only a territory owner can premote or demote someone", "set:ccgui_enforce image:Icon40Move");
            return true;
        }

        
        EXPTerritoryMember player = GetSelectedPlayer();

        if (!player) return true;
        
        EXPTerritory territory = GetTerritoryManagerClient().Territory;

        if (territory)
        {
            if (territory.IsAdmin(player.UID))
            {
                GetRPCManager().SendRPC("EXPTerritory", "DemotePlayerRequest", new Param1<ref EXPTerritoryMember>(player), true, NULL);
            } else {
                GetRPCManager().SendRPC("EXPTerritory", "PromotePlayerRequest", new Param1<ref EXPTerritoryMember>(player), true, NULL);
            }
        }

        return true;
    }
    
   
    bool onPopupConfirmClick()
    {
        GetRPCManager().SendRPC("EXPTerritory", "LeaveTerritoryRequest", NULL, true, NULL);
        contentPanel.Show(true);
        confirmPopup.Show(false);
        return true;
    }

   
    bool onPopupCancelClick()
    {
        contentPanel.Show(true);
        confirmPopup.Show(false);
        return true;
    }

   
    bool onPlayersListClick()
    {
        
        if (activeList == TerritoryList.Territory)
        {
            
            EXPTerritoryMember player = GetSelectedPlayer();
            if (player)
            {
                if (GetTerritoryManagerClient().Territory.IsAdmin(player.UID))
                {
                    promoteDemoteBtn.SetText("Demote");
                } else {
                    promoteDemoteBtn.SetText("Promote");
                }
            }
        }
        return true;
    }
    
   
    bool OnAllPlayersBtnClick()
    {
        
        GetRPCManager().SendRPC("EXPTerritory", "GetAllPlayerListRequest", NULL, true, NULL);

        activeList = TerritoryList.All;
        inviteKickBtn.SetText("Invite");
        
        promoteDemoteBtn.Enable(false);

        allPlayersBtn.SetTextColor(0x7f00ff00);

        territoryPlayersBtn.SetTextColor(0x7fffffff);

        return true;
    }
    
   
    bool OnTerritoryPlayersBtnClick()
    {
        activeList = TerritoryList.Territory;

        inviteKickBtn.SetText("Kick");
        
        allPlayersBtn.SetTextColor(0x7fffffff);

        promoteDemoteBtn.Enable(true);

        territoryPlayersBtn.SetTextColor(0x7f00ff00);

        if (GetTerritoryManagerClient().IsTerritoryMember())
        {
            promoteDemoteBtn.Enable(GetTerritoryManagerClient().IsTerritoryOwner());
            UpdateActiveList();
        }
        else
        {
            GetGame().GetMission().OnEvent(ChatMessageEventTypeID, new ChatMessageEventParams(CCDirect, "", "You are not a member of a territory", ""));
        }

        return true;
    }

    bool OnCloseBtnClick()
    {
        Hide();
        return true;
    }

   
    void Hide()
    {
        SetFocus(NULL);
        
        if (!g_Game.GetUIManager().IsMenuOpen(MENU_INGAME))
        {
            g_Game.GetUIManager().CloseAll();
            GetGame().GetInput().ResetGameFocus();
            GetGame().GetUIManager().ShowUICursor(false);
            GetGame().GetMission().GetHud().Show(true);
        }

        GetGame().GetInput().ResetGameFocus();
        GetGame().GetUIManager().ShowUICursor(false);
        GetGame().GetMission().GetHud().Show(true);

        //GetGame().GetCallQueue(CALL_CATEGORY_GUI).RemoveByName(this, "CheckForSearchChange");

        layoutRoot.Show(false);
    }
    
   
    EXPTerritoryMember GetSelectedPlayer()
    {
        int index = playersList.GetSelectedRow();
        if (index != -1)
        {
            EXPTerritoryMember player;
            playersList.GetItemData(index, 0, player);
            if (player)
            {
                return player;
            }
        }

        return NULL;
    }
}

ref TerritoryManagementMenu g_TerritoryManagementMenu;
TerritoryManagementMenu GetTerritoryManagementMenu()
{
    if (!g_TerritoryManagementMenu)
        g_TerritoryManagementMenu = new TerritoryManagementMenu;
    return g_TerritoryManagementMenu;
}