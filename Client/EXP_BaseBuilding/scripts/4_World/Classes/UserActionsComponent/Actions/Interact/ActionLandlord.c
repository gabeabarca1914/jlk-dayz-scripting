/**
 * ActionLandlord.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Mon Mar 29 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionLandlord: ActionInteractBase
{
    string m_InteractText;
	void ActionLandlord()
	{
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_INTERACTONCE;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_HUDCursorIcon = CursorIcons.CloseHood;
        m_InteractText = "";
	}

    override void CreateConditionComponents()  
	{
		m_ConditionTarget = new CCTObject(4);
		m_ConditionItem = new CCINone;
	}

	override string GetText()
	{
		return m_InteractText;
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
    {
        if (GetGame().IsServer())
			return true;
    
        if (!player)
            return false;

        if (!GetTerritoryManagerClient().IsTerritoryMember())
            m_InteractText = "You Need To Join A Territory First!";
        
        if (GetTerritoryManagerClient().IsTerritoryMember())
            m_InteractText = "Talk To Landlord";

        return true;
    }
    
    override void OnStartClient(ActionData action_data)
    {
        UIManager uiManager = GetGame().GetUIManager();
        Man player = GetDayZGame().GetPlayer();
    
        if (GetTerritoryManagerClient().IsTerritoryMember())
        {
            uiManager.CloseAll();
            uiManager.ShowScriptedMenu(GetTerritoryPaymentMenu(), NULL);
        }
    }
}