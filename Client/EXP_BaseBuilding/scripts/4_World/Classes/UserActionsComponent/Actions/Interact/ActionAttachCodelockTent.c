/**
 * ActionAttachCodelockTent.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Mon May 03 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionAttachCodelockTent: ActionSingleUseBase
{
    override void CreateConditionComponents() {
        m_ConditionItem = new CCINonRuined;
        m_ConditionTarget = new CCTNone;
    }

	override string GetText()
	{
		return "Attach Codelock";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
    {
        if (GetGame().IsServer())
            return true;
        
        Object tentObject = target.GetObject();

        //--- dont show vanilla action on base objects
        if (tentObject)
        {
            //if (Class.CastTo())
            //--- Make sure they are in their own territory
            if (!GetTerritoryManagerClient().GetIsInOwnTerritory())
                return false;
    
            //--- No Item in hands
            if (!item)
                return false;

            //--- Make sure player has codelock in hand
            if (item.GetType() != "EXP_Codelock")
                return false;

            EXP_Items Storage;
            EXP_Openable_Base tent = EXP_Openable_Base.Cast(tentObject);
            
            if (!Class.CastTo(Storage, tentObject) || !Storage.CanAcceptCodelock())
                return false;

            //--- Door already has codelock
            if (tent.HasCodelock())
                return false;

            return true;
        }
		return false;
    }
    
    override bool UseMainItem()
	{
		return true;
	}

    override void OnExecuteServer( ActionData action_data )
    {
        if ( action_data.m_MainItem && action_data.m_MainItem.GetHierarchyRootPlayer() == action_data.m_Player )
        {
            action_data.m_Player.DropItem(action_data.m_MainItem);
            action_data.m_MainItem.Delete();
        }
    }

    override void OnStartServer( ActionData action_data )
	{
        if (GetGame().IsClient()) return;
        
        Object obj = action_data.m_Target.GetObject();
        EXP_Openable_Base tent = EXP_Openable_Base.Cast(obj);

        if (obj)    
        {
            tent.SetCodeLock(true);
        }
    }
};