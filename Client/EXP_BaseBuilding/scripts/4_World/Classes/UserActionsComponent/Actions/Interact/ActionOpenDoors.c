/**
 * ActionOpenDoors.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Wed Mar 31 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionOpenDoors: ActionInteractBase
{
    override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if (GetGame().IsServer())
            return true;
        
        EXP_BaseBuildingObject baseObject = EXP_BaseBuildingObject.Cast(target.GetObject());

        //--- dont show vanilla action on base objects
        if (baseObject && baseObject.IsDoor())
        {
            //--- If door is NOT code locked, just check normal status
            if (!baseObject.HasCodelock())
                return super.ActionCondition(player, target, item);

            //--- Make sure they are in their own territory
            if (GetTerritoryManagerClient().GetIsInOwnTerritory())
                return super.ActionCondition(player, target, item);
            
            return false;
        }
        
		return super.ActionCondition(player, target, item);
	}
};