/**
 * ActionDestroyPart.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Wed Apr 07 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionDestroyPart: ActionContinuousBase 
{
    override string GetText()
	{		
		return "Destroy";
	}

    override protected bool DestroyCondition( PlayerBase player, ActionTarget target, ItemBase item, bool camera_check )
	{
		if (GetGame().IsServer())
            return true;
			
		if ( player && !player.IsLeaning() )
		{
			Object target_object = target.GetObject();
            EXP_BaseBuildingObject baseObject = EXP_BaseBuildingObject.Cast(target_object);
			
			if (baseObject && baseObject.IsBaseObject() && baseObject.IsDoor())
			{
				string part_name = target_object.GetType();
				
                //--- camera and position checks
                if ( !player.GetInputController().CameraIsFreeLook() && IsInReach(player, target, UAMaxDistances.DEFAULT) && !player.GetInputController().CameraIsFreeLook() )
                {
                    //--- Camera check (client-only)
                    if ( camera_check )
                        if ( GetGame() && ( !GetGame().IsMultiplayer() || GetGame().IsClient() ) )
                            if (baseObject.IsFacingCamera()) 
                                return false;

                    return true;			
                }
			}
		}
		
		return false;
	}

    override void OnFinishProgressServer( ActionData action_data )
	{	
		EXP_BaseBuildingObject base_building = EXP_BaseBuildingObject.Cast(action_data.m_Target.GetObject());
		
		if ( base_building )
		{
			GetGame().CreateObject( base_building.KitReturn(), base_building.GetPosition());
			GetGame().ObjectDelete(action_data.m_Target.GetObject());
			
			//--- add damage to tool
			action_data.m_MainItem.DecreaseHealth( UADamageApplied.DESTROY, false );
		}

		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
	}
};