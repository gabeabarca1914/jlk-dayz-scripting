/**
 * ActionBreakIntoTent.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Apr 20 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionBreakIntoTent: ActionContinuousBase 
{
	void ActionBreakIntoTent()
	{
		m_CallbackClass = ActionBreakIntoTentDB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_DISASSEMBLE;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT;
	}
	
	override void CreateConditionComponents()  
	{	
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTNone;
	}

    override string GetText()
	{		
		return "Cut Off Lock";
	}

    override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if (GetGame().IsServer())
            return true;
        
        Object tent = target.GetObject();
        EXP_Openable_Base tentCast = EXP_Openable_Base.Cast(tent);

        //--- dont show vanilla action on base objects
        if (tent)
        {
            //--- Make sure they are in their own territory
            //if (GetTerritoryManagerClient().GetIsInOwnTerritory())
                //return false;
    
            //--- No Item in hands
            if (!item)
                return false;

            //--- Make sure player has codelock in hand
            if (item.GetType() != "Hacksaw")
                return false;
            
            //--- Door already has codelock
            if (!tentCast.HasCodelock())
                return false;
            
            //--- Check if door is open
            if (tentCast.IsOpen())
                return false;

            return true;
        }
		return false;
	}

    override void OnFinishProgressClient( ActionData action_data )
	{	
		if (GetGame().IsServer()) return;
        
        Object tent = action_data.m_Target.GetObject();
        EXP_Openable_Base tentCast = EXP_Openable_Base.Cast(tent);

        if (tent)    
        {
            tentCast.SetCodeLock(false);
        }   
	}

    override void OnFinishProgressServer( ActionData action_data )
	{	
		if (GetGame().IsClient()) return;
        
        Object tent = action_data.m_Target.GetObject();
        EXP_Openable_Base tentCast = EXP_Openable_Base.Cast(tent);

        if (tent)    
        {
            tentCast.SetCodeLock(false);
        }   
	}
};