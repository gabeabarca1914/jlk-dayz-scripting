/**
 * ActionDestroyPartCB.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Tue Apr 06 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class ActionDestroyPartCB : ActionContinuousBaseCB 
{
    override void CreateActionComponent()
	{
        if (m_ActionData)
		{		
			if (m_ActionData.m_Target)
			{
				PlayerBase m_Player         = m_ActionData.m_Player;
				EntityAI m_ItemInHands      = m_ActionData.m_MainItem;
				Object m_TargetObject       = m_ActionData.m_Target.GetObject();
                string m_TargetObjectName   = m_TargetObject.GetType();
                
                float ActionTime = 600;
                if (!m_ItemInHands) return;
                switch (m_TargetObjectName)
                {
                    /*
                    * Tier 1
                    */
                    case "T1_Door": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 600;
                        break;
                    }
                    case "T1_DoubleDoor": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 600;
                        break;
                    }
                    case "T1_Hatch": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 600;
                        break;
                    }
                    case "T1_Gate": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 600;
                        break;
                    }
                    /*
                    * Tier 2
                    */
                    case "T2_Door": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1200;
                        break;
                    }
                    case "T2_DoubleDoor": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1200;
                        break;
                    }
                    case "T2_Hatch": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1200;
                        break;
                    }
                    case "T2_Gate": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1200;
                        break;
                    }
                    /*
                    * Tier 3
                    */
                    case "T3_Door": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1800;
                        break;
                    }
                    case "T3_DoubleDoor": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1800;
                        break;
                    }
                    case "T3_Hatch": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1800;
                        break;
                    }
                    case "T3_Gate": 
                    {
                        if (m_ItemInHands.GetType() == "SledgeHammer")
                            ActionTime = 1800;
                        break;
                    }
                }

                m_ActionData.m_ActionComponent = new CAContinuousTime(ActionTime); 
            }
        }
        else super.CreateActionComponent(); 
    }
};