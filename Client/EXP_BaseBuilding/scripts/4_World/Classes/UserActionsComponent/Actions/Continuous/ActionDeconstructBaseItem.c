/**
 * ActionDeconstructBaseItemOwn.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Wed Apr 07 2021
 * Modified By: SIX
 * 
 * 
 * Notes : Own Territory
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

class ActionDeconstructBaseItemCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{		
        m_ActionData.m_ActionComponent = new CAContinuousTime( 2 );
	}
};

class ActionDeconstructBaseItemOwn: ActionContinuousBase 
{
    void ActionDeconstructBaseItemOwn()
	{
		m_CallbackClass = ActionDeconstructBaseItemCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_DISASSEMBLE;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT;
	}
	
	override void CreateConditionComponents()  
	{	
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTNone;
	}
		
	override string GetText() return "Dismantal Own";

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{   
		if (GetGame().IsServer())
			return true;

		if (vector.Distance(target.GetObject().GetPosition(), player.GetPosition()) < 3)
		{
			if (target.GetObject().IsInherited(TerritoryFlag)) return false;
			if ((target.GetObject().IsInherited(EXP_BaseBuildingObject)) || (target.GetObject().IsInherited(BaseBuildingBase))) 
				return GetTerritoryManagerClient().IsInOwnTerritory();
		}
		return false;
    }

	override void OnFinishProgressServer(ActionData action_data)
	{	
		EXP_BaseBuildingObject BaseItem = EXP_BaseBuildingObject.Cast( action_data.m_Target.GetObject() );
		string objectName = action_data.m_Target.GetObject().GetType();

		if (BaseItem.HasCodelock())
		{
			ItemBase CodelockItem = GetGame().CreateObject( "EXP_Codelock", BaseItem.GetPosition());
			CodelockItem.SetPosition(BaseItem.GetPosition());
		}

		//add damage to tool
		action_data.m_MainItem.DecreaseHealth( 15, false );

		//--- Return Kit
		ItemBase KitReturnItem = GetGame().CreateObject( BaseItem.KitReturn(), action_data.m_Player.GetPosition());
		KitReturnItem.SetPosition(action_data.m_Player.GetPosition());
		KitReturnItem.SetOrientation("0 0 0");

		//--- Delete Object
		GetGame().ObjectDelete(action_data.m_Target.GetObject());

		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
	}
};