/**
 * common.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Fri Aug 27 2021
 * Modified By: SIX
 * 
 * 
 * Notes : N/A
 * 
 * 
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

/*
 * Generate a full date time time stamp 
 * e.g 03/09/2020 21:04:36
 */
static string GenerateDateTimeStamp() {
    return GenerateDateStamp() + " " + GenerateTimeStamp();
}

/*
 * Generate a Date stamp 
 * e.g. 03/09/2020
 */
static string GenerateDateStamp() {

    int year, month, day;
    GetYearMonthDay(year, month, day);

    return "" + year + "/" + month + "/" + day;
}

/*
 * Generate a Time stamp
 * e.g. 21:04:36
 */
static string GenerateTimeStamp() {

    int hour, minute, second;
    GetHourMinuteSecond(hour, minute, second);

    return "" + hour + ":" + minute + ":" + second;
}

/*
 * Generate a unique GUID
 */
static string GenerateGUID() {
    string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    int charsLength = Chars.Length();
    string guid = "";
    for (int i = 0; i < 32; i++)
    {
        int index = Math.RandomInt(0, charsLength);
        string char = Chars.Get(index);
        guid += char;
    }
    return guid;
}

/*
 * Get a PlayerBase object from a uid
 */
static PlayerBase GetPlayerBaseByUID(string uid)
{
    array<Man> players = new array<Man>;
    GetGame().GetPlayers(players);
    for (int i = 0; i < players.Count(); ++i)
    {
        PlayerBase player = PlayerBase.Cast(players[i]);
        if (uid == player.GetIdentity().GetPlainId())
            return player;
    }
    return NULL;
}

/*
 * Get a PlayerIdentity object from a uid
 */
static PlayerIdentity GetPlayerIdentityByUID(string uid)
{
    PlayerBase playerObj = GetPlayerBaseByUID(uid);
    if (playerObj)
        return playerObj.GetIdentity();
    return NULL;
}

static bool ItemIsNearby( string classname, vector center, int radius ) 
{
    array<Object> objects = new array<Object>;
    array<CargoBase> cargos = new array<CargoBase>;

    GetGame().GetObjectsAtPosition(center, radius, objects, cargos);

    foreach (Object obj: objects)
    {
        string objType = obj.GetType();
        if (objType == classname)
            return true;
    }

    return false;
}

static Object GetNearFlagObject( vector center, int radius ) 
{
    array<Object> nearest_objects = new array<Object>;
    array<CargoBase> proxy_cargos = new array<CargoBase>;
    GetGame().GetObjectsAtPosition3D(center, radius, nearest_objects, proxy_cargos);

    for (int i = 0; i < nearest_objects.Count(); i++)
        if (nearest_objects[i].IsInherited(TerritoryFlag)) return nearest_objects[i];
    
    return null; //--- ok?
}

static string GetDisplayName(string itemClass)
{
    string displayName = "";

    if (GetGame().ConfigGetText(CFG_VEHICLESPATH + " " + itemClass + " displayName", displayName))
    {
        displayName = TrimUnt(displayName);
        return displayName;
    }
    else if (GetGame().ConfigGetText(CFG_WEAPONSPATH + " " + itemClass + " displayName", displayName))
    {
        displayName = TrimUnt(displayName);
        return displayName;
    }
    else if (GetGame().ConfigGetText(CFG_MAGAZINESPATH + " " + itemClass + " displayName", displayName))
    {
        displayName = TrimUnt(displayName);
        return displayName;
    }

    return itemClass;
}

/*
    Gets max quantity of specified item from config value
*/
static int GetItemMaxQuantity(string itemClass)
{
    int maxQuantity = 0;

    maxQuantity = GetGame().ConfigGetInt(CFG_MAGAZINESPATH + " " + itemClass + " varQuantityMax");
    if (maxQuantity != 0)
    {
        return maxQuantity;
    }

    maxQuantity = GetGame().ConfigGetInt(CFG_VEHICLESPATH + " " + itemClass + " varQuantityMax");
    if (maxQuantity != 0)
    {
        return maxQuantity;
    }

    maxQuantity = GetGame().ConfigGetInt(CFG_WEAPONSPATH + " " + itemClass + " varQuantityMax");
    if (maxQuantity != 0)
    {
        return maxQuantity;
    }

    return maxQuantity;
}

/*
* Trim bohemias bullshit $UNT$"
*/
static string TrimUnt(string input)
{
    input.Replace("$UNT$", "");
    return input;
}

static int GetPlayerCurrency(PlayerBase player)
{
    int total = 0;
    array<EntityAI> playerItems = new array<EntityAI>;
    player.GetInventory().EnumerateInventory(InventoryTraversalType.INORDER, playerItems);
    
    TStringArray MoneyTypeArray = 
    {
        "BottleCap_7up",
        "BottleCap_Coke",
        "BottleCap_Fanta",
        "BottleCap_Heiniken",
        "Poptab"
    };

    for (int i = 0; i < playerItems.Count(); i++)
    {
        ItemBase itemBase;
        if (Class.CastTo(itemBase, playerItems[i]))
        {
            string itemClassName = itemBase.GetType();
            for (int x = 0; x < MoneyTypeArray.Count(); x++)
            {
                string currency = MoneyTypeArray[x];
                if (currency == itemClassName)
                {
                    float quantity = QuantityConversions.GetItemQuantity(itemBase);
                    total += (1 * quantity);
                }
            }
        }
    }
    return total;
}

/*
    Gets total count of specified item in specified players inventory
    This also takes stack size in to account
*/
static int GetCountOfSpecificItem(PlayerBase player, string itemClass)
{
    itemClass.ToLower();
    int count = 0;
    array<EntityAI> playerItems = new array<EntityAI>;
    player.GetInventory().EnumerateInventory(InventoryTraversalType.PREORDER, playerItems);
    for (int i = 0; i < playerItems.Count(); i++)
    {
        ItemBase tmp;
        if (Class.CastTo(tmp, playerItems[i]))
        {
            string type = tmp.GetType();
            type.ToLower();
            if (type == itemClass)
            {
                float stackQuantity = QuantityConversions.GetItemQuantity(tmp);
                if (stackQuantity > 1)
                    count += stackQuantity;
                else
                    count++;
            }
        }
    }
    return count;
}

/*
    Removes specified quantity of specified item from specified player
    TODO:RE WRITE ... It's fucking bad
*/
static bool RemoveItemQuantity(PlayerBase player, string itemClass, int quantity)
{
    if (GetCountOfSpecificItem(player, itemClass) < quantity) return false;
    int quantityTaken = 0;

    array<EntityAI> playerItems = new array<EntityAI>;
    player.GetInventory().EnumerateInventory(InventoryTraversalType.PREORDER, playerItems);
    for (int i = 0; i < playerItems.Count(); i++)
    {
        ItemBase tmp;
        if (Class.CastTo(tmp, playerItems[i]))
        {
            if (tmp.GetType() == itemClass)
            {
                int stackQuantity = QuantityConversions.GetItemQuantity(tmp);
                int needToTake = quantity - quantityTaken;
                if (stackQuantity >= needToTake)
                {
                    tmp.AddQuantity((quantity * -1));
                    quantityTaken = quantity;
                    if (quantityTaken == quantity) return true;
                }
                else
                {

                    GetGame().ObjectDelete(playerItems[i]);
                    int newQuantityToTake = (quantity - stackQuantity);
                    return RemoveItemQuantity(player, itemClass, newQuantityToTake);
                }
                if (quantityTaken == quantity) return true;
            }
        }
    }
    if (quantityTaken == quantity) return true;
    return false;
}

/*
    Adds specified item of specified quantity to specified player
*/
static ItemBase CreateInInventory(PlayerBase player, string className, int quantity = 1)
{
    if (!player) return NULL; //how

    HumanInventory inventory = player.GetHumanInventory();
    if (!inventory) return NULL; //Again ... how

    int maxQuantity = GetItemMaxQuantity(className);

    if (quantity > maxQuantity && quantity != 1)
    {
        //Call this same function recursively until we are done
        int newQuantityToGive = (quantity - maxQuantity);
        CreateInInventory(player, className, newQuantityToGive);
    }

    ItemBase item;

    item = GetCombinableItem(player, className, quantity);
    if (item)
        return AddQuantity(item, quantity);

    item = inventory.CreateInHands(className);
    //Item is NULL, try creating in inventory instead
    if (!item)
        item = inventory.CreateInInventory(className);
    //Could not create item in inventory, spawn on ground
    if (!item)
    {
        item = player.SpawnEntityOnGroundPos(className, player.GetPosition());
    }
    if (item)
        return SetQuantity(item, quantity);

    //We have not returned yet and we are all out of fucks to give
    return NULL;
}

/*
    Returns first instance found of object of specified class that can be combined with item of same 
    class and of specified quantity 
*/
static ItemBase GetCombinableItem(PlayerBase player, string className, int quantity)
{
    array<ItemBase> combinable = GetCombinableItems(player, className, quantity);
    if (combinable.Count() > 0)
        return combinable[0];
    return NULL;
}

/*
    Returns an array of ItemBase objects of specified class that can be combined Item of same class
    and of specified quantity 
*/
static array<ItemBase> GetCombinableItems(PlayerBase player, string className, int quantity)
{
    array<EntityAI> currentItems = new array<EntityAI>;
    array<ItemBase> returnArr = new array<ItemBase>;
    player.GetInventory().EnumerateInventory(InventoryTraversalType.PREORDER, currentItems);
    for (int i = 0; i < currentItems.Count(); i++)
    {
        ItemBase tmpItem;
        if (Class.CastTo(tmpItem, currentItems[i]))
        {
            if (tmpItem.GetType() == className)
            {
                int maxQuantity = GetItemMaxQuantity(className);
                float currentQuantity = tmpItem.GetQuantity();
                if ((currentQuantity + quantity) <= maxQuantity)
                {
                    returnArr.Insert(tmpItem);
                }
            }
        }
    }
    return returnArr;
}

/*
    Sets quantity of given item to new quantity while checking type
    The only reason it returns the item is for 1 line return statements 
    e.g return SetQuantity(myItem, 50);
*/
static ItemBase SetQuantity(ItemBase item, int quantity)
{
    //--- Is the item a mag
    Magazine mag;
    if (Class.CastTo(mag, item))
    {
        mag.ServerSetAmmoCount(quantity);
        return item;
    }

    item.SetQuantity(quantity);
    return item;
}

/*
    Adds quantity of given item to new quantity while checking type
    The only reason it returns the item is for 1 line return statements 
    e.g return AddQuantity(myItem, 50);
*/
static ItemBase AddQuantity(ItemBase item, int quantity)
{
    //Is the item a mag
    Magazine mag;
    if (Class.CastTo(mag, item)) return SetQuantity(item, (mag.GetAmmoCount() + quantity));
    return SetQuantity(item, (QuantityConversions.GetItemQuantity(item) + quantity));
}




/*array<Man> players = new array<Man>;
GetGame().GetPlayers(players);
for (int i = 0; i < players.Count(); ++i)
{
    PlayerBase player = PlayerBase.Cast(players[i]);
    PlayerIdentity playerIdentity = player.GetIdentity();
    PluginLoadouts Plugin;
    Class.CastTo(Plugin, GetPlugin(PluginLoadouts));
    Plugin.CaptureLoadoutToJson(playerIdentity.GetName(), player);
}*/