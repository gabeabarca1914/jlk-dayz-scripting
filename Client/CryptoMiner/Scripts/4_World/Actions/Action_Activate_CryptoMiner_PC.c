class Action_Activate_CryptoMiner_PC: ActionInteractBase
{
	CryptoMiner_PC pc;
	
	override string GetText()
	{
		return "Activate CryptoMiner";
	}
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( !CastTo( pc, target.GetObject() ) )		return false;
		if ( pc.IsRuined() )							return false;
		if ( !pc.HasAllComponents()  )					return false;
		if ( !pc.IsBeingPowered()  )					return false;
		
		return true;
	}
	
	override void OnExecuteServer( ActionData action_data )
	{
		pc.Activate();
	}
};