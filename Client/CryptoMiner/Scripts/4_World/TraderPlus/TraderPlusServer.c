// !!! This should be moved to serverside
modded class TraderPlusServer
{
	override void TradeRequest( TraderPlusProduct product )
    {
		string crypto = product.ClassName
		
		// Check for valid player and cryptodrive
		if( !product.Customer || !crypto.Contains( "CryptoMiner_CrypoDrive_" ) )
		{
			super.TradeRequest( product );
			return;
		}
		
		// Update to most recent prices
		CryptoPricesServer.UpdatePrices();
		
		// Get the price of the crypto being bought
		crypto.Replace( "CryptoMiner_CrypoDrive_", "" );
		product.Price = CryptoPricesServer.GetPrice( crypto ) * product.Quantity;
		
		// Check Buy or Sell
		string tradeType = "Sell";
		if( !product.TradMode )	tradeType = "Buy";
		
		// Setup POST request to discord webhook
		RestCallback cbx = new RestCallback;
		RestContext ctx = GetRestApi().GetRestContext( CryptoPricesServer.post_url );
		ctx.SetHeader( "application/json" );
		
		// Send the message
		string playerID		= product.Customer.GetIdentity().GetPlainId();
		string playerName	= product.Customer.GetIdentity().GetName();
		string qtyBought	= product.Quantity.ToString();
		string messageToSend = "(" + playerID + ")" + playerName + " " + qtyBought + " " + crypto + " " + tradeType;
		ctx.POST( cbx, "", "{\"content\": \"" + messageToSend + "\"}" );
		
		// Execute the super function with updated product price
		super.TradeRequest( product );
	}
}