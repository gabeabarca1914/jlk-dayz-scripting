modded class TraderPlusMenu
{
	void TraderPlusMenu()
	{
		GetGame().RPCSingleParam( GetGame().GetPlayer(), -4206969, new Param1<bool>( true ), true, GetGame().GetPlayer().GetIdentity() );
	}
	
	override void OnShow()
	{
		super.OnShow();
		
		GetGame().RPCSingleParam( GetGame().GetPlayer(), -4206969, new Param1<bool>( true ), true, GetGame().GetPlayer().GetIdentity() );
	}
	
	override void OnHide()
	{
		super.OnHide();
		
		GetGame().RPCSingleParam( GetGame().GetPlayer(), -4206969, new Param1<bool>( false ), true, GetGame().GetPlayer().GetIdentity() );
	}
	
	override int CalculatePriceForThatItem( bool trademode, string classname, int stockqty, int state, out int maxstock, out int tradeqty )
	{
		int superPrice = super.CalculatePriceForThatItem( trademode, classname, stockqty, state, maxstock, tradeqty );
		
		string cryptoDrivePrefix = "CryptoMiner_CrypoDrive_";
		classname.ToLower();
		cryptoDrivePrefix.ToLower();
		
		if ( !superPrice || !classname.Contains( cryptoDrivePrefix ) )	return superPrice;
		
		string crypto = classname;
		crypto.Replace( cryptoDrivePrefix, "" );
		crypto.ToUpper();
		
		return CryptoPricesClient.GetPrice( GetGame().GetPlayer(), crypto ) * m_QtyMultiplier;
	}
}