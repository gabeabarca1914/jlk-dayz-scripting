class CryptoGetCallback: RestCallback
{
	override void OnSuccess( string data, int dataSize )
	{
		if ( data == "" )	return;
		
		CryptoPricesServer._UpdatePrices( data );
	}
}

class CryptoPricesServer
{
	static const string get_url  = "https://gitlab.com/api/v4/projects/29068670/repository/files/CryptoPrices.c/raw?ref=main&access_token=XxrcyNjojAEyrEUz-CQY";
	static const string post_url = "https://discord.com/api/webhooks/885943742215905320/NpKQh2EKVH7ptCfb-1kbRiaQf6ceoyPahv1NLiBg4OapYdY6HMF_fxzTY9zsZCFkjyk-";
	
	static ref TStringIntMap mapCryptoPrices;
	static bool m_shouldSync;
	static PlayerBase m_plr;
	
	static void UpdatePrices( bool shouldSync = false, PlayerBase PlrToSyncTo = null )
	{
		// Update static vars
		m_shouldSync = shouldSync;
		m_plr = PlrToSyncTo;
		
		// Get contents of gitlab file
		CryptoGetCallback cbx = new CryptoGetCallback;
		RestContext ctx = GetRestApi().GetRestContext( get_url );
		ctx.GET( cbx, "" );
	}
	
	static void _UpdatePrices( string UpdatedPrices )
	{
		// Make a new map or clear the old map
		if ( mapCryptoPrices )
			mapCryptoPrices.Clear();
		else
			mapCryptoPrices = new TStringIntMap;
		
		// Split contents into lines
		auto TstrContents = new TStringArray;
		UpdatedPrices.Split( "\n", TstrContents );
		
		// Search each line for a crypto value
		foreach ( string lineContents: TstrContents )
		{
			// Replace unnecessary chars, leaving only the crypto and the value
			if ( lineContents.Contains( " " ) )		lineContents.Replace( " ", "" );
			if ( lineContents.Contains( "\t" ) )	lineContents.Replace( "\t", "" );
			if ( lineContents.Contains( ";" ) )		lineContents.Replace( ";", "" );
			
			// Split into crypto name and value, then add to the map
			auto TlineContents = new TStringArray;
			lineContents.Split( "=", TlineContents );
			mapCryptoPrices.Insert( TlineContents[0], TlineContents[1].ToInt() );
		}
		
		// Check if should sync
		if ( !m_shouldSync )	return;
		
		// Null pointer check
		if ( !mapCryptoPrices )	return;
		
		// Send the updated prices to the client
		Param1<TStringIntMap> param = new Param1<TStringIntMap>( mapCryptoPrices );
		GetGame().RPCSingleParam( m_plr, -4206969, param, true, m_plr.GetIdentity() );
	}
	
	static int GetPrice( string CryptoToGetPriceOf )
	{
		if ( !mapCryptoPrices )
			return 0;
		
		for ( int i = 0; i < mapCryptoPrices.Count(); i++ )
		{
			string key = mapCryptoPrices.GetKey( i );
			if ( key == CryptoToGetPriceOf ) 
			{
				return mapCryptoPrices.Get( key );
			}
		}
		
		return 0;
	}
}

class CryptoPricesClient
{
	static int GetPrice( PlayerBase plr, string CryptoToGetPriceOf )
	{
		if ( !plr.mapCryptoPrices )
		{
			return 0;
		}
		
		for ( int i = 0; i < plr.mapCryptoPrices.Count(); i++ )
		{
			string key = plr.mapCryptoPrices.GetKey( i );
			if ( key == CryptoToGetPriceOf ) 
			{
				return plr.mapCryptoPrices.Get( key );
			}
		}
		
		return 0;
	}
}