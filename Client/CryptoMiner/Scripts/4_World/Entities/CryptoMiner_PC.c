class CryptoMiner_PC: ItemBase
{
	const float MiningTickInterval = 10; // in seconds ( the same crypto gains will be calulated, this only determines how often it will be updated )
	const float CarBattery_BatteryLife = 3 * 3600; // in seconds ( Currently 3 hours )
	const float TruckBattery_BatteryLife = 5 * 3600; // in seconds ( Currently 5 hours )
	// At max, the generator can run for 35714 seconds
	
	ItemBase Motherboard;
	ItemBase Power_Supply;
	ItemBase Processing_Unit;
	ItemBase Graphics_Card;
	ItemBase RAM_Stick;
	CryptoMiner_CrypoDrive_Base CrypoDrive;
	ItemBase ClosestPowerSource;
	
	ref Timer timerMiningTick;
	
	void CryptoMiner_PC()	{}
	
	void ~CryptoMiner_PC()	{}
	
	bool HasAllComponents()
	{
		if ( !CastTo( Motherboard, FindAttachmentBySlotName( "Motherboard" ) ) )			return false;
		if ( !CastTo( Power_Supply, FindAttachmentBySlotName( "Power_Supply" ) ) )			return false;
		if ( !CastTo( Processing_Unit, FindAttachmentBySlotName( "Processing_Unit" ) ) )	return false;
		if ( !CastTo( Graphics_Card, FindAttachmentBySlotName( "Graphics_Card" ) ) )		return false;
		if ( !CastTo( RAM_Stick, FindAttachmentBySlotName( "RAM_Stick" ) ) )				return false;
		if ( !CastTo( CrypoDrive, FindAttachmentBySlotName( "CrypoDrive" ) ) )				return false;
		
		return true;
	}
	
	bool IsBeingPowered()
	{
		/////EXP_BaseBuilding/////
		//EXPTerritory territory = GetTerritoryManagerServer().GetNearestTerritory( GetPosition() );
		//vector territoryPosition = territory.GetPosition();
		//float territoryRadius = territory.GetLevel().GetRadius();
		
		vector territoryPosition = "0 0 0";
		float territoryRadius = 1;
		
		// Get car/truck battery or generator ( TBD ) within the nearest territory!
		array<Object> PowerSources = GetPowerSources( territoryPosition, territoryRadius );
		
		// Clear if there was a power source previously
		ClosestPowerSource = NULL;
		
		// If there were no power sources nearby
		if ( !PowerSources.Count() )	return false;
		
		// If there the closest power source coundn't cast
		if ( !CastTo( ClosestPowerSource, PowerSources[0] ) )	return false;
		
		// This pc should be successfully powered
		return true;
	}
	
	bool IsPowerSourceFullyDepleted( Object obj )
	{
		ItemBase ib;
			
		if ( !CastTo( ib, obj ) )				return false;
		if ( !ib.HasEnergyManager() )			return false;
		if ( !ib.GetCompEM().CanWork() )		return false;
		
		if ( obj.IsKindOf( "Power_Generator" ) )
		{
			if ( !ib.GetCompEM().IsSwitchedOn() )			return false;
		}
		
		return true;
	}
	
	void DepletePowerSource()
	{
		// TruckBattery has 1500 max energy
		// CarBattery has 500 max energy
		// Generator has 10000 max energy
		
		if ( ClosestPowerSource.IsKindOf( "Power_Generator" ) )
		{
			// Do nothing, generator power is automatically depleted over time
			return;
		}
		
		ComponentEnergyManager em = ClosestPowerSource.GetCompEM();
		
		float BatteryLossPerTick;
		
		if ( ClosestPowerSource.IsKindOf( "CarBattery" ) || ClosestPowerSource.IsInherited( CarBattery ) )
			BatteryLossPerTick = ( MiningTickInterval * CarBattery_BatteryLife ) / em.GetEnergyMax();
		
		if ( ClosestPowerSource.IsKindOf( "TruckBattery" ) || ClosestPowerSource.IsInherited( TruckBattery ) )
			BatteryLossPerTick = ( MiningTickInterval * TruckBattery_BatteryLife ) / em.GetEnergyMax();
		
		em.AddEnergy( BatteryLossPerTick );
	}
	
	void OnMiningTick()
	{
		if ( !HasAllComponents() )
			Deactivate();
			return;
			
		if ( !IsBeingPowered() )
			Deactivate();
			return;
			
		CrypoDrive.OnMiningTick();
		
		DepletePowerSource();
	}
	
	void Activate()
	{
		// ( Loop ) Every <MiningTickInterval> seconds, call OnMiningTick
		timerMiningTick = new Timer;
		timerMiningTick.Run( MiningTickInterval, this, "OnMiningTick", NULL, true );
	}
	
	void Deactivate()
	{
		// Stop the timer
		timerMiningTick.Stop();
		timerMiningTick = NULL;
	}
	
	array<Object> GetPowerSources( vector position, float radius )
	{
		array<Object> objects = new array<Object>();
		array<CargoBase> proxyCargos = new array<CargoBase>();
		
		GetGame().GetObjectsAtPosition3D( position, radius, objects, proxyCargos );
		
		array<Object> PowerSources = new array<Object>();
		
		// Determine which objects will be considered as "Power Sources"
		foreach ( Object obj: objects )
		{
			// Skip this object if it's out of power
			if ( !IsPowerSourceFullyDepleted( obj ) )	continue;
			
			// Keep in mind, IsKindOf means inherited thru config while IsInherited means inherited thru scripts
			if ( obj.IsKindOf( "CarBattery" ) || obj.IsInherited( CarBattery ) )		PowerSources.Insert( obj );
			if ( obj.IsKindOf( "TruckBattery" ) || obj.IsInherited( TruckBattery ) )	PowerSources.Insert( obj );
			if ( obj.IsKindOf( "Power_Generator" ) )									PowerSources.Insert( obj );
		}
		
		return PowerSources;
	}
	
	override void EEItemAttached ( EntityAI item, string slot_name )
	{
		super.EEItemAttached ( item, slot_name );
		
		// Not synced to server method ( will only play for the player who attached something )
		if ( GetGame().IsClient() )	SEffectManager.PlaySound( "sparkplug_attach_SoundSet", GetPosition() );
	}
	
	override void EEItemDetached ( EntityAI item, string slot_name )
	{
		super.EEItemDetached ( item, slot_name );
		
		// Not synced to server method ( will only play for the player who detached something )
		if ( GetGame().IsClient() )	SEffectManager.PlaySound( "sparkplug_detach_SoundSet", GetPosition() );
	}
	
	override bool CanPutInCargo( EntityAI parent )
    {
        if ( HasAnyAttachments() )	return false;
		
        return super.CanPutInCargo( parent );
    }
	
	override bool CanReceiveAttachment( EntityAI attachment, int slotId )
	{
		// Can't put in attachments when the player has it ( in their hands )
		if ( GetHierarchyParent() && GetHierarchyParent().IsInherited( PlayerBase ) )	return false;
		
		return super.CanReceiveAttachment( attachment, slotId );
	}
	
    override bool CanPutIntoHands(EntityAI parent)
    {
        if ( HasAnyAttachments() )	return false;
		
        return super.CanPutIntoHands( parent );
    }
	
	override string GetDisplayName()
    {
        if ( timerMiningTick && timerMiningTick.IsRunning() )
            return ConfigGetString("displayName") + " ( RUNNING )";
		
		return ConfigGetString("displayName");
    }
	
    override string GetTooltip()
    {
        if ( timerMiningTick && timerMiningTick.IsRunning() )
            return ConfigGetString("descriptionShort") + " ( RUNNING )";
		
		return ConfigGetString("descriptionShort");
    }
}