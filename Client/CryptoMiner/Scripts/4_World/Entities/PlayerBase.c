modded class PlayerBase
{
	/*	SyncCryptoPricesInterval
		In seconds, the interval at which the server will check for, then send updated crypto prices to the client
		For performance efficiency, this will only happen with one player, and only while they have the trader menu open
	*/
	static const float SyncCryptoPricesInterval = 10;
	
	ref TStringIntMap mapCryptoPrices;
	bool shouldSyncPrices;
	
	float counterSyncCryptoPrices = SyncCryptoPricesInterval;
	override void OnScheduledTick( float deltaTime )
	{
		super.OnScheduledTick( deltaTime );
		
		if ( GetGame().IsServer() )
		{
			if ( !shouldSyncPrices )	return;
			
			counterSyncCryptoPrices += deltaTime;
			
			if ( counterSyncCryptoPrices < SyncCryptoPricesInterval ) return;
			
			CryptoPricesServer.UpdatePrices( true, this );
			
			counterSyncCryptoPrices = 0;
		}
	}
	
 	override void SetActions() 
	{
		super.SetActions();
		
		AddAction( Action_Activate_CryptoMiner_PC );
	}
	
	override void OnRPC( PlayerIdentity sender, int rpc_type, ParamsReadContext ctx )
	{
		super.OnRPC( sender, rpc_type, ctx );
		
		if ( rpc_type != -4206969 )		return;
		
		if ( GetGame().IsClient() )
		{
			Param1<TStringIntMap> paramMapCryptoPrices(null);
			if ( ctx.Read( paramMapCryptoPrices ) )
			{
				if ( mapCryptoPrices )
					mapCryptoPrices.Clear();
				else
					mapCryptoPrices = new TStringIntMap;
				
				mapCryptoPrices.Copy( paramMapCryptoPrices.param1 );
			}
		}
		
		if ( GetGame().IsServer() )
		{
			Param1<bool> paramShouldSyncPrices;
			if ( ctx.Read( paramShouldSyncPrices ) )
			{
				shouldSyncPrices = paramShouldSyncPrices.param1;
			}
		}
	}
}