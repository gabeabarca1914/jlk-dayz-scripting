class CryptoMiner_CrypoDrive_Base: ItemBase
{
	/* You cant split or combine flashdrives, but maybe it would provide some QOL for players
	override bool CanBeSplit()
	{
		return false;
	}
	
	override bool CanBeCombined( EntityAI other_item, bool reservation_check = true, bool stack_max_limit = false )
	{
		return false;
	}
	*/

	override string GetDisplayName()
    {
		string crypto = GetType();
		crypto.Replace( "CryptoMiner_CrypoDrive_", "" )
        return crypto + " " + ConfigGetString("displayName");
    }
	
    override string GetTooltip()
    {
		string crypto = GetType();
		crypto.Replace( "CryptoMiner_CrypoDrive_", "" )
        return ConfigGetString("descriptionShort") + GetQuantity().ToString() + " " + crypto;
    }
	
	// While the pc is activated, this is called every 10 seconds
	void OnMiningTick()
	{
		AddQuantity( 1 ); // Will change this value later, could end up based on power type, crypo type, ect
	}
}