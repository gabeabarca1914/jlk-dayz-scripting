/**
* config.cpp
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

class CfgPatches
{
	class CryptoMiner_Scripts
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Scripts",
			"TraderPlus_Script"
		};
	};
};

class CfgMods
{
	class CryptoMiner
	{
		name = "CryptoMiner";
		dir = "CryptoMiner";
		author = "6IX";
		type = "mod";
		hideName = 1;
		hidePicture = 1;
		dependencies[] = { "Game", "World", "Mission" };
		class defs
		{
			class gameScriptModule
			{
				value = "";
				files[] = { "CryptoMiner/Scripts/3_Game" };
			};
			
			class worldScriptModule
			{
				value = "";
				files[] = { "CryptoMiner/Scripts/4_World" };
			};
			
			class missionScriptModule
			{
				value = "";
				files[] = { "CryptoMiner/Scripts/5_Mission" };
			};
		};
	};
};

class CfgVehicles
{
	class Inventory_Base;
	
	class CryptoMiner_PC: Inventory_Base
	{
		scope=2;
		model="\dz\gear\food\apple.p3d";
		displayName="Crypto Miner PC";
		descriptionShort="Assemble the necessary components to automate tasks.";
		attachments[]=
		{
			"Motherboard",
			"Power_Supply",
			"Processing_Unit",
			"Graphics_Card",
			"RAM_Stick",
			"CrypoDrive"
		};
	};
	
	class CryptoMiner_Motherboard: Inventory_Base
	{
		scope=2;
		model="\dz\gear\food\apple.p3d";
		displayName="PC Motherboard";
		descriptionShort="Can be assembled with other components to automate tasks.";
		inventorySlot[]=
		{
			"Motherboard"
		};
	};
	
	class CryptoMiner_Power_Supply: Inventory_Base
	{
		scope=2;
		model="\dz\gear\food\apple.p3d";
		displayName="PC Power Supply";
		descriptionShort="Can be assembled with other components to automate tasks.";
		inventorySlot[]=
		{
			"Power_Supply"
		};
	};
	
	class CryptoMiner_Processing_Unit: Inventory_Base
	{
		scope=2;
		model="\dz\gear\food\apple.p3d";
		displayName="PC Core Processing Unit";
		descriptionShort="Can be assembled with other components to automate tasks.";
		inventorySlot[]=
		{
			"Processing_Unit"
		};
	};
	
	class CryptoMiner_Graphics_Card: Inventory_Base
	{
		scope=2;
		model="\dz\gear\food\apple.p3d";
		displayName="PC Graphical Processing Unit";
		descriptionShort="Can be assembled with other components to automate tasks.";
		inventorySlot[]=
		{
			"Graphics_Card"
		};
	};
	
	class CryptoMiner_RAM_Stick: Inventory_Base
	{
		scope=2;
		model="\dz\gear\food\apple.p3d";
		displayName="PC Random Access Memory";
		descriptionShort="Can be assembled with other components to automate tasks.";
		inventorySlot[]=
		{
			"RAM_Stick"
		};
	};
	
	class CryptoMiner_CrypoDrive_Base: Inventory_Base
	{
		scope=0;
		stackedUnit="bts";
		varQuantityDestroyOnMin=1;
		varQuantityInit=1;
		varQuantityMin=0;
		varQuantityMax=911; // biggest number with the smallest width
		model="\dz\gear\food\apple.p3d";
		displayName="CrypoDrive";
		// This will be updated in scripts to contain a certain currency and amount
		descriptionShort="USB Drive containing cryptocurrency, this one contains ";
		inventorySlot[]=
		{
			"CrypoDrive"
		};
	};
	
	class CryptoMiner_CrypoDrive_BTC: CryptoMiner_CrypoDrive_Base	{ scope=2; };
	class CryptoMiner_CrypoDrive_ETH: CryptoMiner_CrypoDrive_Base	{ scope=2; };
	class CryptoMiner_CrypoDrive_LTC: CryptoMiner_CrypoDrive_Base	{ scope=2; };
	class CryptoMiner_CrypoDrive_DOGE: CryptoMiner_CrypoDrive_Base	{ scope=2; };
	class CryptoMiner_CrypoDrive_XRP: CryptoMiner_CrypoDrive_Base	{ scope=2; };
	class CryptoMiner_CrypoDrive_DZC: CryptoMiner_CrypoDrive_Base	{ scope=2; };
};

class CfgSlots
{
	class Slot_Motherboard
	{
		name="Motherboard";
		displayName="Motherboard";
	};
	class Slot_Power_Supply
	{
		name="Power_Supply";
		displayName="Power Supply";
	};
	class Slot_Processing_Unit
	{
		name="Processing_Unit";
		displayName="CPU";
	};
	class Slot_Graphics_Card
	{
		name="Graphics_Card";
		displayName="GPU";
	};
	class Slot_RAM_Stick
	{
		name="RAM_Stick";
		displayName="RAM";
	};
	class Slot_CrypoDrive
	{
		name="CrypoDrive";
		displayName="CrypoDrive";
	};
};