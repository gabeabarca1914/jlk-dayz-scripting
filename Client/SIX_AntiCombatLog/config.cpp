/**
* config.cpp
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

class CfgPatches
{
	class EXP_AntiCombatLog
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Scripts"
		};
	};
};

class CfgMods
{
	class EXP_AntiCombatLog
	{
		name = "The EXP Servers Mod";
		dir = "EXP_AntiCombatLog";
		hideName = 1;
		hidePicture = 1;
        creditsJson = "EXP_AntiCombatLog/Scripts/Credits.json";
		author = "6IX";
		type = "mod";
		dependencies[] = { "World", "Mission" };
		class defs
		{
			class worldScriptModule
			{
				value = "";
				files[] = { "EXP_AntiCombatLog/Scripts/4_World" };
			};

			class missionScriptModule
			{
				value = "";
				files[] = { "EXP_AntiCombatLog/Scripts/5_Mission" };
			};
		};
	};
};