/**
* InGameMenu.c
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

modded class InGameMenu extends UIScriptedMenu
{
    protected bool playerIsInCombat;
    protected int timeLeft;
    protected string str;
    protected PlayerBase m_Player; 

    override void OnClick_Exit() 
    {   

        Man manPlayer = GetGame().GetPlayer();
        PlayerBase player = PlayerBase.Cast( manPlayer );

        if ( !manPlayer ) return;

        playerIsInCombat = player.IsInCombat();
        
        if ( playerIsInCombat && player.IsAlive()) 
        {
            GetGame().GetUIManager().Back();
            GetGame().GetMission().PlayerControlEnable(true);
            
            if ( player.HasNotifiedCombatLog() ) 
            {
                timeLeft = player.calculateTimeLeftCombatLog();

                if (timeLeft >= 60)
                    str = string.Format("You have %1 minute(s) left till you can log out!", (timeLeft / 60));
                else if (timeLeft < 60)
                    str = string.Format("You have %1 second(s) left till you can log out!", timeLeft);

                NotificationSystem.AddNotificationExtended( 3.5, "SIX Anti-Combatlog System", str, "" );

                player.MessageImportant( "SIX Anti-Combatlog System - " + str );
                return;
            } 
            else 
            {
                str = "You're in Combat. You cannot disconnect!";
                NotificationSystem.AddNotificationExtended( 3.5, "SIX Anti-Combatlog System", str, "" );

                //--- Red No Indentifer
                player.MessageImportant( "SIX Anti-Combatlog System - " + str );

                player.setNotifiedCombatLog(true);
                return;
            }
        }

        super.OnClick_Exit()
    }
};