/**
* MissionGameplay.c
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/
modded class MissionGameplay
{
	protected float 		combatLastTime;
	protected bool  	 	playerIsInCombat;			
	
    protected Man   	 	player;
    protected PlayerBase 	playerPB;

    override void OnUpdate( float timeslice ) {
		super.OnUpdate( timeslice );

		player 				= GetGame().GetPlayer();
		playerPB 			= PlayerBase.Cast( player );

		if ( !playerPB || !player ) return;

		playerIsInCombat 	= playerPB.IsInCombat();
		combatLastTime 		= playerPB.lastTimeInCombat();

		if ( player ) {
			if ( playerIsInCombat ) {
				//--- Get difference from current time and last logged combat time.
				if ( ( ( playerPB.getCurrentTime() / 1000 ) - ( combatLastTime / 1000 ) ) >= 300 ) {
					//--- Remove Combat state
					playerPB.setPlayerInCombat(false);
					playerPB.setNotifiedCombatLog(false);
				}
			}
		}
	}
};