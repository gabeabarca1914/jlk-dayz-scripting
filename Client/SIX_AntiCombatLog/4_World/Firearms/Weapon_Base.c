/**
* Weapon_Base.c
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : SIX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

modded class Weapon_Base
{
    override void EEFired ( int muzzleType, int mode, string ammoType ) 
    {
		PlayerBase shooter = PlayerBase.Cast( GetHierarchyRootPlayer() );
        
        shooter.setPlayerInCombat(true);
        shooter.setCombatTimer();

        //--- Get everyone around the player and set them in combat
        array<Object> nearest_objects = new array<Object>;
		array<CargoBase> proxy_cargos = new array<CargoBase>;
		GetGame().GetObjectsAtPosition3D( shooter.GetPosition(), 20, nearest_objects, proxy_cargos );

        foreach (Object selectedObject: nearest_objects)
        {
            PlayerBase NearPlayer;
            if (selectedObject && Class.CastTo(NearPlayer, selectedObject) && NearPlayer.IsAlive())
            {
                NearPlayer.setPlayerInCombat(true);
                NearPlayer.setCombatTimer();
            }
        }

        super.EEFired( muzzleType, mode, ammoType );
    }
};