/**
* PlayerBase.c
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

modded class PlayerBase extends ManBase
{
    protected float                     six_lastCombatTime, six_GetCurrentTime;
    protected bool                      six_IsInCombat, combatLogNotificationShown;
    protected int                       timeLeft, distanceToTrader;

    override void Init() {
        super.Init();
                
        six_IsInCombat              = false;
        combatLogNotificationShown  = false;

        RegisterNetSyncVariableBool( "six_IsInCombat" );
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).setPlayerInCombat( true );
        Params : Bool
        Returns : N/A
    */
    //--- Allow functions to set combat state to player
    void setPlayerInCombat( bool state ) {
        six_IsInCombat = state;
        SetSynchDirty();
        
        /*
        if ( distanceToTrader < 750 && TraderCombatWaringTimer() ) {
            lastTimeTraderCombatWarning();
            //NotificationSystem.AddNotificationExtended( 5, "Safezone Combat Warning", "WARNING! " + playerName + ", It Is Against The Rules To Initiatate Combat Near The Trader!", "" );
            //GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.callThisLater, 6.5 * 1000, false );
        }*/
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).IsInCombat();
        Params : N/A
        Returns : Bool
    */
    //--- Get variable from player for if player is in combat
    bool IsInCombat() {
        //--- Functions returns this variable when called.
        return six_IsInCombat;
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).setNotifiedCombatLog();
        Params : Bool
        Returns : N/A
    */
    void setNotifiedCombatLog( bool value ) {
        combatLogNotificationShown = value;
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).HasNotifiedCombatLog();
        Params : N/A
        Returns : Bool
    */
    bool HasNotifiedCombatLog() {
        return combatLogNotificationShown;
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).setCombatTimer();
        Params : N/A
        Returns : N/A
    */
    //--- Updates 'six_lastCombatTime' with new current time
    void setCombatTimer() {
        float time = GetGame().GetTime();
        six_lastCombatTime = time;
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).getCurrentTime();
        Params : N/A
        Returns : float
    */
    //--- Returns the current time
    float getCurrentTime() {
        float six_GetCurrentTime = GetGame().GetTime();
        return six_GetCurrentTime;
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).lastTimeInCombat();
        Params : N/A
        Returns : float
    */
    //--- Returns 'six_lastCombatTime' as set by 'setCombatTimer' 
    float lastTimeInCombat() {
        return six_lastCombatTime;
    }

    /* 
        Example : PlayerBase.Cast( GetGame().GetPlayer() ).calculateTimeLeftCombatLog();
        Params : N/A
        Returns : int
    */
    //--- Returns 'timeLeft' as set by 'lastTimeInCombat' 
    int calculateTimeLeftCombatLog() 
    {
        timeLeft = ( 300 - ( ( getCurrentTime() / 1000 ) - ( lastTimeInCombat() / 1000 ) ));
        return timeLeft;
    }

    override void EEHitBy( TotalDamageResult damageResult, int damageType, EntityAI source, int component, string dmgZone, string ammo, vector modelPos, float speedCoef ) {
        //--- Only process this if the player is alive, otherwise skip
        if (!IsAlive()) return;
        
        setPlayerInCombat(true);
        setCombatTimer();

        //--- Get everyone around the player and set them in combat
        array<Object> nearest_objects = new array<Object>;
		array<CargoBase> proxy_cargos = new array<CargoBase>;
		GetGame().GetObjectsAtPosition3D( GetPosition(), 20, nearest_objects, proxy_cargos );

        foreach (Object selectedObject: nearest_objects)
        {
            PlayerBase NearPlayer;
            if (selectedObject && Class.CastTo(NearPlayer, selectedObject) && NearPlayer.IsAlive())
            {
                NearPlayer.setPlayerInCombat(true);
                NearPlayer.setCombatTimer();
            }
        }

		super.EEHitBy(damageResult, damageType, source, component, dmgZone, ammo, modelPos, speedCoef); 
	}
};