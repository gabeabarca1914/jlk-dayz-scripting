/**
 * BasicMapController.c
 * 
 * SIX
 * www.expgaming.net
 * © 2020 - 2021 EXP Gaming Community
 * 
 * Last Modified: Mon Jan 04 2021
 * Modified By: SIX
 * 
 * 
 * Notes : 
 * 
 * RPC ID : RoamingTraderRPCs
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
 * This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content without written permission from the content author (6IX). 
 * Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
 */

modded class BasicMapController 
{
    static string RoamingTrader_Key = "BASICMAP_KEY";
    ref TBasicMapMarkerArray RoamingTraderMarkers = new TBasicMapMarkerArray;

    override void Init()
    {
        super.Init();
        
		GetRPCManager().AddRPC("RoamingTraderRPCs", "RPCSyncRoamingTraderData", this, SingeplayerExecutionType.Both);
		RegisterGroup(RoamingTrader_Key, new ref BasicMapGroupMetaData(RoamingTrader_Key, "Roaming Traders"), NULL);

		if (GetGame().IsMultiplayer() && GetGame().IsClient())
			GetRPCManager().SendRPC("RoamingTraderRPCs", "RPCSyncRoamingTraderData", new Param1<TBasicMapMarkerArray>(NULL), true, NULL);
    }

    void RPCSyncRoamingTraderData(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
		Param1<TBasicMapMarkerArray> data;

		if (!ctx.Read(data))
            return;
        
        if (GetGame().IsMultiplayer() && GetGame().IsClient())
        {
			RoamingTraderMarkers = data.param1;

			for (int i = 0; i < RoamingTraderMarkers.Count(); i++)
            {
				RoamingTraderMarkers.Get(i).SetCanEdit(false);
				RoamingTraderMarkers.Get(i).SetGroup(RoamingTrader_Key);
			}

			Markers.Set(RoamingTrader_Key, RoamingTraderMarkers);
		}
        
        if (GetGame().IsMultiplayer() && GetGame().IsServer())
			GetRPCManager().SendRPC("RoamingTraderRPCs", "RPCSyncRoamingTraderData", new Param1<TBasicMapMarkerArray>(RoamingTraderMarkers), true, sender);
	}

    void AddRoamingTraderMarker(ref BasicMapMarker marker)
    {
        if (GetGame().IsServer())
        {
            RoamingTraderMarkers.Insert(marker);

            for ( int i = 0; i < RoamingTraderMarkers.Count(); i++)
            {
                vector pos = RoamingTraderMarkers.Get(i).GetPosition();

                if ( pos[1] == 0)
                {
                    pos[1] = GetGame().SurfaceY( pos[0], pos[2] );
                    RoamingTraderMarkers.Get(i).SetPosition(pos);
                }

                RoamingTraderMarkers.Get(i).SetCanEdit(false);
                RoamingTraderMarkers.Get(i).SetGroup(RoamingTrader_Key);
            }

            Markers.Set(RoamingTrader_Key, RoamingTraderMarkers);

            GetRPCManager().SendRPC("RoamingTraderRPCs", "RPCSyncRoamingTraderData", new Param1<TBasicMapMarkerArray>(RoamingTraderMarkers), true);
        }
    }

    void RemoveRoamingTraderMarker(ref BasicMapMarker marker)
    {
        if (GetGame().IsServer())
        {
            RoamingTraderMarkers.RemoveOrdered(RoamingTraderMarkers.Find(marker));
            RemoveMarker(marker);

            GetRPCManager().SendRPC("RoamingTraderRPCs", "RPCSyncRoamingTraderData", new Param1<TBasicMapMarkerArray>(RoamingTraderMarkers), true);
        }
    }
};