/**
* config.cpp
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

class CfgPatches
{
	class EXP_RoamingTraderClient {
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]= {
			"DZ_Scripts",
			"DZ_Data",
			"DZ_Weapons_Lights",
			"BasicMap"
		};
	};
};

class CfgMods
{
	class EXP_RoamingTraderClient {
		dir = "EXP_RoamingTraderClient";
		picture = "";
		action = "";
		hideName = 1;
		hidePicture = 1;
		name = "The EXP Servers Mod";
		credits = "0";
        creditsJson = "EXP_RoamingTraderClient/Scripts/Credits.json";
		author = "6IX";
		authorID = "0"; 
		version = "1.0"; 
		extra = 0;
		type = "mod";
		dependencies[] = { "Game", "World"};
		class defs {
			class gameScriptModule {
				value = "";
				files[] = { "EXP_RoamingTraderClient/Scripts/3_Game" };
			};

			class worldScriptModule {
				value = "";
				files[] = { "EXP_RoamingTraderClient/Scripts/4_World" };
			};
		};
	};
};