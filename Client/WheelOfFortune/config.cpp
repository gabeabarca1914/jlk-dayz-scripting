/**
* config.cpp
*
* 6IX
* https://steamcommunity.com/id/6IX6OD/
* © 2019 - 2020 6IX Development
*
* Author : 6IX
*
*
* Notes : N/A
*
*
* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/.
* This work is property of The EXP Development. You do not have permissions to edit/distribute any of this content
* without written permission from the content author (6IX). 
* Failure to adhere to the content license(s) above will result in actions from Bohemia Interactive Studios and/or Valve.
*/

class CfgPatches
{
	class WheelOfFortune
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Scripts"
		};
	};
};

class CfgMods
{
	class WheelOfFortune
	{
		name = "WheelOfFortune";
		dir = "WheelOfFortune";
		author = "6IX";
		type = "mod";
		hideName = 1;
		hidePicture = 1;
		dependencies[] = { "World", "Mission" };
		class defs
		{
			class worldScriptModule
			{
				value = "";
				files[] = { "WheelOfFortune/Scripts/4_World" };
			};
			
			class missionScriptModule
			{
				value = "";
				files[] = { "WheelOfFortune/Scripts/5_Mission" };
			};
		};
	};
};

class cfgVehicles
{
	//class HouseNoDestruct;
	class Inventory_Base;
	
	//class Land_WheelOfFortune: HouseNoDestruct
	class WheelOfFortune: Inventory_Base
	{
		scope=2;
		displayName="Wheel Of Fortune";
		descriptionShort="Place your bet in a nearby Betting Drop Box to play!";
		model="\WheelOfFortune\Data\WheelOfFortune.p3d";
	};
	
	class WheelDropBox: Inventory_Base
	{
		scope=2;
		model="\dz\gear\containers\Protector_Case.p3d"; // Will change later
		displayName="Betting Drop Box";
		descriptionShort="Place your bet for the Wheel of Fortune here!";
		rotationFlags=17;
		itemsCargoSize[]={10,2};
		attachments[]=
		{
			"WheelDropBox_Bet1",
			"WheelDropBox_Bet3",
			"WheelDropBox_Bet5",
			"WheelDropBox_Bet10",
			"WheelDropBox_Bet20"
		};
	};
	
	class Rag;
	class BettingRag: Rag
	{
		scope=2;
		canBeSplit=1;
		varQuantityInit=100;
		varQuantityMin=0;
		varQuantityMax=100;
		varQuantityDestroyOnMin=1;
		inventorySlot[]=
		{
			"WheelDropBox_Bet1",
			"WheelDropBox_Bet3",
			"WheelDropBox_Bet5",
			"WheelDropBox_Bet10",
			"WheelDropBox_Bet20"
		};
	};
};

class CfgSlots
{
	class Slot_WheelDropBox_Bet1
	{
		name="WheelDropBox_Bet1";
		displayName="Bet On 1";
	};
	class Slot_WheelDropBox_Bet3
	{
		name="WheelDropBox_Bet3";
		displayName="Bet On 3";
	};
	class Slot_WheelDropBox_Bet5
	{
		name="WheelDropBox_Bet5";
		displayName="Bet On 5";
	};
	class Slot_WheelDropBox_Bet10
	{
		name="WheelDropBox_Bet10";
		displayName="Bet On 10";
	};
	class Slot_WheelDropBox_Bet20
	{
		name="WheelDropBox_Bet20";
		displayName="Bet On 20";
	};
};