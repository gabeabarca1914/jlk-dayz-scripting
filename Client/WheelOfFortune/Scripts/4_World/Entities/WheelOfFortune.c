class WheelOfFortune: ItemBase
{
	static const float SpinTimeOneFullRotation = 3; // In seconds, may set to random interval later
	static const float AnimPhaseAmount_Spin360Degrees = 36; // Don't change this unless you know what you're doing
	
	float LastAnimPhase;
	float SpinAnimPhaseValue;
	bool ShouldSpin;
	float SpinTimeTotal;
	ref Timer waitForSpinToFinish;
	
	void WheelOfFortune()
	{
		SetEventMask( EntityEvent.SIMULATE ); // Calls EOnSimulate every frame ( Server )
	}
	
	void ~WheelOfFortune()	{}
	
	override bool CanPutInCargo( EntityAI parent )
    {
        return false;
    }
	
    override bool CanPutIntoHands( EntityAI parent )
    {
        return false;
    }
	
	bool IsSpinning()
	{
		return ShouldSpin;
	}
	
	void SpinTheWheel()
	{
		// I want the player to still be able to spin the wheel, even without betting ( just for fun )
		// So don't check for dropbox validity here
		
		SpinAnimPhaseValue = GetSpinAnimPhaseValue( 2, 5 );
		float FullSpinsToTake = ( SpinAnimPhaseValue / AnimPhaseAmount_Spin360Degrees );
		SpinTimeTotal = FullSpinsToTake * SpinTimeOneFullRotation;
		ShouldSpin = true;
		
		waitForSpinToFinish = new Timer;
		waitForSpinToFinish.Run( SpinTimeTotal + 2, this, "OnWheelSpinFinished" );
		Print( "[WheelOfFortune] Wheel Spun, the spin should take " + SpinTimeTotal.ToString() + " seconds" );
	}
	
	void OnWheelSpinFinished() // call this <SpinTimeTotal + 2> seconds after you call SpinTheWheel
	{
		// Server stuff
		if ( GetGame().IsServer() )
		{
			WheelDropBox nearestDropBox;
			
			if ( CastTo( nearestDropBox, FindNearestDropBox() ) )
			{
				nearestDropBox.ExecuteBettingResult( GetWheelValue() );
			}
		}
		
		// Client stuff ( possibly sounds )
		if ( GetGame().IsClient() )
		{
			ShouldSpin = false; // Just for action condition
		}
	}
	
	int GetWheelValue() // call this <SpinStartToEndDuration + 2> seconds after you call SpinTheWheel
	{
		float AnimPhase = GetAnimationPhase( "Wheel" );
		
		// I got these values by sight with buldozer in objectbuilder
		if ( AnimPhase >= 34.665 )	return 3;
		if ( AnimPhase >= 33.14 )	return 1;
		if ( AnimPhase >= 31.84 )	return 10;
		if ( AnimPhase >= 30.29 )	return 1;
		if ( AnimPhase >= 29.04 )	return 3;
		if ( AnimPhase >= 27.52 )	return 1;
		if ( AnimPhase >= 26.205 )	return 5;
		if ( AnimPhase >= 24.63 )	return 1;
		if ( AnimPhase >= 23.31 )	return 5;
		if ( AnimPhase >= 21.84 )	return 3;
		if ( AnimPhase >= 20.33 )	return 1;
		if ( AnimPhase >= 18.80 )	return 10;
		if ( AnimPhase >= 17.4 )	return 1;
		if ( AnimPhase >= 36.0 )	return 3;
		if ( AnimPhase >= 14.45 )	return 1;
		if ( AnimPhase >= 13.33 )	return 5;
		if ( AnimPhase >= 11.49 )	return 1;
		if ( AnimPhase >= 10.15 )	return 3;
		if ( AnimPhase >= 8.52 )	return 1;
		if ( AnimPhase >= 7.22 )	return 20;
		if ( AnimPhase >= 5.67 )	return 1;
		if ( AnimPhase >= 3.34 )	return 3;
		if ( AnimPhase >= 2.80 )	return 1;
		if ( AnimPhase >= 1.49 )	return 5;
		if ( AnimPhase >= 0.00 )	return 1;
		
		return 0;
	}
	
	// Comments explaining this soon
	float counter_lerpAnimPhase;
	float thisRotation;
	void LerpWheelAnimPhase( float deltaTime )
	{
		if ( !ShouldSpin )	return;
		
		counter_lerpAnimPhase += deltaTime;
		
		float lerpAnimPhase;
		
		int TimesToFullRotate = SpinAnimPhaseValue / AnimPhaseAmount_Spin360Degrees;
		float NextAnimPhase = MiscGameplayFunctions.FModulus( SpinAnimPhaseValue, AnimPhaseAmount_Spin360Degrees ); // FMod is remainder (%) for floats
		float LastAnimPhaseNormalized = ( LastAnimPhase / AnimPhaseAmount_Spin360Degrees );
		float NextAnimPhaseNormalized = ( NextAnimPhase / AnimPhaseAmount_Spin360Degrees );
		
		if ( thisRotation < TimesToFullRotate )
		{
			Print( "[WheelOfFortune] thisRotation < TimesToFullRotate" );
			if ( GetAnimationPhase( "Wheel" ) >= LastAnimPhase )
			{
				Print( "[WheelOfFortune] GetAnimationPhase( \"Wheel\" ) >= LastAnimPhase" );
				lerpAnimPhase = Math.Lerp( LastAnimPhase, AnimPhaseAmount_Spin360Degrees, ( counter_lerpAnimPhase / ( SpinTimeOneFullRotation * ( 1 - LastAnimPhaseNormalized ) ) ) );
				if ( lerpAnimPhase > AnimPhaseAmount_Spin360Degrees )	lerpAnimPhase = AnimPhaseAmount_Spin360Degrees;
				SetAnimationPhase( "Wheel", lerpAnimPhase );
			}
			else
			{
				Print( "[WheelOfFortune] ! GetAnimationPhase( \"Wheel\" ) >= LastAnimPhase" );
				lerpAnimPhase = Math.Lerp( 0, LastAnimPhase, ( counter_lerpAnimPhase / ( SpinTimeOneFullRotation * LastAnimPhaseNormalized ) ) );
				if ( lerpAnimPhase > LastAnimPhase )	lerpAnimPhase = LastAnimPhase;
				SetAnimationPhase( "Wheel", lerpAnimPhase );
			}
			
			thisRotation++;
		}
		else
		{
			Print( "[WheelOfFortune] ! thisRotation < TimesToFullRotate" );
			if ( NextAnimPhase < LastAnimPhase )
			{
				Print( "[WheelOfFortune] NextAnimPhase < LastAnimPhase" );
				if ( LastAnimPhase < AnimPhaseAmount_Spin360Degrees )
				{
					Print( "[WheelOfFortune] GetAnimationPhase( \"Wheel\" ) >= LastAnimPhase" );
					lerpAnimPhase = Math.Lerp( LastAnimPhase, AnimPhaseAmount_Spin360Degrees, ( counter_lerpAnimPhase / ( ( SpinTimeOneFullRotation * 2 ) * ( 1 - LastAnimPhaseNormalized ) ) ) );
					if ( lerpAnimPhase > AnimPhaseAmount_Spin360Degrees )	lerpAnimPhase = AnimPhaseAmount_Spin360Degrees;
					SetAnimationPhase( "Wheel", lerpAnimPhase );
				}
				else
				{
					Print( "[WheelOfFortune] ! NextAnimPhase < LastAnimPhase" );
					lerpAnimPhase = Math.Lerp( 0, NextAnimPhase, ( counter_lerpAnimPhase / ( ( SpinTimeOneFullRotation * 2 ) * NextAnimPhaseNormalized ) ) );
					if ( lerpAnimPhase > NextAnimPhase )	lerpAnimPhase = NextAnimPhase;
					SetAnimationPhase( "Wheel", lerpAnimPhase );
				
					if ( lerpAnimPhase == NextAnimPhase )
					{
						LastAnimPhase = NextAnimPhase;
						counter_lerpAnimPhase = 0;
						thisRotation = 0;
						ShouldSpin = false;
					}
				}
			}
			else
			{
				Print( "[WheelOfFortune] ! NextAnimPhase < LastAnimPhase" );
				lerpAnimPhase = Math.Lerp( LastAnimPhase, NextAnimPhase, ( counter_lerpAnimPhase / ( ( SpinTimeOneFullRotation * 2 ) * ( NextAnimPhaseNormalized - LastAnimPhaseNormalized ) ) ) );
				if ( lerpAnimPhase > NextAnimPhase )	lerpAnimPhase = NextAnimPhase;
				SetAnimationPhase( "Wheel", lerpAnimPhase );
				
				if ( lerpAnimPhase == NextAnimPhase )
				{
					LastAnimPhase = NextAnimPhase;
					counter_lerpAnimPhase = 0;
					thisRotation = 0;
					ShouldSpin = false;
				}
			}
		}
	}
	
	override void EOnSimulate( IEntity other, float dt ) // OnTick ( Server )
	{
		LerpWheelAnimPhase( dt );
	}
	
	float GetSpinAnimPhaseValue( float minFullSpins, float maxFullSpins )
	{
		return Math.RandomFloatInclusive( minFullSpins, maxFullSpins ) * AnimPhaseAmount_Spin360Degrees;
	}
	
	WheelDropBox FindNearestDropBox( float radius = 50 )
	{
		vector thisWheelPos = GetPosition();
		Object nearestDropBox;
		vector nearestDropBoxPos;
		float nearestDropBoxDist;
		
		array<Object> objects = new array<Object>();
		array<CargoBase> proxyCargos = new array<CargoBase>();
		
		GetGame().GetObjectsAtPosition3D( thisWheelPos, radius, objects, proxyCargos );
		
		foreach ( Object thisDropBox: objects )
		{
			if ( !thisDropBox.IsInherited( WheelDropBox ) )		continue;
			
			vector thisDropBoxPos = thisDropBox.GetPosition();
			float thisDropBoxDist = vector.Distance( thisWheelPos, thisDropBoxPos );
			
			if ( thisDropBoxDist < nearestDropBoxDist )
			{
				nearestDropBox = thisDropBox;
				nearestDropBoxPos = thisDropBoxPos;
				nearestDropBoxDist = thisDropBoxDist;
			}
		}
		
		if ( nearestDropBox )
		{
			return WheelDropBox.Cast( nearestDropBox );
		}
		
		return NULL;
	}
}