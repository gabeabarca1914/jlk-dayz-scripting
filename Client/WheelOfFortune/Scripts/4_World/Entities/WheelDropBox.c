class WheelDropBox: ItemBase
{
	const float MaxTimeBetsOrWinningsAllowedToStay = 60; // In Seconds
	
	string CurrentPlayerBettingID;
	ItemBase BetInAttachment;
	int NumBettingOn;
	
	override void EEItemAttached ( EntityAI item, string slot_name )
	{
		super.EEItemAttached ( item, slot_name );
		
		SetPlayerOwner();
	}
	
	override void EEItemDetached ( EntityAI item, string slot_name )
	{
		super.EEItemDetached ( item, slot_name );
		
		if ( !HasAnyCargo() && GetInventory().AttachmentCount() == 0 )
			SetPlayerOwner( true );
	}
	
	void EECargoOut( EntityAI item )
	{
		super.EECargoOut ( item );
		
		if ( !HasAnyCargo() && GetInventory().AttachmentCount() == 0 )
			SetPlayerOwner( true );
	}
	
	override bool CanReceiveItemIntoCargo( EntityAI item )
	{
		return false;
	}

	override bool CanReceiveAttachment( EntityAI attachment, int slotId )
	{
		if ( !IsPlayerOwner() )	return false;
		if ( GetInventory().AttachmentCount() > 0 )	return false;
		
		return super.CanReceiveAttachment( attachment, slotId );
	}
	
	override bool CanReleaseAttachment ( EntityAI attachment )
	{
		if ( !IsPlayerOwner() )	return false;
		
		return super.CanReleaseAttachment( attachment );
	}
	
	override bool CanReleaseCargo ( EntityAI cargo )
	{
		if ( !IsPlayerOwner() )	return false;
		
		return super.CanReleaseCargo( cargo );
	}
	
	override bool CanPutInCargo( EntityAI parent )
    {
        return false;
    }
	
    override bool CanPutIntoHands( EntityAI parent )
    {
        return false;
    }
	
	bool IsPlayerOwner()
	{
		// If there is no owner, set one
		SetPlayerOwner();
		return true;
		
		PlayerBase player;
		Print( "[WheelOfFortune] Casting To Player" );
		if ( !CastTo( player, GetGame().GetPlayer() ) )					return false;
		Print( "[WheelOfFortune] Player Casted, Compare PlayerID" );
		if ( player.GetIdentity().GetId() != CurrentPlayerBettingID )	return false;
		Print( "[WheelOfFortune] PlayerID match" );
		
		return true;
	}
	
	void SetPlayerOwner( bool ToNobody = false )
	{
		if ( ToNobody )
			CurrentPlayerBettingID = "";
			return;
		
		PlayerBase player;
		Print( "[WheelOfFortune] Set Casting To Player" );
		if ( !CastTo( player, GetGame().GetPlayer() ) )	return;
		Print( "[WheelOfFortune] Set Player setting" );
		CurrentPlayerBettingID = player.GetIdentity().GetId();
		Print( "[WheelOfFortune] Set PlayerID set" );
	}
	
	ItemBase GetBetInAttachment()
	{
		ItemBase itemToReturn;
		
		if ( CastTo( BetInAttachment, FindAttachmentBySlotName( "WheelDropBox_Bet1" ) ) )		NumBettingOn = 1;
		if ( CastTo( BetInAttachment, FindAttachmentBySlotName( "WheelDropBox_Bet3" ) ) )		NumBettingOn = 3;
		if ( CastTo( BetInAttachment, FindAttachmentBySlotName( "WheelDropBox_Bet5" ) ) )		NumBettingOn = 5;
		if ( CastTo( BetInAttachment, FindAttachmentBySlotName( "WheelDropBox_Bet10" ) ) )		NumBettingOn = 10;
		if ( CastTo( BetInAttachment, FindAttachmentBySlotName( "WheelDropBox_Bet20" ) ) )		NumBettingOn = 20;
		
		return BetInAttachment;
	}
	
	void WheelDropBox()	{}
	
	void ~WheelDropBox()	{}
	
	void ExecuteBettingResult( int wheelValue )
	{
		if ( !GetBetInAttachment() )	return;
		
		int betQty = BetInAttachment.GetQuantity();
		
		GetGame().ObjectDelete( BetInAttachment );
		
		if ( wheelValue != NumBettingOn )	return;
		
		int returnQty = betQty + ( betQty * NumBettingOn );
		
		for ( int i; i < returnQty; i++ )
		{
			GetInventory().CreateInInventory( BetInAttachment.GetType() );
		}
	}
	
	float counter_ClearRogueFunds;
	override void EOnFrame( IEntity other, float timeSlice )
	{
		super.EOnFrame( other, timeSlice );
		
		counter_ClearRogueFunds += timeSlice;
		
		// If there is no playerBettingID ( Server restarted ) or the MaxTimeBetsOrWinningsAllowedToStay time is up
		if ( !CurrentPlayerBettingID || ( counter_ClearRogueFunds >= MaxTimeBetsOrWinningsAllowedToStay ) )
		{
			// Clear all cargo/attachments
			array<EntityAI> itemsArray = new array<EntityAI>;
			GetInventory().EnumerateInventory( InventoryTraversalType.LEVELORDER, itemsArray );
			
			foreach ( EntityAI item: itemsArray )
			{
				GetGame().ObjectDelete( item );
			}
			
			counter_ClearRogueFunds = 0;
		}
	}
}