class Action_SpinTheWheel: ActionInteractBase
{
	WheelOfFortune wheel;
	ref Timer waitForSpinToFinish;
	int wheelValue;
	
	override string GetText()
	{
		return "Spin The Wheel";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		// I want the player to still be able to spin the wheel, even without betting ( just for fun )
		// So don't check for dropbox validity here
		
		if ( !Class.CastTo( wheel, target.GetObject() ) )		return false;
		if ( wheel.IsSpinning() )								return false;
		
		Print( "[WheelOfFortune] ActionCondition Passed" );
		
		return true;
	}
	
	override void OnExecute( ActionData action_data )
	{
		wheel.SpinTheWheel();
	}
}