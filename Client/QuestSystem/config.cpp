class CfgPatches
{
	class QuestSystem
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Scripts"
		};
	};
};

class CfgMods
{
	class QuestSystem
	{
		name = "QuestSystem";
		dir = "QuestSystem";
		type = "mod";
		hideName = 1;
		hidePicture = 1;
		dependencies[] = { "Game", "World", "Mission" };
		class defs
		{
			class gameScriptModule
			{
				value = "";
				files[] = { "QuestSystem/Scripts/3_Game" };
			};
			
			class worldScriptModule
			{
				value = "";
				files[] = { "QuestSystem/Scripts/4_World" };
			};
			
			class missionScriptModule
			{
				value = "";
				files[] = { "QuestSystem/Scripts/5_Mission" };
			};
		};
	};
};

class cfgVehicles
{
	
};