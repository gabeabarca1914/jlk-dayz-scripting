class QuestSystemTracker
{
	static const string ProfilePath = "$profile:QuestSystem/Tracker";
	string PlayerID;
	ref TIntStringMap TraderID_Trackers;
	
	void QuestSystemTracker( string playerID )
	{
		if ( !FileExist( ProfilePath ) )
			MakeDirectory( ProfilePath );
		
		TraderID_Trackers = new TIntStringMap;
		
		PlayerID = playerID;
	}
	
	void Load()
	{
		string FilePath = ProfilePath + "/" + PlayerID + ".json";
		if ( FileExist( FilePath ) )
		{
			JsonFileLoader<QuestSystemTracker>.JsonLoadFile( FilePath, this );
		}
		else
		{
			JsonFileLoader<QuestSystemTracker>.JsonSaveFile( FilePath, this );
		}
	}
	
	void Save()
	{
		string FilePath = ProfilePath + "/" + PlayerID + ".json";
		JsonFileLoader<QuestSystemTracker>.JsonSaveFile( FilePath, this );
	}
	
	int GetQuestIndex( int traderID )
	{
		string FilePath = ProfilePath + "/" + PlayerID + ".json";
		if ( !FileExist( FilePath ) )
		{
			return -1; // This will ensure a file is not created for the player ( -1 means no quest active )
		}
		Load();
		
		string QuestAndRep;
		if ( !TraderID_Trackers.Find( traderID, QuestAndRep ) )
		{
			return -1;
		}
		
		TStringArray splitQuestAndRep = new TStringArray;
		QuestAndRep.Replace( "=", "" );
		QuestAndRep.Split( ",", splitQuestAndRep );
		
		string quest = splitQuestAndRep[0];
		quest.Replace( "QuestIndex", "" );
		
		return quest.ToInt();
	}
	
	void SetQuestIndex( int traderID, int QuestIndexToSet )
	{
		Load();
		string QuestAndRep;
		if ( TraderID_Trackers.Find( traderID, QuestAndRep ) );
		{
			TStringArray splitQuestAndRep = new TStringArray;
			QuestAndRep.Split( ",", splitQuestAndRep );
			
			string rep = splitQuestAndRep[1];
			if ( !rep || rep == "" )
				rep = "Rep=0";
			
			string QuestAndRepToSet = "QuestIndex=" + QuestIndexToSet.ToString() + "," + rep;
			
			TraderID_Trackers.Set( traderID, QuestAndRepToSet );
			Save();
			return;
		}
		
		TraderID_Trackers.Insert( traderID, "QuestIndex=" + QuestIndexToSet.ToString() + "," + "Rep=0" );
		Save();
	}
}