class QuestMenu extends UIScriptedMenu
{
	static const float RpcButtonCooldown = 1; // In seconds, to prevent rpc flooding/spoofing
	static const int ActionResponse_SuccessColor = ARGB( 255, 20, 120, 40 ); // Green
	static const int ActionResponse_ErrorColor = ARGB( 255, 130, 40, 20 ); // Red
	
	// Widgets
	Widget RootQuestMenu;
	Widget PanelQuestMenu;
	
	TextWidget TextTitle;
	TextWidget TextQuestCheck;
	TextWidget TextQuestTitle;
	TextWidget TextQuestDescription;
	TextWidget TextActionResponse;
	
	ButtonWidget BtnDeclineQuest;
	ButtonWidget BtnTurnInQuest;
	ButtonWidget BtnAcceptQuest;
	
	// Quest Vars
	PlayerBase trader;
	string TraderType;
	int questIndex;
	
	// Timers
	ref Timer timerDeclineQuestCooldown;
	ref Timer timerTurnInQuestCooldown;
	ref Timer timerRemoveActionResponse;
	
	// Panel Dragging
	float PreviousDragPanelPosX, PreviousDragPanelPosY;
	int PreviousDragMousePosX, PreviousDragMousePosY;
	
	void QuestMenu( PlayerBase Trader )
	{
		GetRPCManager().AddRPC( "QSRPC_QuestInfo", "OnRpcQuestInfo", this );
		GetRPCManager().AddRPC( "QSRPC_ResponseQuestSuccess", "OnRpcResponseQuestSuccess", this );
		
		trader = Trader;
	}

	void ~QuestMenu()
	{
		
	}
	
	override Widget Init()
	{
		// Root/Panel
		RootQuestMenu = GetGame().GetWorkspace().CreateWidgets( "QuestSystem/Layouts/QuestMenu.layout" );
		PanelQuestMenu = RootQuestMenu.FindAnyWidget( "PanelQuestMenu" );
		
		// Text
		CastTo( TextTitle, RootQuestMenu.FindAnyWidget( "TextTitle" ) );
		CastTo( TextQuestCheck, RootQuestMenu.FindAnyWidget( "TextQuestCheck" ) );
		CastTo( TextQuestTitle, RootQuestMenu.FindAnyWidget( "TextQuestTitle" ) );
		CastTo( TextQuestDescription, RootQuestMenu.FindAnyWidget( "TextQuestDescription" ) );
		CastTo( TextActionResponse, RootQuestMenu.FindAnyWidget( "TextActionResponse" ) );
		
		// Buttons
		CastTo( BtnDeclineQuest, RootQuestMenu.FindAnyWidget( "BtnDeclineQuest" ) );
		CastTo( BtnTurnInQuest, RootQuestMenu.FindAnyWidget( "BtnTurnInQuest" ) );
		CastTo( BtnAcceptQuest, RootQuestMenu.FindAnyWidget( "BtnAcceptQuest" ) );
		
		// Init Texts
		TextQuestCheck.SetText( "" );
		TextQuestTitle.SetText( "" );
		TextQuestDescription.SetText( "" );
		TextActionResponse.SetText( "" );
		
		// Hide it
		RootQuestMenu.Show( false );
		
		return RootQuestMenu;
	}
	
	bool OnMouseButtonDown( Widget w, int x, int y, int button )
	{
		super.OnMouseButtonDown( w, x, y, button );
		switch( w )
		{
			case RootQuestMenu:
				OnHide();
				return true;
				
			case PanelQuestMenu:
				g_Game.GetMousePos( PreviousDragMousePosX, PreviousDragMousePosY );
				PanelQuestMenu.GetPos( PreviousDragPanelPosX, PreviousDragPanelPosY );
				GetGame().GetDragQueue().Call( this, "OnPanelDrag" );
				return true;
				
			default:
				break;
		}
		
		return false;
	}
	
	void OnPanelDrag( int mouse_x, int mouse_y, bool is_dragging )
	{
		int w, h;
		GetScreenSize( w, h );
		
		float x = ( ( mouse_x - PreviousDragMousePosX ) / w ) + PreviousDragPanelPosX;
		float y = ( ( mouse_y - PreviousDragMousePosY ) / h ) + PreviousDragPanelPosY;
		
		PanelQuestMenu.SetPos( x, y );
	}
	
	override bool OnClick( Widget w, int x, int y, int button )
	{
		super.OnClick( w, x, y, button );
		
		switch( w )
		{
			case BtnDeclineQuest:
				RequestDeclineQuest();
				return true;
			case BtnTurnInQuest:
				RequestTurnInQuest();
				return true;
			case BtnAcceptQuest:
				RequestAcceptQuest();
				return true;
				
			default:
				break;
		}
		
		return false;
	}
	
	bool OnKeyDown( Widget w, int x, int y, int key )
	{
		super.OnKeyDown( w, x, y, key );
		
		switch( key )
		{
			case KeyCode.KC_ESCAPE:
			case KeyCode.KC_F:
			case KeyCode.KC_TAB:
				OnHide();
				return true;
				
			default:
				break;
		}
		
		return false;
	}
	
	void RequestTurnInQuest()
	{
		GetRPCManager().SendRPC( "QSRPC_CheckQuestSuccess", "OnRpcCheckQuestSuccess", new Param3<PlayerBase,PlayerBase,int>( GetGame().GetPlayer(), trader, questIndex ), true );
		BtnTurnInQuest.Enable( false );
	}
	
	void RequestAcceptQuest()
	{
		GetRPCManager().SendRPC( "QSRPC_AcceptQuest", "OnRpcAcceptQuest", new Param3<PlayerBase,PlayerBase,int>( GetGame().GetPlayer(), trader, questIndex ), true );
		BtnAcceptQuest.Enable( false );
		TextActionResponse.SetText( "This quest has been accepted!" );
		TextActionResponse.SetColor( ActionResponse_SuccessColor );
		if ( timerRemoveActionResponse && timerRemoveActionResponse.IsRunning() )
		{
			timerRemoveActionResponse.Stop();
		}
		timerRemoveActionResponse = new Timer;
		timerRemoveActionResponse.Run( 3, TextActionResponse, "SetText", new Param1<string>( "" ) );
		BtnTurnInQuest.Enable( true );
	}
	
	void RequestDeclineQuest( bool wasTurnedIn = false )
	{
		GetRPCManager().SendRPC( "QSRPC_DeclineQuest", "OnRpcDeclineQuest", new Param3<PlayerBase,PlayerBase,int>( GetGame().GetPlayer(), trader, questIndex ), true );
		
		BtnAcceptQuest.Enable( true );
		
		BtnDeclineQuest.Enable( false );
		timerDeclineQuestCooldown = new Timer;
		timerDeclineQuestCooldown.Run( RpcButtonCooldown, BtnDeclineQuest, "Enable", new Param1<bool>( true ) );
		
		if ( !wasTurnedIn )
		{
			TextActionResponse.SetText( "The last quest was declined!" );
			TextActionResponse.SetColor( ActionResponse_SuccessColor );
			if ( timerRemoveActionResponse && timerRemoveActionResponse.IsRunning() )
			{
				timerRemoveActionResponse.Stop();
			}
			timerRemoveActionResponse = new Timer;
			timerRemoveActionResponse.Run( 3, TextActionResponse, "SetText", new Param1<string>( "" ) );
		}
	}
	
	void OnRpcResponseQuestSuccess( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
		Param1<bool> params;
		if ( !ctx.Read( params ) )	return;
		
		bool wasSuccessful = params.param1;
		
		if ( !wasSuccessful )
		{
			TextActionResponse.SetText( "You did not complete the quest!" );
			TextActionResponse.SetColor( ActionResponse_ErrorColor );
			timerTurnInQuestCooldown = new Timer;
			timerTurnInQuestCooldown.Run( RpcButtonCooldown, BtnTurnInQuest, "Enable", new Param1<bool>( true ) );
		}
		
		if ( wasSuccessful )
		{
			TextActionResponse.SetText( "Thank you for your hard work!" );
			TextActionResponse.SetColor( ActionResponse_SuccessColor );
			RequestDeclineQuest( true );
			TextQuestCheck.SetText( "I have a quest for you!" );
			BtnTurnInQuest.Enable( false );
		}
		
		if ( timerRemoveActionResponse && timerRemoveActionResponse.IsRunning() )
		{
			timerRemoveActionResponse.Stop();
		}
		timerRemoveActionResponse = new Timer;
		timerRemoveActionResponse.Run( 3, TextActionResponse, "SetText", new Param1<string>( "" ) );
	}
	
	void OnRpcQuestInfo( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
		Param1<string> params;
		if ( !ctx.Read( params ) )	return;
		
		string msg = params.param1;
		
		string Check, Title, Description;
		TStringArray splitMsg = new TStringArray;
		
		msg.Split( "|", splitMsg );
		
		TraderType =  splitMsg[0];
		questIndex =  splitMsg[1].ToInt();
		Check = splitMsg[2];
		Title = splitMsg[3];
		Description = splitMsg[4];
		
		TextTitle.SetText( TraderType + " Quests" );
		TextQuestCheck.SetText( Check );
		TextQuestTitle.SetText( Title );
		TextQuestDescription.SetText( Description );
		
		if ( Check == "I have a quest for you!" )
		{
			BtnTurnInQuest.Enable( false );
		}
		
		if ( Check == "I already gave you a quest!" )
		{
			BtnAcceptQuest.Enable( false );
		}
	}
	
	override void OnShow()
	{
		super.OnShow();
		
		RootQuestMenu.Show( true );
		PPEffects.SetBlurMenu( 0.4 );
		GetGame().GetInput().ChangeGameFocus( 1 );
		GetGame().GetUIManager().ShowUICursor( true );
		GetGame().GetMission().GetHud().Show( false );
		GetGame().GetMission().PlayerControlDisable( INPUT_EXCLUDE_ALL );
		SetFocus( RootQuestMenu );
	}
	
	override void OnHide()
	{
		super.OnHide();
		
		RootQuestMenu.Show( false );
		PPEffects.SetBlurMenu( 0 );
		GetGame().GetUIManager().ShowUICursor( false );
		GetGame().GetMission().GetHud().Show( true );
		GetGame().GetMission().PlayerControlEnable( false );
		GetGame().GetInput().ResetGameFocus();
		Close();
	}
}
