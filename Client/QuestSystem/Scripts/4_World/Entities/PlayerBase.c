
modded class PlayerBase
{
	static ref TStringArray arrayTraderTypes;
	
	override void Init()
	{
		super.Init();
		
		if ( GetGame().IsServer() && !arrayTraderTypes )
			RegisterTraderTypes( arrayTraderTypes );
	}
	
	bool IsTrader()
	{
		if ( TraderID == -1 )	return false;
		
		return true;
	}
	
	string GetTraderType()
	{
		return GetTraderTypeFromTraderID( TraderID );
	}
	
	static string GetTraderTypeFromTraderID( int traderID )
	{
		return arrayTraderTypes.Get( traderID );
	}
	
	string GetTraderName()
	{
		// TODO
	}
	
	static string GetTraderNameFromTraderID( int traderID )
	{
		// TODO
	}
	
	void GetItemsForQuest( out array<EntityAI> items )
	{
		items = new array<EntityAI>;
		array<EntityAI> cargoItems = new array<EntityAI>;
		
		// Get "Everything"
		GetInventory().EnumerateInventory( InventoryTraversalType.LEVELORDER, items );
		
		// Remove everything but the first level ( because they are null pointers )
		for ( int i = 0; i < items.Count(); i++ )
		{
			if ( !items[i] )
			{
				items.Remove( i );
				i -= 1;
			}
		}
		
		// Iterate through each level, adding only valid entities to the out array
		for ( int j = 0; j < items.Count(); j++ )
		{
			items[j].GetInventory().EnumerateInventory( InventoryTraversalType.LEVELORDER, cargoItems );
			
			foreach ( auto cargoItem: cargoItems )
			{
				if ( cargoItem )
				{
					items.Insert( cargoItem );
				}
			}
			
			cargoItems.Clear();
		}
	}
	
 	override void SetActions() 
	{
		super.SetActions();
		
		AddAction( ActionOpenQuestMenu );
	}
}
