class ActionOpenQuestMenu: ActionInteractBase
{
	PlayerBase m_player;
	PlayerBase m_trader;
	
	override string GetText()
	{
		return "Quests";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( !player )	return false;
		m_player = player;
		if ( !Class.CastTo( m_trader, target.GetObject() ) )	return false;
		if ( !m_trader.IsTrader() )	return false;
		
		return true;
	}
	
	override void OnExecuteServer( ActionData action_data )
	{
		ref QuestSystemServer QS_Server;
		QS_Server = QuestSystemServer.Get();
		QS_Server.OnOpenQuestMenu( m_player, m_trader );
	}
	
	override void OnExecuteClient( ActionData action_data )
	{
		ref QuestSystemClient QS_Client;
		QS_Client = QuestSystemClient.Get();
		QS_Client.OpenQuestMenu( m_trader );
	}
};