
class QuestSystemClient
{
	static ref QuestSystemClient instance;
	
	ref QuestMenu questMenu;
	
	QuestMenu GetQuestMenu()
	{
		return questMenu;
	}
	
	static QuestSystemClient Get()
	{
		if ( !instance )
			instance = new QuestSystemClient;
		
		return instance;
	}
	
	void QuestSystemClient()
	{
		
	}
	
	void OpenQuestMenu( PlayerBase trader )
	{
		if ( GetGame().GetUIManager().GetMenu() == NULL )
		{
			questMenu = new QuestMenu( trader );
			questMenu.Init();
			GetGame().GetUIManager().ShowScriptedMenu( questMenu, NULL );
		}
	}
	
	void CloseQuestMenu()
	{
		if ( questMenu )
			questMenu.OnHide();
	}
}
