
class Quest
{
	string TraderType;
	string Title;
	string Description;
	float MinsExpire;
	
	ref Timer TimerExpire;
	
	void Quest( TStringStringMap Params = null )
	{
		string value;
		
		if ( Params.Find( "TraderType", value ) )
			TraderType = value;
		if ( Params.Find( "Title", value ) )
			Title = value;
		if ( Params.Find( "Description", value ) )
			Description = value;
		if ( Params.Find( "MinsExpire", value ) )
			MinsExpire = value.ToFloat();
	}
	
	void Start( PlayerBase player, PlayerBase trader )
	{
		if ( MinsExpire )
		{
			TimerExpire = new Timer;
			TimerExpire.Run( MinsExpire, this, "Cleanup", new Param2<PlayerBase,PlayerBase>( player, trader ) );
		}
		
		Print( "[QuestSystem]: " + ClassName() + " Started! " + Title );
		
		// Script the quest here
	}
	
	bool CompleteIfSuccessful( PlayerBase player, PlayerBase trader )
	{
		// Call Complete( player, trader ) if successful
		// Return success!
	}
	
	void Complete( PlayerBase player, PlayerBase trader )
	{
		if ( TimerExpire && TimerExpire.IsRunning() )
		{
			TimerExpire.Stop();
			TimerExpire = NULL;
		}
		
		// Give player rewards if applicable
		// Give this player rep in player.mapRepTracker
		// Serialize updated player.mapRepTracker in a server profile file for this player
		
		// Call Cleanup
		Cleanup( player, trader );
	}
	
	void Cleanup( PlayerBase player, PlayerBase trader )
	{
		// !!! Remove entities created in the scripted quest !!!
	}
}

class Quest_FindItems: Quest
{
	int Quantity;
	string MinHealth;
	ref TStringArray Classnames;
	
	void Quest_FindItems( TStringStringMap Params = null )
	{
		string value;
		
		if ( Params.Find( "Quantity", value ) )
			Quantity = value.ToInt();
		if ( Params.Find( "MinHealth", value ) )
			MinHealth = value;
		if ( Params.Find( "Classnames", value ) )
			Classnames = new TStringArray;
			value.Replace( " ", "" );
			value.Split( ",", Classnames );
	}
	
	override void Start( PlayerBase player, PlayerBase trader )
	{
		super.Start( player, trader );
		
		// Script the quest here
	}
	
	override bool CompleteIfSuccessful( PlayerBase player, PlayerBase trader )
	{
		array<EntityAI> items = new array<EntityAI>;
		array<EntityAI> questItems = new array<EntityAI>;
		
		player.GetItemsForQuest( items );
		Print( "EnumerateInventory Count = " + items.Count().ToString() );
		
		foreach ( EntityAI item: items )
		{
			if ( questItems.Count() >= Quantity )
			{
				break;
			}
			
			foreach ( string classname: Classnames )
			{
				if ( ItemBase.Cast( item ).IsKindOf( classname ) )
				{
					questItems.Insert( item );
				}
			}
		}
		
		if ( questItems.Count() < Quantity )
		{
			return false;
		}
		
		foreach ( EntityAI questItem: questItems )
		{
			GetGame().ObjectDelete( questItem );
		}
		
		Complete( player, trader );
		return true;
	}
	
	override void Complete( PlayerBase player, PlayerBase trader )
	{
		super.Complete( player, trader );
		
		// Give player rewards if applicable
		// Super calls Cleanup
	}
	
	override void Cleanup( PlayerBase player, PlayerBase trader )
	{
		super.Cleanup( player, trader );
		
		// !!! Remove entities created in the scripted quest !!!
	}
}

class Quest_DynamicEventFindItems: Quest_FindItems
{
	void Quest_DynamicEventFindItems( TStringStringMap Params = null, TStringStringMap pItemSpawnInfo = null, TStringStringMap pZombieSpawnInfo = null )
	{
		// !!! TODO !!!
	}
	
	override void Start( PlayerBase player, PlayerBase trader )
	{
		super.Start( player, trader );
		
		// Script the quest here
	}
	
	override bool CompleteIfSuccessful( PlayerBase player, PlayerBase trader )
	{
		// Call Complete( player, trader ) if successful
		// Return success!
	}
	
	override void Complete( PlayerBase player, PlayerBase trader )
	{
		super.Complete( player, trader );
		
		// Give player rewards if applicable
		// Super calls Cleanup
	}
	
	override void Cleanup( PlayerBase player, PlayerBase trader )
	{
		super.Cleanup( player, trader );
		
		// !!! Remove entities created in the scripted quest !!!
	}
}
