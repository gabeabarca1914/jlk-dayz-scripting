
class QuestSystemServer
{
	protected static ref QuestSystemServer instance;
	
	ref array<ref Quest> Food_Quests;
	ref array<ref Quest> Tools_Quests;
	ref array<ref Quest> Weapons_Quests;
	ref QuestSystemTracker tracker;
	
	static QuestSystemServer Get()
	{
		if ( !instance )
			instance = new QuestSystemServer;
		
		return instance;
	}
	
	void QuestSystemServer()
	{
		GetRPCManager().AddRPC( "QSRPC_DeclineQuest", "OnRpcDeclineQuest", this );
		GetRPCManager().AddRPC( "QSRPC_AcceptQuest", "OnRpcAcceptQuest", this );
		GetRPCManager().AddRPC( "QSRPC_CheckQuestSuccess", "OnRpcCheckQuestSuccess", this );
		
		Food_Quests = new array<ref Quest>;
		Tools_Quests = new array<ref Quest>;
		Weapons_Quests = new array<ref Quest>;
		
		RegisterQuests( Food_Quests, Tools_Quests, Weapons_Quests );
	}

	void RegisterQuests()
	{
		if ( !FileExist( "$profile:/QuestSystem/RegisterQuests.json" ) )
		{
			CopyFile( "QuestSystem/Scripts/4_World/ServerConfig/RegisterQuests.json", "$profile:/QuestSystem/RegisterQuests.json" );
		}
		DecommentJson( "$profile:/QuestSystem/RegisterQuests.json", "$profile:/QuestSystem/Decommented_RegisterQuests.json" );
		ref JsonDataRegisterQuests quests;
		JsonFileLoader<ref JsonDataRegisterQuests>.JsonLoadFile( "$profile:/QuestSystem/Decommented_RegisterQuests.json", quests );
	}
	
	void OnRpcCheckQuestSuccess( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
		Param3<PlayerBase,PlayerBase,int> params;
		if ( !ctx.Read( params ) )	return;
		
		PlayerBase player = params.param1;
		PlayerBase trader = params.param2;
		int questIndex = params.param3;
		
		// Get the quest
		Quest quest;
		string traderType = trader.GetTraderType();
		if ( traderType == "Food" )
			quest = Food_Quests.Get( questIndex );
		else if ( traderType == "Tools" )
			quest = Tools_Quests.Get( questIndex );
		else if ( traderType == "Weapons" )
			quest = Weapons_Quests.Get( questIndex );
		else
			return;
		
		Print( quest.Title );
		
		// Check if quest completion was successful
		bool wasSuccessful;
		wasSuccessful = quest.CompleteIfSuccessful( player, trader );
		
		// Send rpc and return if failed
		if ( !wasSuccessful )
		{
			GetRPCManager().SendRPC( "QSRPC_ResponseQuestSuccess", "OnRpcResponseQuestSuccess", new Param1<bool>( false ), true );
			return;
		}
		
		// Remove quest from tracker
		tracker = new QuestSystemTracker( player.GetIdentity().GetPlainId() );
		tracker.SetQuestIndex( trader.TraderID, -1 );
		
		// Send rpc to tell client it was successful
		GetRPCManager().SendRPC( "QSRPC_ResponseQuestSuccess", "OnRpcResponseQuestSuccess", new Param1<bool>( true ), true );
	}
	
	void OnRpcAcceptQuest( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
		Param3<PlayerBase,PlayerBase,int> params;
		if ( !ctx.Read( params ) )	return;
		
		PlayerBase player = params.param1;
		PlayerBase trader = params.param2;
		int questIndex = params.param3;
		
		tracker = new QuestSystemTracker( player.GetIdentity().GetPlainId() );
		tracker.SetQuestIndex( trader.TraderID, questIndex );
		
		Quest quest;
		
		string traderType = trader.GetTraderType();
		if ( traderType == "Food" )				quest = Food_Quests.Get( questIndex );
		else if ( traderType == "Tools" )		quest = Tools_Quests.Get( questIndex );
		else if ( traderType == "Weapons" )		quest = Weapons_Quests.Get( questIndex );
		else	return;
		
		quest.Start( player, trader );
	}
	
	void OnRpcDeclineQuest( CallType type, ParamsReadContext ctx, PlayerIdentity sender, Object target )
	{
		Param3<PlayerBase,PlayerBase,int> params;
		if ( !ctx.Read( params ) )	return;
		
		PlayerBase player = params.param1;
		PlayerBase trader = params.param2;
		int previousQuestIndex = params.param3;
		
		OnOpenQuestMenu( player, trader, previousQuestIndex );
	}
	
	// Check player if player is already doing a quest from this trader
	// Send an appproriate rpc to the client to update the quest menu
	void OnOpenQuestMenu( PlayerBase player, PlayerBase trader, int previousQuestIndex = -1 )
	{
		string traderType = trader.GetTraderType();
		Quest quest;
		string check;
		
		tracker = new QuestSystemTracker( player.GetIdentity().GetPlainId() );
		int questIndex = tracker.GetQuestIndex( trader.TraderID );
		
		// For Decline Quest, if there was a previousQuestIndex, get a new quest
		if ( previousQuestIndex != -1 )
		{
			questIndex = -1;
		}
		
		// If the player was not already doing a quest for this trader, get a random quest index
		if ( questIndex == -1 )
		{
			check = "I have a quest for you!";
			
			if ( traderType == "Food" )				questIndex = Math.RandomInt( 0, Food_Quests.Count() );
			else if ( traderType == "Tools" )		questIndex = Math.RandomInt( 0, Tools_Quests.Count() );
			else if ( traderType == "Weapons" )		questIndex = Math.RandomInt( 0, Weapons_Quests.Count() );
			else	return;
		}
		else // if this player has already gotten a quest from this trader
		{
			check = "I already gave you a quest!";
		}
		
		// For Decline Quest, this ensures the player will not get the same quest twice in a row
		if ( questIndex == previousQuestIndex )
		{
			OnOpenQuestMenu( player, trader, previousQuestIndex );
			return;
		}
		
		// Find the quest using the previously found or random questIndex
		if ( traderType == "Food" )
			quest = Food_Quests.Get( questIndex );
		else if ( traderType == "Tools" )
			quest = Tools_Quests.Get( questIndex );
		else if ( traderType == "Weapons" )
			quest = Weapons_Quests.Get( questIndex );
		else
			return;
		
		// Send this to the client to update the quest menu
		string msg = trader.GetTraderType() + "|" + questIndex + "|" + check + "|" + quest.Title + "|" + quest.Description;
		GetRPCManager().SendRPC( "QSRPC_QuestInfo", "OnRpcQuestInfo", new Param1<string>( msg ), true, player.GetIdentity() );
	}
}
