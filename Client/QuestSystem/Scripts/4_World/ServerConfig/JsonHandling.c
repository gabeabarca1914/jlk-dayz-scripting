void DecommentJson( string input, string output )
{
	FileHandle hInput = OpenFile( input, FileMode.READ );
	string line = "";
	string toOutput = "";
	while ( FGets( hInput, line ) != -1 )
	{
		toOutput += line + "\n";
	}
	CloseFile( hInput );
	
	FileHandle hOutput = OpenFile( output, FileMode.WRITE );
	bool isInsideQuote;
	for ( int i = 0; i < toOutput.Length(); i++ )
	{
		if ( toOutput.Get( i ) == "\"" )
		{
			isInsideQuote = true;
		}
		else if ( isInsideQuote && toOutput.Get( i ) == "\"" )
		{
			isInsideQuote = false;
		}
		else if ( !isInsideQuote && toOutput.Get( i ) == "/" && toOutput.Get( i + 1 ) == "*" )
		{
			toOutput = toOutput.SubstringInverted( i, IndexOfFrom( i, "*/" ) + 2 );
		}
		else if ( !isInsideQuote && toOutput.Get( i ) == "/" && toOutput.Get( i + 1 ) == "/" )
		{
			toOutput = toOutput.SubstringInverted( i, IndexOfFrom( i, "\n" ) );
		}
		else if ( !isInsideQuote && toOutput.Get( i ) == "#" )
		{
			toOutput = toOutput.SubstringInverted( i, IndexOfFrom( i, "\n" ) );
		}
	}
	FPrintln( hOutput, toOutput );
	CloseFile( hOutput );
}

class JsonDataRegisterQuests: Managed
{
	ref array<ref JsonDataQuestType> RegisterQuests;
}

class JsonDataQuestType: Managed
{
	string QuestType;
	ref array<ref JsonDataQuest> Quests;
}

class JsonDataQuest: Managed
{
	string Title;
	string Description;
	float MinsExpire;
	ref array<ref JsonDataTurnInClassnames> TurnInClassnames;
	ref array<ref JsonDataItemSpawnInfo> ItemSpawnInfo;
	ref array<ref JsonDataZombieSpawnInfo> ZombieSpawnInfo;
}

class JsonDataTurnInClassnames: Managed
{
	ref TStringArray Classnames;
	string MinHealth = "worn";
	int Quantity = 1;
}

class JsonDataItemSpawnInfo: Managed
{
	ref TStringArray Classnames;
	int SpawnQuanity = 1;
	vector Position;
	vector Orientation;
	float Radius;
}

class JsonDataZombieSpawnInfo: Managed
{
	ref TStringArray Classnames;
	int SpawnQuanity = 1;
	vector Position;
	float Radius;
}